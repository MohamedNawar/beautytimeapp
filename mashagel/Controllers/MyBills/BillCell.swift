//
//  BillCell.swift
//  mashagel
//
//  Created by Omar Adel on 12/30/19.
//  Copyright © 2019 MuhammedAli. All rights reserved.
//

import UIKit

class BillCell: UITableViewCell {
    @IBOutlet weak var billNumberLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
