//
//  CategoryViewController.swift
//  mashagel
//
//  Created by MACBOOK on 9/29/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//


import UIKit
import PKHUD
import Alamofire
import FCAlertView
import SDWebImage
class CategoryViewController: UIViewController{
    var shopData = Shop(){
        didSet{
            if let  imageIndex = shopData.data {
                let  imageIndexString = imageIndex.images?.first!.large!
                shopImage.sd_setImage(with: URL(string: imageIndexString ?? ""), completed: nil)
                print(imageIndexString ?? "")
            }
        }
    }
    var id = Int()
    var imagesArray = [#imageLiteral(resourceName: "img36"),#imageLiteral(resourceName: "icons8-star-64"),#imageLiteral(resourceName: "img38"),#imageLiteral(resourceName: "img37")]
    var nameArray = [NSLocalizedString("Working hours", comment: "أوقات الدوام"),
                     NSLocalizedString("Ratting", comment: "أوقات الدوام"),
                     NSLocalizedString("Received Orders", comment: "الطلبات الواردة"),
                     NSLocalizedString("Provider Data", comment: "بيانات السنتر")]
    
    @IBOutlet weak var shopImage: UIImageView!
    @IBOutlet weak var CategoryCollectionView: UICollectionView!
    
   override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.changeFont()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        CategoryCollectionView.delegate = self
        CategoryCollectionView.dataSource = self
        userData.Instance.fetchUser()
        if let currentID = userData.Instance.userSelectedShopId {
            loadShopData(id: currentID )
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationItem.backBarButtonItem?.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationController?.navigationBar.backItem?.backBarButtonItem?.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.title = shopData.data?.name ?? ""

    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        _ = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
    @IBAction func changeLanguage(_ sender: Any) {
        if L102Language.currentAppleLanguage() == "ar"{
            L102Language.setAppleLAnguageTo(lang: "en")
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
        } else {
            L102Language.setAppleLAnguageTo(lang: "ar")
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
        }
     restartApplication ()
    }
    func restartApplication () {
            print(L102Language.currentAppleLanguage())
            Bundle.setLanguage(L102Language.currentAppleLanguage())
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            UIApplication.shared.keyWindow?.rootViewController = storyboard.instantiateInitialViewController()
        }
    @IBAction func notification(_ sender: Any) {
        let navVC = storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        self.navigationController?.pushViewController(navVC, animated: true)
    }
    @IBAction func location(_ sender: Any) {
        let navVC = self.storyboard?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        navVC.shop = self.shopData.data ?? ShopData()
        self.navigationController?.pushViewController(navVC, animated: true)
    }
}
//MARK:- CollectionView Delegate and DataSource
extension CategoryViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            let navVC = storyboard?.instantiateViewController(withIdentifier: "TimeOfWorksTabeViewController") as! TimeOfWorksTabeViewController
            self.navigationController?.pushViewController(navVC, animated: true)
        case 1:
            let navVC = storyboard?.instantiateViewController(withIdentifier: "myShopReservationVC") as! myShopReservationVC
            self.navigationController?.pushViewController(navVC, animated: true)
        case 2:
            let navVC = storyboard?.instantiateViewController(withIdentifier: "myShopReservationVC") as! myShopReservationVC
                self.navigationController?.pushViewController(navVC, animated: true)

        case 3:
            let navVC = storyboard?.instantiateViewController(withIdentifier: "AbouteShopAndEditViewController") as! AbouteShopAndEditViewController
            self.navigationController?.pushViewController(navVC, animated: true)
        default:
            break
        }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == CategoryCollectionView {
            return 4
        }
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCell", for: indexPath)
        let image = cell.viewWithTag(1) as! UIImageView
        let lbl = cell.viewWithTag(2) as! UILabel
        image.image = imagesArray[indexPath.row]
        lbl.text = nameArray[indexPath.row]
        return cell
    }
}
//MARK:- CollectionView FlowLAyout
extension CategoryViewController: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
      
        return CGSize(width: (self.CategoryCollectionView.frame.width / 2)  , height: (self.CategoryCollectionView.frame.width / 2))
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
}
//MARK:- Network
extension CategoryViewController{
    func loadShopData(id:Int){
        let header = APIs.Instance.getHeader()
        print(header)
        HUD.show(.progress, onView: self.view)
        print(APIs.Instance.showShop(id:id))
        Alamofire.request(APIs.Instance.showShop(id:id) , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "img34"))
                        print(err.errors!)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.shopData = try JSONDecoder().decode(Shop.self, from: response.data!)
                        print(self.shopData)
                    }catch{
                        print(error)
                      //  HUD.flash(.label("Error Try Again"), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
}
var bundleKey: UInt8 = 0

class AnyLanguageBundle: Bundle {

override func localizedString(forKey key: String,
                              value: String?,
                              table tableName: String?) -> String {

    guard let path = objc_getAssociatedObject(self, &bundleKey) as? String,
        let bundle = Bundle(path: path) else {
        return super.localizedString(forKey: key, value: value, table: tableName)
    }

    return bundle.localizedString(forKey: key, value: value, table: tableName)
  }
}

extension Bundle {

class func setLanguage(_ language: String) {

    defer {
        object_setClass(Bundle.main, AnyLanguageBundle.self)
    }

    objc_setAssociatedObject(Bundle.main, &bundleKey, Bundle.main.path(forResource: language, ofType: "lproj"), .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
  }
}
