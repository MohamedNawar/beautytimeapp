//
//  myShopReservationVC.swift
//  mashagel
//
//  Created by iMac on 4/2/19.
//  Copyright © 2019 MuhammedAli. All rights reserved.
//


import UIKit
import Alamofire
import PKHUD
import FCAlertView
import SDWebImage
import HCSStarRatingView
class myShopReservationVC: UIViewController{
    var myReservation = MyReservation(){
        didSet{
            tableView.reloadData()
        }
    }
    var image = UIImage()
    var currentReservationData = ReservationData()

    @IBOutlet weak var tableView: UITableView!
    
   override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.changeFont()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        tableView.separatorStyle = .none
        userData.Instance.fetchUser()
        if userData.Instance.data?.type == "2" {
            let currentID = userData.Instance.userSelectedShopId!
            loadMyReservationData(shopId: currentID)
        }else{
            loadMyReservationData()
        }
        self.title = NSLocalizedString("Reservations", comment:"الحجوزات")
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationItem.backBarButtonItem?.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationController?.navigationBar.backItem?.backBarButtonItem?.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        userData.Instance.fetchUser()
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        _ = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "reservationInsideSalon" {
            let destination = segue.destination as! RequestSummary2ViewController
            destination.reservationData = currentReservationData
        }
        if segue.identifier == "reservationOutsideSalon1" {
            let destination = segue.destination as! RequestSummaryViewController
            destination.reservationData = currentReservationData
        }
    }
    @IBAction func insideOrOutSide(_ sender: Any) {
        if userData.Instance.data?.type == "2" {
            let currentID = userData.Instance.userSelectedShopId!
            loadMyReservationData(shopId: currentID)
        }else{
            loadMyReservationData()
        }
    }
    @IBAction func dismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
//MARK:- TableView Delegate and DataSource
extension myShopReservationVC: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myReservation.data?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Reservation11Cell", for: indexPath)
        if let titleValue1 = myReservation.data?[indexPath.row].total_price {
            let title1 = cell.viewWithTag(2) as! UILabel
            title1.text = "\(titleValue1)" + NSLocalizedString(" SR", comment: "")
        }
        if let titleValue = myReservation.data?[indexPath.row].date?.date {
            let title = cell.viewWithTag(11) as! UILabel
            var stringValue = titleValue
            stringValue = stringValue.toDate(format: "yyyy-MM-dd HH:mm:ss.SSS")?.asString() ?? ""
            title.text = stringValue
        }
        let button = cell.viewWithTag(220) as! UIButton
        if L102Language.currentAppleLanguage() == "ar"{
            button.setImage(#imageLiteral(resourceName: "img22"), for: .normal)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        currentReservationData = myReservation.data?[indexPath.row] ?? ReservationData()
        performSegue(withIdentifier: "reservationOutsideSalon1", sender: self)
    }
}
//MARK:- Network
extension myShopReservationVC{
    func loadMyReservationData(shopId:Int){
        let header = APIs.Instance.getHeader()
        print(header)
        HUD.show(.progress, onView: self.view)
        print(APIs.Instance.showShopReservation(id: shopId))
        Alamofire.request(APIs.Instance.showShopReservation(id: shopId) , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "img34"))
                        print(err.errors!)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.myReservation = try JSONDecoder().decode(MyReservation.self, from: response.data!)
                        print(self.myReservation)
                    }catch{
                      //  HUD.flash(.label("Error Try Again"), delay: 1.0)
                        print(error)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    func loadMyReservationData(){
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        print(APIs.Instance.showReservation())
        Alamofire.request(APIs.Instance.showReservation() , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "img34"))
                        print(err.errors!)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.myReservation = try JSONDecoder().decode(MyReservation.self, from: response.data!)
                        print(self.myReservation)
                    }catch{
                      //  HUD.flash(.label("Error Try Again"), delay: 1.0)
                        print(error)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
}
