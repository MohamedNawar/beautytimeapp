//
//  AbouteShopAndEditViewController.swift
//  mashagel
//
//  Created by iMac on 10/14/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//

import UIKit
import PKHUD
import Alamofire
import FCAlertView
class AbouteShopAndEditViewController: UIViewController {
    
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var label3: UILabel!
    @IBOutlet weak var editButton: UIButton!
    
    var shopData = Shop()
    
   override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.changeFont()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        userData.Instance.fetchUser()
        let currentID = userData.Instance.userSelectedShopId!
        loadShopData(id: currentID)
        nameTextField.addPadding(UITextField.PaddingSide.left(20))
        phoneTextField.addPadding(UITextField.PaddingSide.left(20))
        emailTextField.addPadding(UITextField.PaddingSide.left(20))
        self.title = NSLocalizedString("About Shop", comment: "عن المحل")
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationItem.backBarButtonItem?.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationController?.navigationBar.backItem?.backBarButtonItem?.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        if L102Language.currentAppleLanguage() == "ar" {
            phoneTextField.textAlignment = .right
            emailTextField.textAlignment = .right
            nameTextField.textAlignment = .right
            label2.textAlignment = .right
            label3.textAlignment = .right
            name.textAlignment = .right
        }
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        _ = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
    @IBAction func edit(_ sender: Any) {
        nameTextField.isEnabled = true
        emailTextField.isEnabled = true
        phoneTextField.isEnabled = true
        editButton.isHidden = false
    }
    @IBAction func editShopData(_ sender: Any) {
        print(userData.Instance.userSelectedShopId!)
        let currentID = userData.Instance.userSelectedShopId!
        updateShopData(id: currentID)
    }
}
//MARK:- Network
extension AbouteShopAndEditViewController{
    func updateShopData(id:Int){
        if self.emailTextField.text == "" || self.nameTextField.text == "" || self.phoneTextField.text == "" {
            HUD.flash(.label("Enter your Data"), delay: 1.0)
            return
        }
        var header = APIs.Instance.getHeader()
        header.updateValue("application/x-www-form-urlencoded", forKey: "Content-Type")
        let par = ["name": nameTextField.text! , "email": emailTextField.text! , "mobile": phoneTextField.text!] as [String : Any];
        HUD.show(.progress, onView: self.view)
        print(APIs.Instance.UpdateShop(id:id))
        Alamofire.request(APIs.Instance.UpdateShop(id:id) , method: .put, parameters: par , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "img34"))
                        print(err.errors!)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.shopData = try JSONDecoder().decode(Shop.self, from: response.data!)
                        self.editButton.isHidden = true
                        self.nameTextField.resignFirstResponder()
                        self.emailTextField.resignFirstResponder()
                        self.phoneTextField.resignFirstResponder()
                        print(self.shopData)
                    }catch{
                        print(error)
                      //  HUD.flash(.label("Error Try Again"), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    func loadShopData(id:Int){
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        print(APIs.Instance.showShop(id:id))
        Alamofire.request(APIs.Instance.showShop(id:id) , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "img34"))
                        print(err.errors!)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.shopData = try JSONDecoder().decode(Shop.self, from: response.data!)
                        if let dataValue = self.shopData.data{
                            self.nameTextField.text = dataValue.name!
                            self.emailTextField.text = dataValue.email!
                            self.phoneTextField.text = dataValue.mobile!
                        }
                        print(self.shopData)
                    }catch{
                        print(error)
                      //  HUD.flash(.label("Error Try Again"), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                HUD.flash(.label("Error Try Again Later"), delay: 1.0)
                break
            }
        }
    }
}
