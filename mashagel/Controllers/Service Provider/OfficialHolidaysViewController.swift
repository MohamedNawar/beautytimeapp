//
//  OfficialHolidaysViewController.swift
//  mashagel
//
//  Created by MACBOOK on 9/26/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD
import FCAlertView
class OfficialHolidaysViewController: UIViewController, reloadViewDataContent  {
    func reload() {
        userData.Instance.fetchUser()
        let currentID = userData.Instance.userSelectedShopId!
        OfficialHolidaysShopsData(id: currentID)
    }
    var id = Int()
    var currentDate = String()
    var weekDays = Days(){
        didSet{
            self.tableView.reloadData()
        }
    }
    var currentDay = scheduleData()
    
    @IBOutlet weak var tableView: UITableView!
    
   override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.changeFont()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        userData.Instance.fetchUser()
        tableView.separatorStyle = .none
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        userData.Instance.fetchUser()
        self.navigationItem.backBarButtonItem?.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationController?.navigationBar.backItem?.backBarButtonItem?.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        print("eeeeeee")
        id = userData.Instance.userSelectedShopId!
        OfficialHolidaysShopsData(id: id)
        if L102Language.currentAppleLanguage() == "ar"{
            self.title = "أجزات رسمية"
        }else{
            self.title = NSLocalizedString("Official Holidays", comment:"أجزات رسمية")
        }
    }
    private func mangeschedule(){
        //        let navVC = self.storyboard?.instantiateViewController(withIdentifier: "updateExceptionDayViewController") as! updateExceptionDayViewController
        //        navVC.exceptionId = currentDay.id!
        //        navVC.modalPresentationStyle = .overFullScreen
        //        present(navVC, animated: true, completion: nil)
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        _ = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
    @IBAction func addExceptionDay(_ sender: Any) {
        let navVC = self.storyboard?.instantiateViewController(withIdentifier: "AddExceptionDayViewController") as! AddExceptionDayViewController
        navVC.delegate = self
        navVC.id = id
        navVC.modalPresentationStyle = .overFullScreen
        present(navVC, animated: true, completion: nil)
    }
}
extension Date {
    func dayNumberOfWeek() -> Int? {
        return Calendar.current.dateComponents([.weekday], from: self).weekday
    }
}
//MARK:- TableView Delegate and DataSource
extension OfficialHolidaysViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weekDays.data?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if ((weekDays.data?[indexPath.row].date) != nil)  {
            if  ((weekDays.data?[indexPath.row].from_inside) != nil) {
                let cell = tableView.dequeueReusableCell(withIdentifier: "OfficialHolidaysCell2", for: indexPath) as! OfficialHolidaysTableViewCell
                let title = cell.viewWithTag(222) as! UILabel
                let title1 = cell.viewWithTag(2222) as! UILabel
                if L102Language.currentAppleLanguage() == "ar" {
                    title.textAlignment = .right
                    title1.textAlignment = .right
                }
                cell.OfficialHolidaysCallBack = {
                    let navVC = self.storyboard?.instantiateViewController(withIdentifier: "AddExceptionDayViewController") as! AddExceptionDayViewController
                    navVC.delegate = self
                    navVC.selectedID = self.weekDays.data?[indexPath.row].id ?? 0
                    navVC.modalPresentationStyle = .overFullScreen
                    self.present(navVC, animated: true, completion: nil)
                }
                currentDay = (weekDays.data?[indexPath.row])!
                if let titleValue = weekDays.data?[indexPath.row].date{
                    let title = cell.viewWithTag(10) as! UILabel
                    title.text = titleValue
                    if L102Language.currentAppleLanguage() == "ar" {
                        title.textAlignment = .right
                    }
                }
                if let titleValue = weekDays.data?[indexPath.row] {
                    let title = cell.viewWithTag(3) as! UILabel
                    if L102Language.currentAppleLanguage() == "ar" {
                        title.textAlignment = .right
                    }
                    if let from = titleValue.from_inside{
                        if let to = titleValue.to_inside{
                            if L102Language.currentAppleLanguage() == "ar"{
                                title.text = "من الساعة \(from)إلي الساعة \(to)"
                            }else{
                                title.text = NSLocalizedString("from \(from) to \(to) ", comment:   "من الساعة \(from) إلي الساعة \(to) ")                               }
                        }
                    }
                }
                if let titleValue1 = weekDays.data?[indexPath.row] {
                    let title = cell.viewWithTag(22) as! UILabel
                    if L102Language.currentAppleLanguage() == "ar" {
                        title.textAlignment = .right
                    }
                    if let from = titleValue1.from_outside{
                        if let to = titleValue1.to_outside{
                            if L102Language.currentAppleLanguage() == "ar"{
                                title.text = "من الساعة \(from) إلي الساعة \(to) "
                            }else{
                                title.text = NSLocalizedString("from \(from) to \(to)  ", comment:   "من الساعة \(from) صباحاإلي الساعة \(to) ليلا خارج الصالون")
                            }
                        }
                    }
                }
                if let titleValue = weekDays.data?[indexPath.row].date?.toDate(format: "yyyy-MM-dd")?.dayNumberOfWeek() {
                    let title = cell.viewWithTag(1) as! UILabel
                    if L102Language.currentAppleLanguage() == "ar" {
                        title.textAlignment = .right
                    }
                    switch titleValue {
                    case 7:
                        if L102Language.currentAppleLanguage() == "ar"{
                            title.text = "السبت"
                        }else{
                            title.text = NSLocalizedString("Saturday", comment:"السبت")
                        }
                    case 1:
                        if L102Language.currentAppleLanguage() == "ar"{
                            title.text = "الأحد"
                        }else{
                            title.text = NSLocalizedString("Sunday", comment:"الأحد")
                        }
                    case 2:
                        if L102Language.currentAppleLanguage() == "ar"{
                            title.text = "الأثنين"
                        }else{
                            title.text = NSLocalizedString("Monday", comment:"الأثنين")
                        }
                    case 3:
                        if L102Language.currentAppleLanguage() == "ar"{
                            title.text = "الثلاثاء"
                        }else{
                            title.text = NSLocalizedString("Tuesday", comment:"الثلاثاء")
                        }
                    case 4:
                        if L102Language.currentAppleLanguage() == "ar"{
                            title.text = "الأربعاء"
                        }else{
                            title.text = NSLocalizedString("Wednesday", comment:"الأربعاء")
                        }
                    case 5:
                        if L102Language.currentAppleLanguage() == "ar"{
                            title.text = "الخميس"
                        }else{
                            title.text = NSLocalizedString("Thursday", comment:"الخميس")
                        }
                    case 6:
                        if L102Language.currentAppleLanguage() == "ar"{
                            title.text = "الجمعة"
                        }else{
                            title.text = NSLocalizedString("Friday", comment:"الجمعة")
                        }
                    default:
                        title.text = "\(titleValue)"
                    }
                }
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "OfficialHolidaysCell", for: indexPath) as! OfficialHolidaysTableViewCell
                cell.OfficialHolidaysCallBack = {
                    let navVC = self.storyboard?.instantiateViewController(withIdentifier: "AddExceptionDayViewController") as! AddExceptionDayViewController
                    navVC.delegate = self
                    navVC.selectedID = self.weekDays.data?[indexPath.row].id ?? 0
                    navVC.modalPresentationStyle = .overFullScreen
                    self.present(navVC, animated: true, completion: nil)
                }
                currentDay = (weekDays.data?[indexPath.row])!
                if let titleValue = weekDays.data?[indexPath.row].date{
                    let title = cell.viewWithTag(10) as! UILabel
                    if L102Language.currentAppleLanguage() == "ar" {
                        title.textAlignment = .right
                    }
                    title.text = titleValue
                }
                if let titleValue = weekDays.data?[indexPath.row].date?.toDate(format: "yyyy-MM-dd")?.dayNumberOfWeek() {
                    let title = cell.viewWithTag(1) as! UILabel
                    if L102Language.currentAppleLanguage() == "ar" {
                        title.textAlignment = .right
                    }
                    switch titleValue {
                    case 7:
                        if L102Language.currentAppleLanguage() == "ar"{
                            title.text = "السبت"
                        }else{
                            title.text = NSLocalizedString("Saturday", comment:"السبت")
                        }
                    case 1:
                        if L102Language.currentAppleLanguage() == "ar"{
                            title.text = "الأحد"
                        }else{
                            title.text = NSLocalizedString("Sunday", comment:"الأحد")
                        }
                    case 2:
                        if L102Language.currentAppleLanguage() == "ar"{
                            title.text = "الأثنين"
                        }else{
                            title.text = NSLocalizedString("Monday", comment:"الأثنين")
                        }
                    case 3:
                        if L102Language.currentAppleLanguage() == "ar"{
                            title.text = "الثلاثاء"
                        }else{
                            title.text = NSLocalizedString("Tuesday", comment:"الثلاثاء")
                        }
                    case 4:
                        if L102Language.currentAppleLanguage() == "ar"{
                            title.text = "الأربعاء"
                        }else{
                            title.text = NSLocalizedString("Wednesday", comment:"الأربعاء")
                        }
                    case 5:
                        if L102Language.currentAppleLanguage() == "ar"{
                            title.text = "الخميس"
                        }else{
                            title.text = NSLocalizedString("Thursday", comment:"الخميس")
                        }
                    case 6:
                        if L102Language.currentAppleLanguage() == "ar"{
                            title.text = "الجمعة"
                        }else{
                            title.text = NSLocalizedString("Friday", comment:"الجمعة")
                        }
                    default:
                        title.text = "\(titleValue)"
                    }
                }
                return cell
            }
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "OfficialHolidaysCell1", for: indexPath) as! OfficialHolidaysTableViewCell
            let title = cell.viewWithTag(333) as! UILabel
            if L102Language.currentAppleLanguage() == "ar" {
                title.textAlignment = .right
            }
            cell.RemoveHolidaysCallBack = {
                self.RemoveOfficialHoliday(id: self.weekDays.data?[indexPath.row].id ?? 0)
            }
            if let titleValue = weekDays.data?[indexPath.row].day {
                let title = cell.viewWithTag(1) as! UILabel
                if L102Language.currentAppleLanguage() == "ar" {
                    title.textAlignment = .right
                }
                if L102Language.currentAppleLanguage() == "ar" {
                    title.textAlignment = .right
                }
                switch titleValue {
                case 6:
                    if L102Language.currentAppleLanguage() == "ar"{
                        title.text = "السبت"
                    }else{
                        title.text = NSLocalizedString("Saturday", comment:"السبت")
                    }
                case 7:
                    if L102Language.currentAppleLanguage() == "ar"{
                        title.text = "الأحد"
                    }else{
                        title.text = NSLocalizedString("Sunday", comment:"الأحد")
                    }
                case 1:
                    if L102Language.currentAppleLanguage() == "ar"{
                        title.text = "الأثنين"
                    }else{
                        title.text = NSLocalizedString("Monday", comment:"الأثنين")
                    }
                case 2:
                    if L102Language.currentAppleLanguage() == "ar"{
                        title.text = "الثلاثاء"
                    }else{
                        title.text = NSLocalizedString("Tuesday", comment:"الثلاثاء")
                    }
                case 3:
                    if L102Language.currentAppleLanguage() == "ar"{
                        title.text = "الأربعاء"
                    }else{
                        title.text = NSLocalizedString("Wednesday", comment:"الأربعاء")
                    }
                case 4:
                    if L102Language.currentAppleLanguage() == "ar"{
                        title.text = "الخميس"
                    }else{
                        title.text = NSLocalizedString("Thursday", comment:"الخميس")
                    }
                case 5:
                    if L102Language.currentAppleLanguage() == "ar"{
                        title.text = "الجمعة"
                    }else{
                        title.text = NSLocalizedString("Friday", comment:"الجمعة")
                    }
                default:
                    title.text = "\(titleValue)"
                }
            }
            return cell
        }
    }
}
//MARK:- Network
extension OfficialHolidaysViewController{
    func OfficialHolidaysShopsData(id:Int){
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        print(APIs.Instance.showShopExceptions(id: id))
        Alamofire.request(APIs.Instance.showShopExceptions(id: id) , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "img34"))
                        print(err.errors!)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.weekDays = try JSONDecoder().decode(Days.self, from: response.data!)
                        print(self.weekDays)
                    }catch{
                      //  HUD.flash(.label("Error Try Again"), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    func RemoveOfficialHoliday(id:Int){
        let header = APIs.Instance.getHeader()
        print(header)
        HUD.show(.progress, onView: self.view)
        print(APIs.Instance.removeOfficialHoliday(id: id))
        Alamofire.request(APIs.Instance.removeOfficialHoliday(id: id) , method: .delete, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "img34"))
                        print(err.errors!)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    let currentID = userData.Instance.userSelectedShopId!
                    self.OfficialHolidaysShopsData(id: currentID)
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
}
