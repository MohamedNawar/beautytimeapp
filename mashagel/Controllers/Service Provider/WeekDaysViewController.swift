//
//  WeekDaysViewController.swift
//  mashagel
//
//  Created by MACBOOK on 9/26/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD
import FCAlertView
class WeekDaysViewController: UIViewController, reloadViewDataContent {
    func reload() {
        userData.Instance.fetchUser()
        currentID1 = userData.Instance.userSelectedShopId!
        WeekDaysShopsData(id: currentID1)
    }
    
    var id = Int()
    var dayId = Int()
    var weekDays = Days(){
        didSet{
            self.tableView.reloadData()
        }
    }
    var currentDay = scheduleData()
    var currentID1 = Int()
    
    @IBOutlet weak var tableView: UITableView!
    
   override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.changeFont()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        tableView.separatorStyle = .none
        userData.Instance.fetchUser()
        let currentID = userData.Instance.userSelectedShopId!
        print(currentID)
        WeekDaysShopsData(id: currentID)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationItem.backBarButtonItem?.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationController?.navigationBar.backItem?.backBarButtonItem?.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        userData.Instance.fetchUser()
        currentID1 = userData.Instance.userSelectedShopId!
        let currentID = userData.Instance.userSelectedShopId!
        print(currentID)
        WeekDaysShopsData(id: currentID)
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        _ = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
    @IBAction func addScheduleDays(_ sender: Any) {
        //        let navVC = self.storyboard?.instantiateViewController(withIdentifier: "AddScheduleViewController") as! AddScheduleViewController
        //        navVC.modalPresentationStyle = .overFullScreen
        //        present(navVC, animated: true, completion: nil)
    }
}
//MARK:- Table View Delegate and Data source
extension WeekDaysViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weekDays.data?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WeekDaysCell", for: indexPath) as! WeekDaysTableViewCell
        currentDay = (weekDays.data?[indexPath.row])!
        dayId = indexPath.row+1
        let num1 = cell.viewWithTag(222) as! UILabel
        let num2 = cell.viewWithTag(2222) as! UILabel
        let num3 = cell.viewWithTag(12) as! UILabel
        let num4 = cell.viewWithTag(33) as! UILabel
        let num5 = cell.viewWithTag(32) as! UILabel
        let num6 = cell.viewWithTag(52) as! UILabel
        if L102Language.currentAppleLanguage() == "ar" {
            num1.textAlignment = .right
            num2.textAlignment = .right
            num3.textAlignment = .right
            num4.textAlignment = .right
            num5.textAlignment = .right
            num6.textAlignment = .right
        }
        num1.text = "\(weekDays.data?[indexPath.row].number_inside ?? "0")"
        num2.text = "\(weekDays.data?[indexPath.row].number_outside ?? "0")"
        if let titleValue = weekDays.data?[indexPath.row].day_of_week {
            let title = cell.viewWithTag(1) as! UILabel
            if L102Language.currentAppleLanguage() == "ar" {
                title.textAlignment = .right
            }
            switch titleValue {
            case "6":
                cell.editWeekDayCallBack = {
                    self.mangeschedule(Cellid:6)
                }
                cell.addToHolidaysCallBack = {
                    self.addToHoliday(id: self.currentID1, selectedDay: 6)
                }
                    title.text = NSLocalizedString("Saturday", comment:"السبت")
            case "7":
                cell.editWeekDayCallBack = {
                    self.mangeschedule(Cellid:7)
                }
                cell.addToHolidaysCallBack = {
                    self.addToHoliday(id: self.currentID1, selectedDay: 7)
                }
                    title.text = NSLocalizedString("Sunday", comment:"الأحد")
            case "1":
                cell.editWeekDayCallBack = {
                    self.mangeschedule(Cellid:1)
                }
                cell.addToHolidaysCallBack = {
                    self.addToHoliday(id: self.currentID1, selectedDay: 1)
                }
                    title.text = NSLocalizedString("Monday", comment:"الأثنين")
            case "2":
                cell.editWeekDayCallBack = {
                    self.mangeschedule(Cellid:2)
                }
                cell.addToHolidaysCallBack = {
                    self.addToHoliday(id: self.currentID1, selectedDay: 2)
                }
                    title.text = NSLocalizedString("Tuesday", comment:"الثلاثاء")
            case "3":
                cell.editWeekDayCallBack = {
                    self.mangeschedule(Cellid:3)
                }
                cell.addToHolidaysCallBack = {
                    self.addToHoliday(id: self.currentID1, selectedDay: 3)
                }
                    title.text = NSLocalizedString("Wednesday", comment:"الأربعاء")
            case "4":
                cell.editWeekDayCallBack = {
                    self.mangeschedule(Cellid:4)
                }
                cell.addToHolidaysCallBack = {
                    self.addToHoliday(id: self.currentID1, selectedDay: 4)
                }
                    title.text = NSLocalizedString("Thursday", comment:"الخميس")
            case "5":
                cell.editWeekDayCallBack = {
                    self.mangeschedule(Cellid:5)
                }
                cell.addToHolidaysCallBack = {
                    self.addToHoliday(id: self.currentID1, selectedDay: 5)
                }
                    title.text = NSLocalizedString("Friday", comment:"الجمعة")
            default:
                title.text = "\(titleValue)"
            }
        }
        if let titleValue = weekDays.data?[indexPath.row] {
            dayId = weekDays.data?[indexPath.row].id ?? 0
            let title = cell.viewWithTag(3) as! UILabel
            if L102Language.currentAppleLanguage() == "ar" {
                title.textAlignment = .right
            }
            if let from = titleValue.from_inside{
                if let to = titleValue.to_inside{
                    if L102Language.currentAppleLanguage() == "ar"{
                        title.text = "من الساعة \(from)إلي الساعة \(to)"
                    }
                    else{
                        title.text = NSLocalizedString("from \(from) to \(to) ", comment:   "من الساعة \(from) إلي الساعة \(to) ")
                        
                    }
                }
            }
        }
        if let titleValue1 = weekDays.data?[indexPath.row] {
            let title = cell.viewWithTag(22) as! UILabel
            if L102Language.currentAppleLanguage() == "ar" {
                title.textAlignment = .right
            }
            if let from = titleValue1.from_outside{
                if let to = titleValue1.to_outside{
                    if L102Language.currentAppleLanguage() == "ar"{
                        title.text = "من الساعة \(from) إلي الساعة \(to) "
                        
                    }else{
                        title.text = NSLocalizedString("from \(from) to \(to)  ", comment:   "من الساعة \(from) صباحاإلي الساعة \(to) ليلا خارج الصالون")
                    }
                }
            }
        }
        return cell
    }
}
//MARK:- Network
extension WeekDaysViewController{
    private func mangeschedule(Cellid:Int){
        let navVC = self.storyboard?.instantiateViewController(withIdentifier: "EditWeekDayViewController") as! EditWeekDayViewController
        navVC.delegate = self
        navVC.day = Cellid
        print(Cellid)
        navVC.modalPresentationStyle = .overFullScreen
        present(navVC, animated: true, completion: nil)
    }
    func addToHoliday(id:Int , selectedDay:Int){
        let par = ["day": selectedDay] as [String :Any]
        print(par)
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.addToHoliday(id: id), method: .post, parameters: par , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "img34"))
                        print(err.message!)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    self.WeekDaysShopsData(id: self.currentID1)
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    func WeekDaysShopsData(id:Int){
        print("\(id)")
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.showSchedule(id: id) , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "img34"))
                        print(err.message!)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.weekDays = try JSONDecoder().decode(Days.self, from: response.data!)
                        print(self.weekDays)
                    }catch{
//                      //  HUD.flash(.label("Error Try Again"), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
}
