//
//  EditWeekDayViewController.swift
//  mashagel
//
//  Created by MACBOOK on 10/8/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//
import UIKit
import Alamofire
import PKHUD
import CoreLocation
import FCAlertView
protocol reloadViewDataContent {
    func reload()
}
class EditWeekDayViewController: UIViewController {
    
    var delegate:reloadViewDataContent? = nil
    var id = Int()
    var day = Int(){
        didSet{
            print(day)
        }
    }
    var quentity = 1 {
        didSet{
            num1.text = "\(quentity)"
        }
    }
    var quentity1 = 1 {
        didSet{
            num2.text = "\(quentity1)"
        }
    }
    var weekDays = Days()
    let fromPicker1 = UIDatePicker()
    let toPicker1 = UIDatePicker()
    let fromPicker = UIDatePicker()
    let toPicker = UIDatePicker()
    
    @IBOutlet weak var to1: UILabel!
    @IBOutlet weak var from1: UILabel!
    @IBOutlet weak var to: UILabel!
    @IBOutlet weak var from: UILabel!
    @IBOutlet weak var fromTextField1: UITextField!
    @IBOutlet weak var toTextField1: UITextField!
    @IBOutlet weak var toTextField: UITextField!
    @IBOutlet weak var fromTextField: UITextField!
    @IBOutlet weak var num1: UILabel!
    @IBOutlet weak var num2: UILabel!

   override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.changeFont()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.title = NSLocalizedString("Edit", comment: "تعديل")
        userData.Instance.fetchUser()
        id = userData.Instance.userSelectedShopId!
        toPicker.locale = Locale(identifier: "en_US_POSIX")
        toPicker1.locale = Locale(identifier: "en_US_POSIX")
        fromPicker.locale = Locale(identifier: "en_US_POSIX")
        fromPicker1.locale = Locale(identifier: "en_US_POSIX")

        toPicker.backgroundColor = .white
        fromPicker.backgroundColor = .white
        fromTextField1.addPadding(UITextField.PaddingSide.left(20))
        toTextField1.addPadding(UITextField.PaddingSide.left(20))
        fromTextField.addPadding(UITextField.PaddingSide.left(20))
        toTextField.addPadding(UITextField.PaddingSide.left(20))
        fromTextField1.setRightViewFAIcon(icon: .FAAngleDown, rightViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
        toTextField1.setRightViewFAIcon(icon: .FAAngleDown, rightViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
        fromTextField.setRightViewFAIcon(icon: .FAAngleDown, rightViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
        toTextField.setRightViewFAIcon(icon: .FAAngleDown, rightViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
        showToPicker()
        showFromPicker()
        showToPicker1()
        showFromPicker1()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        userData.Instance.fetchUser()
        self.navigationItem.backBarButtonItem?.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationController?.navigationBar.backItem?.backBarButtonItem?.tintColor = .clear
        self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        if L102Language.currentAppleLanguage() == "ar" {
            //            to1.textAlignment = .right
            from1.textAlignment = .right
            //            to.textAlignment = .right
            from.textAlignment = .right
            //            fromTextField1.textAlignment = .right
            //            toTextField1.textAlignment = .right
            //            toTextField.textAlignment = .right
            //            fromTextField.textAlignment = .right
        }
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        _ = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
    @IBAction func dismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func add(_ sender: Any) {
        if id != 0 && day != 0 {
            self.dismiss(animated: true) {
                self.updateShopScheduleShopsData(id: self.id)
            }
        }
    }
    @IBAction func removeOneInSide(_ sender: Any) {
        quentity -= 1
        if quentity < 1 {
            quentity = 1
        }
    }
    @IBAction func addOneInside(_ sender: Any) {
        quentity += 1
    }
    @IBAction func removeOneInExternal(_ sender: Any) {
        quentity1 -= 1
        if quentity1 < 1 {
            quentity1 = 1
        }
    }
    @IBAction func addOneInExternal(_ sender: Any) {
        quentity1 += 1
    }
}
//MARK:- PickerView Methods
extension EditWeekDayViewController{
    func showFromPicker(){
        //Formate Date
        fromPicker.datePickerMode = .time
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Cancel", comment: ""), style: .plain, target: self, action: #selector(cancelDatePicker));
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        fromTextField.inputAccessoryView = toolbar
        fromTextField.inputView = fromPicker
    }
    func showToPicker(){
        //Formate Date
        toPicker.datePickerMode = .time
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donedatePicker1));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Dismiss", comment: "إلغاء"), style: .plain, target: self, action: #selector(cancelDatePicker));
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        toTextField.inputAccessoryView = toolbar
        toTextField.inputView = toPicker
    }
    @objc func donedatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        fromTextField.text = formatter.string(from: fromPicker.date)
        self.view.endEditing(true)
    }
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    @objc func donedatePicker1(){
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        toTextField.text = formatter.string(from: toPicker.date)
        self.view.endEditing(true)
    }
    func showFromPicker1(){
        //Formate Date
        fromPicker1.datePickerMode = .time
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker11));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        fromTextField1.inputAccessoryView = toolbar
        fromTextField1.inputView = fromPicker1
    }
    func showToPicker1(){
        //Formate Date
        toPicker1.datePickerMode = .time
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donedatePicker111));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Dismiss", comment: "إلغاء"), style: .plain, target: self, action: #selector(cancelDatePicker));
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        toTextField1.inputAccessoryView = toolbar
        toTextField1.inputView = toPicker1
    }
    @objc func donedatePicker11(){
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        fromTextField1.text = formatter.string(from: fromPicker1.date)
        self.view.endEditing(true)
    }
    @objc func donedatePicker111(){
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        toTextField1.text = formatter.string(from: toPicker1.date)
        self.view.endEditing(true)
    }
}
//MARK:- network
extension EditWeekDayViewController{
    func updateShopScheduleShopsData(id:Int){
        let par = ["day_of_week": day ,"number_inside":Int(num1.text!) ?? 0 ,"number_outside":Int(num2.text!) ?? 0,"from_inside": fromTextField.text ?? "" , "to_inside": toTextField.text ?? "","from_outside": fromTextField1.text ?? "" , "to_outside": toTextField1.text ?? "" ] as [String : Any]
        print(par)
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.AddShopSchedule(id: id) , method: .post, parameters: par , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "img34"))
                        print(err.message!)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.weekDays = try JSONDecoder().decode(Days.self, from: response.data!)
                        print(self.weekDays)
                        self.delegate?.reload()
                    }catch{
                      //  HUD.flash(.label("Error Try Again"), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
}
