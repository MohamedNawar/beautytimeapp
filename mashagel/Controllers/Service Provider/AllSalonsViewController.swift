//
//  AllSalonsViewController.swift
//  mashagel
//
//  Created by MACBOOK on 10/1/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//

import UIKit

class AllSalonsViewController: UIViewController{
    @IBOutlet weak var tableView: UITableView!
    
   override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.changeFont()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        tableView.separatorStyle = .none
        userData.Instance.fetchUser()
    }
}
//MARK:- tableView Delegate and DataSource
extension AllSalonsViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SalonCell", for: indexPath)
            return cell
    }
}
