//
//  MyPocketViewController.swift
//  mashagel
//
//  Created by Omar Adel on 12/31/19.
//  Copyright © 2019 MuhammedAli. All rights reserved.
//

import UIKit
import TelrSDK

class ViewController: UIViewController {

        let KEY:String = "????????"  // TODO fill key
        let STOREID:String = "?????????"         // TODO fill store id
        let EMAIL:String = "?????????"  // TODO fill email
        
        var paymentRequest:PaymentRequest?
        //var show:Bool = false;

        @IBOutlet var showcardbtn: UIButton!
        @IBOutlet var cardsv: UIStackView!
        @IBOutlet var amount_txt: UITextField!
        @IBOutlet var card_txt: UILabel!
        
        
        @IBAction func showcardbtnPressed(_ sender: Any) {
            card_txt.text = "**** **** **** " + getSavedData(key: "last4")
            if(cardsv.isHidden == true){
                cardsv.isHidden = false
                showcardbtn.setTitle("Hide",for: .normal)
            }else{
                cardsv.isHidden = true
                showcardbtn.setTitle("Show stored card",for: .normal)
            }
        }
        
        @IBAction func paybtnPressed(_ sender: Any) {
            paymentRequest = preparePaymentRequest()
            performSegue(withIdentifier: "TelrSegue", sender: paymentRequest)
        }
        
        @IBAction func paybtn2Pressed(_ sender: Any) {
            paymentRequest = preparePaymentRequest2()
            performSegue(withIdentifier: "TelrSegue", sender: paymentRequest)
        }
        
        override func viewDidLoad() {
            super.viewDidLoad()
            if(getSavedData(key: "last4").isEmpty){
                showcardbtn.isHidden = true
            }else{
                showcardbtn.isHidden = false
            }
            
        }
        
        
        override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            if let destination = segue.destination as? TelrController{
                destination.paymentRequest = paymentRequest!
            }
        }
        
        private func getSavedData(key:String) -> String{
            let defaults = UserDefaults.standard
            return defaults.string(forKey: key) ?? ""
        }
        
        
        private func preparePaymentRequest() -> PaymentRequest{
            
            let paymentReq = PaymentRequest()
            paymentReq.key = KEY
            paymentReq.store = STOREID
            paymentReq.appId = "123456789"
            paymentReq.appName = "TelrSDK"
            paymentReq.appUser = "123456"
            paymentReq.appVersion = "0.0.1"
            paymentReq.transTest = "1"
            paymentReq.transType = "auth"
            paymentReq.transClass = "paypage"
            paymentReq.transCartid = String(arc4random())
            paymentReq.transDesc = "Test API"
            paymentReq.transCurrency = "AED"
            paymentReq.transAmount = amount_txt.text!
            paymentReq.transLanguage = "en"
            paymentReq.billingEmail = EMAIL // TODO fill email
            paymentReq.billingFName = "Hany"
            paymentReq.billingLName = "Sakr"
            paymentReq.billingTitle = "Mr"
            paymentReq.city = "Dubai"
            paymentReq.country = "AE"
            paymentReq.region = "Dubai"
            paymentReq.address = "line 1"
            paymentReq.billingPhone="8785643"
            return paymentReq
        }
        
        private func preparePaymentRequest2() -> PaymentRequest{
            
            let paymentReq = PaymentRequest()
            paymentReq.key = KEY
            paymentReq.store = STOREID
            paymentReq.appId = "123456789"
            paymentReq.appName = "TelrSDK"
            paymentReq.appUser = "123456"
            paymentReq.appVersion = "0.0.1"
            paymentReq.transTest = "1"
            paymentReq.transType = "sale"
            paymentReq.transClass = "cont"
            paymentReq.transCartid = String(arc4random())
            paymentReq.transDesc = "Test API"
            paymentReq.transCurrency = "AED"
            paymentReq.transAmount = amount_txt.text!
            paymentReq.transRef = getSavedData(key: "ref")
            return paymentReq
        }


    @IBOutlet weak var myCurrentChargeLabel: UILabel!
    @IBOutlet weak var operationsTableView: UITableView!
   override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.changeFont()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        operationsTableView.delegate = self
        operationsTableView.dataSource = self
    }
}

extension MyPocketViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyPocket", for: indexPath) as! MyPocket
        return cell
    }
    
    
}
