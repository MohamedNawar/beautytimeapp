//
//  PersonalProfileViewController.swift
//  mashagel
//
//  Created by MACBOOK on 10/6/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//


import UIKit
import PKHUD
import Alamofire
import FCAlertView
class PersonalProfileViewController: UIViewController {
    
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var passText: UITextField!
    @IBOutlet weak var passLbl: UILabel!
    @IBOutlet weak var phoneTxt: UITextField!
    @IBOutlet weak var phoneLbl: UILabel!
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var nameTxt: UITextField!
    
   override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.changeFont()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        userData.Instance.fetchUser()
        print(userData.Instance.userSelectedShopId ?? "")
        print(userData.Instance.name ?? "")
        nameTxt.text = userData.Instance.data?.name ?? ""
        phoneTxt.text = userData.Instance.data?.mobile ?? ""
        emailTxt.text = userData.Instance.data?.email ?? ""
        
        passText.addPadding(UITextField.PaddingSide.left(20))
        nameTxt.addPadding(UITextField.PaddingSide.left(20))
        phoneTxt.addPadding(UITextField.PaddingSide.left(20))
        emailTxt.addPadding(UITextField.PaddingSide.left(20))
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationItem.backBarButtonItem?.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationController?.navigationBar.backItem?.backBarButtonItem?.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.8558626771, green: 0.2503757179, blue: 0.4763913751, alpha: 1)
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0.8558626771, green: 0.2503757179, blue: 0.4763913751, alpha: 1)
        if L102Language.currentAppleLanguage() == "ar" {
            phoneTxt.textAlignment = .right
            emailTxt.textAlignment = .right
            nameLbl.textAlignment = .right
            emailLbl.textAlignment = .right
            nameTxt.textAlignment = .right
            passLbl.textAlignment = .right
            passText.textAlignment = .right
            passText.addPadding(UITextField.PaddingSide.right(20))
            nameTxt.addPadding(UITextField.PaddingSide.right(20))
            phoneTxt.addPadding(UITextField.PaddingSide.right(20))
            emailTxt.addPadding(UITextField.PaddingSide.right(20))
        }
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        _ = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
    @IBAction func editShopData(_ sender: Any) {
        updateProfile()
    }
    @IBAction func editProfile(_ sender: Any) {
        passLbl.isHidden = false
        passText.isHidden = false
        emailTxt.isEnabled = true
        nameTxt.isEnabled = true
        phoneTxt.isEnabled = true
        editBtn.isHidden = false
    }
}
//MARK:- Network
extension PersonalProfileViewController{
    func updateProfile(){
        if self.emailTxt.text == "" || self.nameTxt.text == "" || self.phoneTxt.text == "" {
            HUD.flash(.label("Enter your Data"), delay: 1.0)
            return
        }
        if self.passText.text == "" {
            HUD.flash(.label("Enter your Password."), delay: 1.0)
            return
        }
        var header = APIs.Instance.getHeader()
        header.updateValue("application/x-www-form-urlencoded", forKey: "Content-Type")
        let par = ["name": nameTxt.text! , "email": emailTxt.text! , "mobile": phoneTxt.text!,"password": passText.text!] as [String : Any];
        HUD.show(.progress, onView: self.view)
        print(APIs.Instance.updateProfile(id: userData.Instance.data?.id ?? 0))
        Alamofire.request(APIs.Instance.updateProfile(id: userData.Instance.data?.id ?? 0), method: .patch, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            print(response)
            switch(response.result) {
            case .success( _):
                HUD.hide()
                let temp = response.response?.statusCode ?? 400
                print(temp)
                if temp >= 300 {
                    print("errorrrr")
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "img34"))
                        print(err.message!)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        userData.Instance.saveUser(data: response.data!)
                        userData.Instance.fetchUser()
                        print("successsss")
                        self.editBtn.isHidden = true
                    }catch{
                      //  HUD.flash(.label("Error Try Again"), delay: 1.0)
                    }
                    HUD.flash(.success, delay: 1.0)
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
        
    }

}
