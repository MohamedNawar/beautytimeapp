//
//  SearchViewController.swift
//  mashagel
//
//  Created by MACBOOK on 9/30/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD
import CoreLocation
import FCAlertView
import SideMenu
import DLRadioButton

class SearchViewController:
UIViewController , CLLocationManagerDelegate{
    
    @IBOutlet weak var departmentTextField: UITextField!
    @IBOutlet weak var areaLbl: UILabel!
    @IBOutlet weak var areaTextField: UITextField!
    @IBOutlet weak var serviceLocationLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var districtionLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var hiddentextField: UITextField!
    @IBOutlet weak var districtTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var dateTextField: DesignableTextField!
    
    var request = "http://service.beautytimes.org/api/home"
    var shopes = Shops()
    let locationManager = CLLocationManager()
    var coordinateValue = CLLocationCoordinate2D(latitude: 0, longitude: 0)
    let cityPicker = UIPickerView()
    let districtPicker = UIPickerView()
    let areaPicker = UIPickerView()
    let datePicker = UIDatePicker()
    let departmentPicker = UIPickerView()
    var selectedCity = CitiesData()
    var departments = Department()
    var selectedDepartment = Int()
    var areas = Area(){
        didSet{
            areaPicker.reloadAllComponents()
        }
    }
    var city = [CitiesData](){
        didSet{
            cityPicker.reloadAllComponents()
        }
    }
    var districts = [Areas](){
        didSet{
            districtPicker.reloadAllComponents()
        }
    }
    var words = ["Cat", "Chicken", "fish", "Dog", "Mouse", "Guinea Pig", "monkey"]
    var selected = ""
    var words1 = ["Ccat", "Cchicken", "fcish", "Dcog", "Mcouse", "Gucinea Pcig", "cmonkey"]
    var selected1 = ""
    var words2 = ["Ccat", "Cchicken", "fcish", "Dcog", "Mcouse", "Gucinea Pcig", "cmonkey"]
    var selected2 = ""
    var cityId :Int = 0
    var areaId :Int = 0
    var selectedAreaId :Int = 0
    var districtId :Int = 0
    var selectedCityIndex = 0
    var serType = "inside"
    
   override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.changeFont()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        let date = Date().dayAfter
        dateTextField.text = date.asString()
        if L102Language.currentAppleLanguage() == "ar"{
            self.title = "ابحث"
            navigationItem.title = "ابحث"
            //            serviceType.setTitle("في ضيافتنا", forSegmentAt: 0)
            //            serviceType.setTitle("بالمنزل", forSegmentAt: 1)
        }else{
            self.title = NSLocalizedString("Search", comment:"ابحث")
            navigationItem.title = NSLocalizedString("Search", comment:"ابحث")
        }
        self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 0.992618865)
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 0.992618865)
        userData.Instance.fetchUser()
        if L102Language.currentAppleLanguage() == "ar" {
            cityLabel.textAlignment = .right
            areaLbl.textAlignment = .right
            districtionLabel.textAlignment = .right
            dateLabel.textAlignment = .right
            serviceLocationLabel.textAlignment = .right
        }
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if  let coordinate = locationManager.location?.coordinate {
            coordinateValue = coordinate
            self.locationManager.stopUpdatingLocation()
        }else {
        }
    }
    @IBAction func search(_ sender: Any) {
        if self.districtTextField.text == "" {
            HUD.flash(.label(NSLocalizedString("Enter The District", comment: "")), delay: 1.0)
            return
        }
        if self.departmentTextField.text == "" {
            HUD.flash(.label(NSLocalizedString("Enter The Department", comment: "")), delay: 1.0)
            return
        }
        if self.dateTextField.text == "" {
            HUD.flash(.label(NSLocalizedString("Enter The Date", comment: "")), delay: 1.0)
            return
        }
        if let date = dateTextField.text {
            userData.Instance.removeselectedDate()
            userData.Instance.selectedDate =  date
            UserDefaults.standard.set(date, forKey: "selectedDate")
        }
        if cityTextField.text != "" {
            request =  "http://service.beautytimes.org/api/home?area=\(selectedAreaId)?latitude=\(coordinateValue.latitude)?longitude=\(coordinateValue.longitude)?type=\(serType)?departments=\(selectedDepartment)"
            print(request)
        }
        let navVC = self.storyboard?.instantiateViewController(withIdentifier: "MyPocketViewController") as! MyPocketViewController
        print("sesrch ------\(request)")
//        navVC.url = request
        self.navigationController?.pushViewController(navVC, animated: true)
    }
    @IBAction func language(_ sender: Any) {
        if L102Language.currentAppleLanguage() == "ar"{
            L102Language.setAppleLAnguageTo(lang: "en")
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
        } else {
            L102Language.setAppleLAnguageTo(lang: "ar")
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
        }
        let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
        rootviewcontroller.rootViewController = self.storyboard?.instantiateViewController(withIdentifier: "TabBarControllerViewController")
        let mainwindow = (UIApplication.shared.delegate?.window!)!
        mainwindow.backgroundColor = UIColor(hue: 0.6477, saturation: 0.6314, brightness: 0.6077, alpha: 0.8)
        UIView.transition(with: mainwindow, duration: 0, options: .transitionFlipFromLeft, animations: { () -> Void in
        }) { (finished) -> Void in
        }
    }
    @IBAction func showNearby(_ sender: Any) {
        if self.dateTextField.text == "" {
            HUD.flash(.label("Enter The Date"), delay: 1.0)
            return
        }
        if let date = dateTextField.text {
            userData.Instance.removeselectedDate()
            userData.Instance.selectedDate =  date
            UserDefaults.standard.set(date, forKey: "selectedDate")
        }
        request =  "http://service.beautytimes.org/api/home?latitude=\(coordinateValue.latitude)?longitude=\(coordinateValue.longitude)?type=\(serType)"
        print(request)
        
        let navVC = self.storyboard?.instantiateViewController(withIdentifier: "AllBeautyCenterViewController") as! AllBeautyCenterViewController
        print("sesrch ------\(request)")
        navVC.url = request
        self.navigationController?.pushViewController(navVC, animated: true)
    }
    @IBAction func menue(_ sender: Any) {
        if L102Language.currentAppleLanguage() == "en" {
            present(SideMenuManager.default.menuLeftNavigationController!, animated: true, completion: nil)
        }else{
            present(SideMenuManager.default.menuRightNavigationController!, animated: true, completion: nil)
        }
    }
    //radio btn ==> service location
    @IBAction private func logSelectedButton(radioButton : DLRadioButton) {
        let o = radioButton.selected()!.titleLabel!.text!
        print(String(format: "%@ is selected.\n", o));
        if o == "Outside"{
            serType = "outside"
        }else{
            serType = "inside"
        }
    }
}

//MARK:- Textfield Delegate
extension SearchViewController: UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.borderColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        textField.borderWidth = 1
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text == "" {
            textField.borderColor = #colorLiteral(red: 0.6642242074, green: 0.6642400622, blue: 0.6642315388, alpha: 1)
        }
    }
}
//MARK:- PickerView Delegate and DataSource
extension SearchViewController: UIPickerViewDelegate , UIPickerViewDataSource{
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == cityPicker {
            return city.count
        }
        if pickerView == departmentPicker {
            return departments.data?.count ?? 0
        }
        if pickerView == districtPicker {
            return districts.count
        }
        if pickerView == areaPicker {
            return areas.data?.count ?? 0
        }
        return 0
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == cityPicker {
            return "\(city[row].name ?? "")"
        }
        if pickerView == departmentPicker {
            return "\(departments.data?[row].name ?? "")"
        }
        if pickerView == districtPicker {
            return "\(districts[row].name ?? "")"
        }
        if pickerView == areaPicker {
            return "\(areas.data?[row].name ?? "")"
        }
        return ""
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == areaPicker {
            areaTextField.text = areas.data?[row].name ?? ""
            city = areas.data?[row].cities ?? [CitiesData]()
            cityTextField.isEnabled = false
            districtTextField.isEnabled = false
            cityTextField.text = ""
            districtTextField.text = ""
            if city.count > 0 {
                cityTextField.isEnabled = true
            }
        }
        if pickerView == cityPicker {
            cityTextField.text = city[row].name ?? ""
            districts = city[row].areas ?? [Areas]()
            districtTextField.isEnabled = false
            districtTextField.text = ""
            if districts.count > 0 {
                districtTextField.isEnabled = true
            }
        }
        if pickerView == districtPicker {
            districtTextField.text = districts[row].name ?? ""
            selectedAreaId = districts[row].id ?? 0
            
        }
        if pickerView == departmentPicker {
            departmentTextField.text = departments.data?[row].name ?? ""
            selectedDepartment = departments.data?[row].id ?? 0
        }
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
}
//MARK:- PickerView Methods
extension SearchViewController{
    func showAreaPicker(){
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donePicker1));
        let spaceButton1 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title:NSLocalizedString("Dismiss", comment: "إلغاء"), style: .plain , target: self, action: #selector(cancel1))
        toolbar.setItems([doneButton , spaceButton1 ,cancelButton], animated: false)
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        areaTextField.inputView = areaPicker
        areaTextField.inputAccessoryView = toolbar
    }
    func showDistrictPicker(){
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donePicker2));
        let spaceButton1 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title:NSLocalizedString("Dismiss", comment: "إلغاء"), style: .plain , target: self, action: #selector(cance2))
        toolbar.setItems([doneButton , spaceButton1 ,cancelButton], animated: false)
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        districtTextField.inputView = districtPicker
        districtTextField.inputAccessoryView = toolbar
    }
    func showDepartmentPicker(){
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donePicker33));
        let spaceButton1 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Dismiss", comment: ""), style: .plain , target: self, action: #selector(cancel33))
        toolbar.setItems([doneButton , spaceButton1 ,cancelButton], animated: false)
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        departmentTextField.inputView = departmentPicker
        departmentTextField.inputAccessoryView = toolbar
    }
    func showCityPicker(){
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donePicker));
        let spaceButton1 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Dismiss", comment: ""), style: .plain , target: self, action: #selector(cancel))
        toolbar.setItems([doneButton , spaceButton1 ,cancelButton], animated: false)
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        cityTextField.inputView = cityPicker
        cityTextField.inputAccessoryView = toolbar
    }
    @objc func cancel1(){
        areaTextField.text = ""
        self.view.endEditing(true)
    }
    @objc func cancel33(){
        departmentTextField.text = ""
        self.view.endEditing(true)
    }
    @objc func donePicker33(){
        if departmentTextField.text == "" {
            if departments.data?.count ?? 0 > 0 {
                departmentTextField.text = departments.data?[0].name ?? ""
                selectedDepartment = departments.data?[0].id ?? 0
            } else {
                self.dismiss(animated: true, completion: nil)
            }
        }
        self.view.endEditing(true)
    }
    @objc func donePicker1(){
        if areaTextField.text == "" {
            if areas.data?.count ?? 0 > 0 {
                areaTextField.text = areas.data?[0].name ?? ""
                city = areas.data?[0].cities ?? [CitiesData]()
                cityTextField.isEnabled = false
                districtTextField.isEnabled = false
                cityTextField.text = ""
                districtTextField.text = ""
                if city.count > 0 {
                    cityTextField.isEnabled = true
                }
            } else {
                self.dismiss(animated: true, completion: nil)
            }
        }
        self.view.endEditing(true)
    }
    @objc func donePicker2(){
        if districtTextField.text == "" {
            if districts.count > 0 {
                districtTextField.text = districts[0].name ?? ""
                selectedAreaId = districts[0].id ?? 0
            } else {
                self.dismiss(animated: true, completion: nil)
            }
        }
        self.view.endEditing(true)
    }
    @objc func cance2(){
        districtTextField.text = ""
        self.view.endEditing(true)
    }
    @objc func cancel(){
        cityTextField.text = ""
        self.view.endEditing(true)
    }
    @objc func donePicker(){
        if cityTextField.text == "" {
            if city.count > 0 {
                cityTextField.text = city[0].name ?? ""
                districts = city[0].areas ?? [Areas]()
                districtTextField.isEnabled = false
                districtTextField.text = ""
                if districts.count > 0 {
                    districtTextField.isEnabled = true
                }
                self.view.endEditing(true)
            } else {
                self.dismiss(animated: true, completion: nil)
            }
        }else {
            self.view.endEditing(true)
        }
    }
    func showDatePicker(){
        //Formate Date
        datePicker.datePickerMode = .date
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Dismiss", comment: "إلغاء"), style: .plain, target: self, action: #selector(cancelDatePicker));
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        dateTextField.inputAccessoryView = toolbar
        dateTextField.inputView = datePicker
    }
    @objc func donedatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        dateTextField.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
}
//MARK:- Network
extension SearchViewController{
    func getCitiesServer(){
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.showAreas() , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        print(err.message!)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.areas = try JSONDecoder().decode(Area.self, from: response.data!)
                    }catch{
                        let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                        print(error)
                        HUD.flash(.label(lockString), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    func getDepartment(){
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.getDepartments() , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        print(err.message!)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.departments = try JSONDecoder().decode(Department.self, from: response.data!)
                        print(value)
                    }catch{
                        let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                        print(error)
                        HUD.flash(.label(lockString), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
}
//MARK:- setupView
extension SearchViewController{
    func setupView(){
        navigationItem.hidesBackButton = true
        userData.Instance.fetchUser()
        self.title = NSLocalizedString("Search", comment:"ابحث")
        navigationItem.title = NSLocalizedString("Search", comment:"ابحث")
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        districtTextField.isEnabled = false
        locationManager.startUpdatingLocation()
        userData.Instance.fetchUser()
        if L102Language.currentAppleLanguage() == "ar" {
            cityLabel.textAlignment = .right
            areaLbl.textAlignment = .right
            districtionLabel.textAlignment = .right
            dateLabel.textAlignment = .right
            serviceLocationLabel.textAlignment = .right
        }
        setupSideMenu()
        areaTextField.delegate = self
        districtPicker.dataSource = self
        districtPicker.backgroundColor = .white
        districtPicker.showsSelectionIndicator = true
        districtPicker.delegate = self
        cityPicker.dataSource = self
        cityPicker.backgroundColor = .white
        cityPicker.showsSelectionIndicator = true
        cityPicker.delegate = self
        areaPicker.delegate = self
        areaPicker.delegate = self
        areaPicker.dataSource = self
        areaPicker.backgroundColor = .white
        areaPicker.showsSelectionIndicator = true
        datePicker.backgroundColor = .white
        districtTextField.delegate = self
        cityTextField.delegate = self
        departmentPicker.delegate = self
        departmentPicker.delegate = self
        departmentPicker.dataSource = self
        departmentPicker.backgroundColor = .white
        departmentTextField.delegate = self
        departmentPicker.showsSelectionIndicator = true;
        departmentTextField.addPadding(UITextField.PaddingSide.left(20))
        areaTextField.addPadding(UITextField.PaddingSide.left(20))
        districtTextField.addPadding(UITextField.PaddingSide.left(20))
        cityTextField.addPadding(UITextField.PaddingSide.left(20))
        departmentTextField.setRightViewFAIcon(icon: .FAAngleDown, rightViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
        districtTextField.setRightViewFAIcon(icon: .FAAngleDown, rightViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
        hiddentextField.setRightViewFAIcon(icon: .FAAngleDown, rightViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
        cityTextField.setRightViewFAIcon(icon: .FAAngleDown, rightViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
        areaTextField.setRightViewFAIcon(icon: .FAAngleDown, rightViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
        showCityPicker()
        showAreaPicker()
        showDistrictPicker()
        showDatePicker()
        getCitiesServer()
        showDepartmentPicker()
        getDepartment()
    }
}
