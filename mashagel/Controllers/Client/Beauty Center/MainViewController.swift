//
//  MainViewController.swift
//  mashagel
//
//  Created by MACBOOK on 9/27/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//

import UIKit
import PKHUD
import FCAlertView
import SJSegmentedScrollView
import Alamofire

class MainViewController: SJSegmentedViewController {
    var shopTitle = String()
    var selectedSegment: SJSegmentTab?
    var shopData = ShopData()
    var id = Int()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.backBarButtonItem?.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationController?.navigationBar.backItem?.backBarButtonItem?.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let textAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.8558626771, green: 0.2503757179, blue: 0.4763913751, alpha: 1)
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0.8558626771, green: 0.2503757179, blue: 0.4763913751, alpha: 1)
        //        userData.Instance.fetchUser()
        //        print("eeeeeee")
        //         let currentID = userData.Instance.userSelectedShopId as! Int
        //  loadShopData(id: currentID)
        //     setUp_segment()
    }
    override func viewDidLoad() {
        let textAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.8558626771, green: 0.2503757179, blue: 0.4763913751, alpha: 1)
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0.8558626771, green: 0.2503757179, blue: 0.4763913751, alpha: 1)
        setUp_segment()
        title = shopTitle
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = false
    }
    
    // MARK: - SETUP SEGMENT
    func setUp_segment()  {
        if L102Language.currentAppleLanguage() == "en" {
            if let storyboard = self.storyboard {
                let headerController = storyboard
                    .instantiateViewController(withIdentifier: "HeaderViewController1") as! HeaderViewController
                headerController.shopData = shopData
                
                let firstViewController = storyboard
                    .instantiateViewController(withIdentifier: "FirstTableViewController") as! FirstTableViewController
                firstViewController.title = "Services"
                firstViewController.shopData = shopData
                //                    as? OffersInMainViewController1
                //                firstViewController?.title = "Services"
                //                firstViewController?.shopData = shopData
                
                let secondViewController = storyboard
                    .instantiateViewController(withIdentifier: "SecondTableViewController") as? SecondTableViewController
                secondViewController?.title = "Offers"
                secondViewController?.shopData = shopData
                
                let thirdViewController = storyboard
                    .instantiateViewController(withIdentifier: "AboutInMainViewController") as? AboutInMainViewController
                thirdViewController?.title = "About Center"
                
                let fourthViewController = storyboard
                    .instantiateViewController(withIdentifier: "CustomerOpinionsInMainViewController") as? CustomerOpinionsInMainViewController
                fourthViewController?.title = "Reviews"
                headerViewController = headerController
                segmentControllers = [firstViewController,
                                      secondViewController,
                                      thirdViewController,
                                      fourthViewController] as! [UIViewController]
                headerViewHeight = 200
                selectedSegmentViewHeight = 5.0
                headerViewOffsetHeight = 0.0
                segmentTitleColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
                selectedSegmentViewColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
                segmentShadow = SJShadow.light()
                showsHorizontalScrollIndicator = false
                showsVerticalScrollIndicator = false
                segmentBounces = false
                delegate = self
            }
        }
        if L102Language.currentAppleLanguage() == "ar" {
            if let storyboard = self.storyboard {
                let headerController = storyboard
                    .instantiateViewController(withIdentifier: "HeaderViewController1") as! HeaderViewController
                headerController.shopData = shopData
                
                let fourthViewController = storyboard
                    .instantiateViewController(withIdentifier: "FirstTableViewController") as? FirstTableViewController

                fourthViewController?.shopData = shopData
                
                let secondViewController  = storyboard
                    .instantiateViewController(withIdentifier: "SecondTableViewController") as? SecondTableViewController
                secondViewController?.shopData = shopData
                
                let thirdViewController = storyboard
                    .instantiateViewController(withIdentifier: "AboutInMainViewController") as? AboutInMainViewController
                
                let firstViewController = storyboard
                    .instantiateViewController(withIdentifier: "CustomerOpinionsInMainViewController") as? CustomerOpinionsInMainViewController
                fourthViewController?.title = "أراء العملاء"
                firstViewController?.title = "الخدمات"
                secondViewController?.title = "عن المركز"
                thirdViewController?.title = "العروض"

                headerViewController = headerController
                segmentControllers = [firstViewController,
                                      thirdViewController,
                                      secondViewController,
                                      fourthViewController] as! [UIViewController]
                headerViewHeight = 200
                selectedSegmentViewHeight = 5.0
                headerViewOffsetHeight = 0.0
                segmentTitleColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
                selectedSegmentViewColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
                segmentShadow = SJShadow.light()
                showsHorizontalScrollIndicator = false
                showsVerticalScrollIndicator = false
                segmentBounces = false
                delegate = self
            }
        }
    }
    func getSegmentTabWithImage(_ imageName: String) -> UIView {
        let view = UIImageView()
        view.frame.size.width = 100
        view.image = UIImage(named: imageName)
        view.contentMode = .scaleAspectFit
        view.backgroundColor = .white
        return view
    }
}

extension MainViewController: SJSegmentedViewControllerDelegate {
    func didMoveToPage(_ controller: UIViewController, segment: SJSegmentTab?, index: Int) {
        if selectedSegment != nil {
            selectedSegment?.titleColor(.lightGray)
        }
        if segments.count > 0 {
            selectedSegment = segments[index]
            selectedSegment?.titleColor(.red)
        }
    }
}




