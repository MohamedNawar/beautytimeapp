//
//  AboutInMainViewController.swift
//  mashagel
//
//  Created by MACBOOK on 9/27/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//

import UIKit
import PKHUD
import Alamofire
import FCAlertView
import SJSegmentedScrollView

class AboutInMainViewController:  UIViewController{
    @IBOutlet weak var firstStackView : UIStackView!
    @IBOutlet weak var secondStackView : UIStackView!
    @IBOutlet weak var thirdStackView : UIStackView!
    @IBOutlet weak var fourthStackView : UIStackView!
    @IBOutlet weak var tableView: UITableView!
    
    var shopData = Shop(){
        didSet{
            let name = tableView.viewWithTag(110) as! UILabel
            if L102Language.currentAppleLanguage() == "ar" {
                firstStackView.semanticContentAttribute = .forceRightToLeft
                name.textAlignment = .right
            }
            let name1 = tableView.viewWithTag(1010) as! UILabel
            if L102Language.currentAppleLanguage() == "ar" {
                name1.textAlignment = .right
                secondStackView.semanticContentAttribute = .forceRightToLeft
            }
            let title = tableView.viewWithTag(44) as! UILabel
            title.text = shopData.data?.address
            if L102Language.currentAppleLanguage() == "ar" {
                title.textAlignment = .right
                thirdStackView.semanticContentAttribute = .forceRightToLeft
            }
            let title1 = tableView.viewWithTag(45) as! UILabel
            title1.text = shopData.data?.mobile
            if L102Language.currentAppleLanguage() == "ar" {
                title1.textAlignment = .right
                fourthStackView.semanticContentAttribute = .forceRightToLeft
            }
            self.tableView.reloadData()
            self.title = NSLocalizedString("About Shop", comment: "عن المحل")
        }
    }
    
   override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.changeFont()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        tableView.allowsSelection = false
        userData.Instance.fetchUser()
        let currentID = userData.Instance.userSelectedShopId!
        print(userData.Instance.userSelectedShopId ?? "")
        loadShopData(id: currentID)
        tableView.separatorStyle = .none
        tableView.allowsSelection = false
    }
}

extension AboutInMainViewController: SJSegmentedViewControllerViewSource {
    func viewForSegmentControllerToObserveContentOffsetChange() -> UIView {
        return tableView
    }
}
//MARK:- Network
extension AboutInMainViewController{
    func loadShopData(id:Int){
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        print(APIs.Instance.showShop(id:id))
        Alamofire.request(APIs.Instance.showShop(id:id) , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "img34"))
                        print(err.message!)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.shopData = try JSONDecoder().decode(Shop.self, from: response.data!)
                    }catch{
                        print(error)
                      //  HUD.flash(.label("Error Try Again"), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                HUD.flash(.label("Error Try Again Later"), delay: 1.0)
                break
            }
        }
    }
}
//MARK:- TableView Delegate and DataSource
extension AboutInMainViewController: UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(shopData.data?.schedule?.count ?? 0)
        return shopData.data?.schedule?.count ?? 7
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AboutCell", for: indexPath)
        let title = cell.viewWithTag(3) as! UILabel
        let title1 = cell.viewWithTag(2) as! UILabel
        let title11 = cell.viewWithTag(10) as! UILabel
        let title111 = cell.viewWithTag(20) as! UILabel
        if L102Language.currentAppleLanguage() == "ar" {
            title11.textAlignment = .right
            title111.textAlignment = .right
            title1.textAlignment = .right
            title.textAlignment = .right
        }
        if let titleValue = shopData.data?.schedule?[indexPath.row].day_of_week {
            let title = cell.viewWithTag(1) as! UILabel
            if L102Language.currentAppleLanguage() == "ar" {
                title.textAlignment = .right
            }
            switch titleValue {
            case "1":
                if L102Language.currentAppleLanguage() == "ar"{
                    title.text = "السبت"
                }else{
                    title.text = NSLocalizedString("Saturday", comment:"السبت")
                }
            case "2":
                if L102Language.currentAppleLanguage() == "ar"{
                    title.text = "الأحد"
                }else{
                    title.text = NSLocalizedString("Sunday", comment:"الأحد")
                }
            case "3":
                if L102Language.currentAppleLanguage() == "ar"{
                    title.text = "الأثنين"
                }else{
                    title.text = NSLocalizedString("Monday", comment:"الأثنين")
                }
            case "4":
                if L102Language.currentAppleLanguage() == "ar"{
                    title.text = "الثلاثاء"
                }else{
                    title.text = NSLocalizedString("Tuesday", comment:"الثلاثاء")
                }
            case "5":
                if L102Language.currentAppleLanguage() == "ar"{
                    title.text = "الأربعاء"
                }else{
                    title.text = NSLocalizedString("Wednesday", comment:"الأربعاء")
                }
            case "6":
                if L102Language.currentAppleLanguage() == "ar"{
                    title.text = "الخميس"
                }else{
                    title.text = NSLocalizedString("Thursday", comment:"الخميس")
                }
            case "7":
                if L102Language.currentAppleLanguage() == "ar"{
                    title.text = "الجمعة"
                }else{
                    title.text = NSLocalizedString("Friday", comment:"الجمعة")
                }
            default:
                title.text = "\(titleValue)"
            }
        }
        if let titleValue = shopData.data?.schedule?[indexPath.row] {
            if L102Language.currentAppleLanguage() == "ar" {
                title.textAlignment = .right
            }
            if let from = titleValue.from_inside{
                if let to = titleValue.to_inside{
                    print(from)
                    if L102Language.currentAppleLanguage() == "ar"{
                        title.text =
                        "من الساعة\(from) إلي الساعة\(to) "
                    }else{
                        title.text = "from \(from) to \(to)"
                    }
                }
            }
            if let from = titleValue.from_outside{
                if let to = titleValue.to_outside{
                    if L102Language.currentAppleLanguage() == "ar"{
                        title1.text =  "من الساعة \(from) إلي الساعة \(to) "                    }else{
                        title1.text = NSLocalizedString("from \(from) to \(to) ", comment:   "من الساعة\(from) إلي الساعة\(to)")
                    }
                    
                }
            }
        }
        return cell
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        _ = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
}
