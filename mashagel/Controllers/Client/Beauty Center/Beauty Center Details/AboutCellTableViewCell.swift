//
//  AboutCellCollectionViewCell.swift
//  mashagel
//
//  Created by ElNoorOnline on 9/12/19.
//  Copyright © 2019 MuhammedAli. All rights reserved.
//

import UIKit

class AboutCellTableViewCell: UITableViewCell {
    @IBOutlet weak var theview : UIView!
    
    override func awakeFromNib() {
        if L102Language.currentAppleLanguage() == "ar" {
            theview.semanticContentAttribute = .forceRightToLeft
        }else {
            theview.semanticContentAttribute = .forceLeftToRight
        }
    }
}
