//
//  CustomerOpinionsInMainViewController.swift
//  mashagel
//
//  Created by MACBOOK on 9/27/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//

import UIKit
import PKHUD
import Alamofire
import FCAlertView
import SDWebImage
import HCSStarRatingView
import SJSegmentedScrollView

class CustomerOpinionsInMainViewController: UIViewController{

    var shopData = Shop(){
        didSet{
            rateLbl.text = "\(shopData.data?.review_count ?? 0) Rattings"
            if L102Language.currentAppleLanguage() == "ar" {
                rateLbl.text = "\(shopData.data?.review_count ?? 0) تقييم"
            }
            let rateValue = shopData.data?.review_average ?? ""
            if let n = NumberFormatter().number(from: rateValue) {
                let f = CGFloat(truncating: n)
                rate.value = f
            }
            self.tableView.reloadData()
        }
    }
    
    @IBOutlet weak var rateLbl: UILabel!
    @IBOutlet weak var rate: HCSStarRatingView!
    @IBOutlet weak var tableView: UITableView!
    
   override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.changeFont()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        tableView.allowsSelection = false
        userData.Instance.fetchUser()
        let currentID = userData.Instance.userSelectedShopId!
        print(userData.Instance.userSelectedShopId ?? "")
        loadShopData(id: currentID)
        self.title = NSLocalizedString("Reviews", comment: "أراء العملا")
    }
    func viewForObserve() -> UIView{
        return self.tableView
    }
    
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        _ = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
}

extension CustomerOpinionsInMainViewController: SJSegmentedViewControllerViewSource {
    func viewForSegmentControllerToObserveContentOffsetChange() -> UIView {
        return tableView
    }
}
//MARK:- TableView delegate and DataSource
extension CustomerOpinionsInMainViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return shopData.data?.review?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomerOpinionsCell", for: indexPath)
        if let nameValue = shopData.data?.review?[indexPath.row].name{
            let name = cell.viewWithTag(1) as! UILabel
            name.text = "\(nameValue)"
        }
        if let messageValue = shopData.data?.review?[indexPath.row].message {
            let message = cell.viewWithTag(2) as! UILabel
            if L102Language.currentAppleLanguage() == "ar" {
                message.textAlignment = .right
            }
            message.text = "\(messageValue )"
        }
        if let rateValue = shopData.data?.review?[indexPath.row].review {
            let ratting = cell.viewWithTag(10) as! HCSStarRatingView
            ratting.value = rateValue.CGFloatValue() ?? 0.0
        }
        if let dateValue = shopData.data?.review?[indexPath.row].created_at {
            let date = cell.viewWithTag(8) as! UILabel
            var stringValue = dateValue.date
            stringValue = stringValue?.components(separatedBy: ".")[0]
            print(stringValue ?? "")
            date.text = "\(stringValue ?? "")"
        }
        //        if let  imageIndex = (reviewData.data?[indexPath.row].image?.large) {
        //            let image = cell.viewWithTag(5) as! UIImageView
        //            image.sd_setImage(with: URL(string: imageIndex), completed: nil)
        //        }
        
        return cell
    }
}
//MARK:- Network
extension CustomerOpinionsInMainViewController{
    func loadShopData(id:Int){
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        print(APIs.Instance.showShop(id:id))
        Alamofire.request(APIs.Instance.showShop(id:id) , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "img34"))
                        print(err.message!)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.shopData = try JSONDecoder().decode(Shop.self, from: response.data!)
                    }catch{
                        print(error)
                      //  HUD.flash(.label("Error Try Again"), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
}
