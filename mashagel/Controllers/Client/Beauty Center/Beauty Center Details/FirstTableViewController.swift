//
//  FirstViewController.swift
//  mashagel
//
//  Created by ElNoorOnline on 4/24/19.
//  Copyright © 2019 MuhammedAli. All rights reserved.
//

import UIKit
import FCAlertView
import Alamofire
import PKHUD
import SJSegmentedScrollView

class FirstTableViewController: UITableViewController {
    @IBOutlet weak var serviceType: UISegmentedControl!

    var serviceReservation = Reservation()
    var currentServiceID = Int()
    var shopID = Int()
    var currentID  = Int()
    var serType = "inside"
    var contentCount = 0
    var headerHeigh = 0
    var FooterHeigh = 0
    var reservationIdentifier = Int()
    var shopData = ShopData(){
        didSet{
            for item in shopData.services! {
                if item.type?.SString == serType {
                    if new_shopData.keys.contains(item.category?.name ?? "")  {
                        new_shopData[(item.category?.name) ?? ""]?.append(item)
                    }else{
                        new_shopData[(item.category?.name) ?? ""] = [item]
                    }
                }
            }
        }
    }
    var new_shopData : [String: [ServicesData]] = [:]
    
    override func viewDidLoad() {
            super.viewDidLoad()
            userData.Instance.fetchUser()
            currentID = userData.Instance.userSelectedShopId!
        }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.backBarButtonItem?.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                self.navigationController?.navigationBar.backItem?.backBarButtonItem?.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        if L102Language.currentAppleLanguage() == "ar" {
            serviceType.setTitle("في ضيافتنا", forSegmentAt: 0)
            serviceType.setTitle("بالمنزل", forSegmentAt: 1)
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        let shopID = userData.Instance.userSelectedShopId as! Int
//
//        reservationIdentifier = userData.Instance.identifierInside ?? -1
//        if reservationIdentifier == nil || reservationIdentifier == -1{
//            PreparingReservationShopData(id:shopID, type: true)
//        }else{
//            PreparingReservationShopData(id:shopID, type: false)
//        }
//        self.tableView.reloadData()
//
    }
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        refreshControl?.addTarget(self,
//                                  action: #selector(handleRefresh(_:)),
//                                  for: UIControl.Event.valueChanged)
//    }
    
//    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
//
//        self.perform(#selector(self.endRefresh), with: nil, afterDelay: 1.0)
//    }
//
//    @objc func endRefresh() {
//
//        refreshControl?.endRefreshing()
//    }
//
//    override func numberOfSections(in tableView: UITableView) -> Int {
//        return 1
//    }
//
//    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return 25
//    }
//
//    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "CellIdentifier", for: indexPath)
//
//        cell.textLabel?.text = "Row " + String((indexPath as NSIndexPath).row)
//
//        return cell
//    }
    
    func viewForObserve() -> UIView{
        return self.tableView
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        alert.colorScheme = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
    @IBAction func toggle(_ sender: Any) {
        if serviceType.selectedSegmentIndex == 1 {
            serType = "outside"
        }else {
            serType = "inside"
        }
        new_shopData.removeAll()
        for item in shopData.services! {
            if item.type?.SString == serType {
                if new_shopData.keys.contains(item.category?.name ?? "")  {
                    new_shopData[(item.category?.name) ?? ""]?.append(item)
                }else{
                    new_shopData[(item.category?.name) ?? ""] = [item]
                }
            }
        }
        self.tableView.reloadData()
    }
}
//MARK:- TableView CellDelegate
extension FirstTableViewController : TableViewCellDelegate{
    override func numberOfSections(in tableView: UITableView) -> Int {
        return new_shopData.count
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let category_name = Array(new_shopData)[section].value[0]
        let is_opened = category_name.flag ?? false
        if is_opened {
            print("****\(Array(new_shopData)[section].value.count)")
            return Array(new_shopData)[section].value.count + 1
        }else {
            return 1
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = Bundle.main.loadNibNamed("OfferSectionHeader", owner: self, options: nil)?.first as! OfferSectionHeader
            //  cell.configure_cell(data: shopData.services?[indexPath.section] ?? ServicesData())
            let category_name = Array(new_shopData)[indexPath.section].key
            cell.configure_cell(data: category_name)
          //  let services = shopData.services
            return cell
        }else{
            let cell = Bundle.main.loadNibNamed("OfferCollapseCell", owner: self, options: nil)?.first as! OfferCollapseCell
            let labl = cell.viewWithTag(10) as! UILabel
            if L102Language.currentAppleLanguage() == "ar" {
                labl.textAlignment = .right
                labl.text = "عدد الأشخاص"
            }
            //    cell.configure_cell(data: shopData.services?[indexPath.section] ?? ServicesData())
            let category_name = Array(new_shopData)[indexPath.section].value[indexPath.row - 1]
            cell.delegate = self
            //            cell.tag = Array(new_shopData)[indexPath.section].value[indexPath.row - 1].id ?? -1
            //            cell.addService_btn.addTarget(self, action: #selector(addService), for: .touchUpInside)
            cell.configure_cell(data: category_name)
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 44
        }else {
            return 240
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            if Array(new_shopData)[indexPath.section].value[0].flag == true {
                Array(new_shopData)[indexPath.section].value[0].flag = false
            }else{
                Array(new_shopData)[indexPath.section].value[0].flag = true
            }
            let section = IndexSet.init(integer: indexPath.section)
            tableView.reloadSections(section, with: .none)
        }
    }
    
    // MARK: - add btn
    func didAddService(_ sender: OfferCollapseCell) {
        guard let tappedIndexPath = tableView.indexPath(for: sender) else { return }
        let cell1 =  tableView.cellForRow(at: tappedIndexPath) as! OfferCollapseCell
        currentServiceID = Array(new_shopData)[tappedIndexPath.section].value[tappedIndexPath.row-1].id ?? 0
        self.addCurrentService(qty:cell1.numOfPerson_lbl.text!)
        print("+++++\(tappedIndexPath.row)++++\(tappedIndexPath.section)++++\(cell1.numOfPerson_lbl.text!)")
    }
    
    @objc func addService(_ sender: UIButton){
        
    }
    
    func makeCannotReserveAlert1(title: String, SubTitle: String, Image : UIImage , qty:String) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        alert.colorScheme = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        alert.addButton(NSLocalizedString("Go To Reservations", comment: "") ) {
            let navVC = self.storyboard?.instantiateViewController(withIdentifier: "Cart1ViewController") as! Cart1ViewController
            self.navigationController?.pushViewController(navVC, animated: true)
        }
        alert.addButton(NSLocalizedString("Continue Current Reservations", comment: "")) {
            userData.Instance.fetchUser()
            if userData.Instance.serviceType == "inside" {
                userData.Instance.removeIdentifierInside()
                userData.Instance.removeidentifierOutside()
                let shopID = userData.Instance.userSelectedShopId!
                UserDefaults.standard.set(shopID , forKey: "currentReserveShopId")
                self.PreparingReservationShopData11(id:shopID, type: true , qty:qty)
            }else{
                userData.Instance.removeidentifierOutside()
                userData.Instance.removeidentifierOutside()
                let shopID = userData.Instance.userSelectedShopId!
                UserDefaults.standard.set(shopID , forKey: "currentReserveShopId")
                self.PreparingReservationShopData11(id:shopID, type: true, qty:qty)
            }
        }
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Dismiss", comment: "إلغاء"), andButtons: nil)
    }
    private func addCurrentService(qty:String){
        if serType == "outside" {
            userData.Instance.fetchUser()
            self.reservationIdentifier = userData.Instance.identifierInside ?? -1
            let shopID = userData.Instance.userSelectedShopId!
            var reservationShopId = userData.Instance.currentReserveShopId!
            if reservationShopId == nil{
                reservationShopId = shopID
            }
            var serviceType = userData.Instance.serviceType
            if serviceType == nil {
                serviceType = "outside"
                UserDefaults.standard.setValue("outside", forKey: "serviceType")
            }
            print(reservationShopId)
            print(shopID)
            userData.Instance.fetchUser()
            
            let ser = userData.Instance.serviceType
            print(ser ?? "")
            print(serType)
            if shopID != reservationShopId || ser  != serType  {
                makeCannotReserveAlert1(title: "", SubTitle: NSLocalizedString("You cannot add any service or package from another shop you have to confirm the previous reservation or continue with a new one", comment: "") , Image: #imageLiteral(resourceName: "img34") , qty:qty)
            }else{
                reservationIdentifier = userData.Instance.identifierOutside ?? -1
                if reservationIdentifier == -1{
                    PreparingReservationShopData(id:shopID, type: true)
                }else{
                    PreparingReservationShopData(id:shopID, type: false)
                }
                userData.Instance.fetchUser()
                let userIdentifier = userData.Instance.identifierOutside
                self.AddService(serviceId: self.currentServiceID, userIdentifier: userIdentifier ?? -1 , serviceType: self.serType, quantity: qty)
                userData.Instance.removeserviceType()
                userData.Instance.serviceType = "outside"
            }
        }
        else{
            userData.Instance.fetchUser()
            self.reservationIdentifier = userData.Instance.identifierInside ?? -1
            let shopID = userData.Instance.userSelectedShopId!
            var reservationShopId = userData.Instance.currentReserveShopId
            var serviceType = userData.Instance.serviceType
            if serviceType == nil {
                serviceType = "inside"
                UserDefaults.standard.setValue("inside", forKey: "serviceType")
            }
            if reservationShopId == nil{
                reservationShopId = shopID
            }
            userData.Instance.fetchUser()
            let ser = userData.Instance.serviceType
            if shopID != reservationShopId || ser  != serType {
                makeCannotReserveAlert1(title: "", SubTitle: NSLocalizedString("You cannot add any service or package from another shop you have to confirm the previous reservation or continue with a new one", comment: ""), Image: #imageLiteral(resourceName: "img34"), qty: qty)
            }else{
                reservationIdentifier = userData.Instance.identifierInside ?? -1
                print(reservationIdentifier)
                if reservationIdentifier == -1{
                    PreparingReservationShopData(id:shopID, type: true)
                }else{
                    PreparingReservationShopData(id:shopID, type: false)
                }
                userData.Instance.fetchUser()
                let userIdentifier = userData.Instance.identifierInside
                self.AddService(serviceId: self.currentServiceID, userIdentifier: userIdentifier ?? -1 , serviceType: self.serType, quantity: qty)
                userData.Instance.removeserviceType()
                userData.Instance.serviceType = "inside"
            }
        }
        print(userData.Instance.serviceType ?? "")
    }
}
//MARK:- Network
extension FirstTableViewController{
    func PreparingReservationShopData(id:Int , type:Bool){
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        print(header)
        print(APIs.Instance.CurrentPreparingReservation(id: id))
        Alamofire.request(APIs.Instance.CurrentPreparingReservation(id: id) , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        print(err.message!)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.serviceReservation = try JSONDecoder().decode(Reservation.self, from: response.data!)
                        if type {
                            if self.serType == "outside" {
                                if let identifier = self.serviceReservation.data?.id {
                                    userData.Instance.identifierOutside =  identifier
                                    print(identifier)
                                    UserDefaults.standard.set(identifier , forKey: "identifierOutside")
                                }
                            }else{
                                if let identifier = self.serviceReservation.data?.id {
                                    userData.Instance.identifierInside =  identifier
                                    print(identifier)
                                    UserDefaults.standard.set(identifier , forKey: "identifierInside")
                                }
                            }
                        }
                        print(self.serType)
                    }catch{
                        print(error)
                      //  HUD.flash(.label("Error Try Again"), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    func PreparingReservationShopData1(id:Int , type:Bool){
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.CurrentPreparingReservation(id: id) , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        print(err.message!)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.serviceReservation = try JSONDecoder().decode(Reservation.self, from: response.data!)
                        if type {
                            if self.serType == "outside" {
                                if let identifier = self.serviceReservation.data?.id {
                                    userData.Instance.identifierOutside =  identifier
                                    print(identifier)
                                    UserDefaults.standard.set(identifier , forKey: "identifierOutside")
                                }
                            }else{
                                if let identifier = self.serviceReservation.data?.id {
                                    userData.Instance.identifierInside =  identifier
                                    print(identifier)
                                    UserDefaults.standard.set(identifier , forKey: "identifierInside")
                                }
                            }
                        }
                        print(self.serType)
                    }catch{
                        print(error)
                      //  HUD.flash(.label("Error Try Again"), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    func PreparingReservationShopData11(id:Int , type:Bool, qty:String){
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.CurrentPreparingReservation(id: id) , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        print(err.message!)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.serviceReservation = try JSONDecoder().decode(Reservation.self, from: response.data!)
                        if type {
                            if self.serType == "outside" {
                                if let identifier = self.serviceReservation.data?.id {
                                    userData.Instance.identifierOutside =  identifier
                                    print(identifier)
                                    UserDefaults.standard.set(identifier , forKey: "identifierOutside")
                                    userData.Instance.fetchUser()
                                    self.reservationIdentifier = userData.Instance.identifierOutside ?? -1
                                    self.AddService(serviceId: self.currentServiceID, userIdentifier: self.reservationIdentifier , serviceType: self.serType, quantity: qty)
                                    
                                }
                            }else{
                                if let identifier = self.serviceReservation.data?.id {
                                    userData.Instance.identifierInside =  identifier
                                    print(identifier)
                                    UserDefaults.standard.set(identifier , forKey: "identifierInside")
                                    userData.Instance.fetchUser()
                                    self.reservationIdentifier = userData.Instance.identifierInside ?? -1
                                    self.AddService(serviceId: self.currentServiceID, userIdentifier: self.reservationIdentifier , serviceType: self.serType, quantity: qty)
                                }
                            }
                        }
                        print(self.serType)
                    }catch{
                        print(error)
                      //  HUD.flash(.label("Error Try Again"), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    func AddService(serviceId:Int , userIdentifier:Int , serviceType:String , quantity:String){
        let header = APIs.Instance.getHeader()
        print(header)
        let shopID = userData.Instance.userSelectedShopId!
        print(APIs.Instance.AddService(id: serviceId, identifier: userIdentifier, serviceType: serviceType, quantity: quantity, shopId: shopID))
        HUD.show(.progress, onView: self.view)
        print(APIs.Instance.AddService(id: serviceId, identifier: userIdentifier, serviceType: serviceType, quantity: quantity, shopId: shopID))
        Alamofire.request(APIs.Instance.AddService(id: serviceId, identifier: userIdentifier, serviceType: serType , quantity: quantity, shopId: shopID), method: .post, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    print(temp)
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        print(err.message!)
                        self.makeCannotReserveAlert1(title: "", SubTitle: NSLocalizedString("You cannot add any service or package from another shop you have to confirm the previous reservation or continue with a new one", comment: "") , Image: #imageLiteral(resourceName: "img34") , qty:quantity)
                    }catch{
                    }
                }else{
                    do {
                        self.serviceReservation = try JSONDecoder().decode(Reservation.self, from: response.data!)
                        print(userData.Instance.serviceType ?? "")
                        userData.Instance.removeserviceType()
                        print(self.serType )
                        if self.serType == "outside" {
                            UserDefaults.standard.set(self.serType , forKey: "serviceType")
                        }else{
                            UserDefaults.standard.set("inside" , forKey: "serviceType")
                        }
                        userData.Instance.removecurrentReserveShopId()
                        let shopID = userData.Instance.userSelectedShopId!
                        UserDefaults.standard.set(shopID , forKey: "currentReserveShopId")
                    }catch{
                        print(error)
                      //  HUD.flash(.label("Error Try Again"), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
}
