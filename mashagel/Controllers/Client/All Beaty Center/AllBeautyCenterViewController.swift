//
//  AllBeautyCenterViewController.swift
//  mashagel
//
//  Created by MACBOOK on 9/29/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD
import FCAlertView
import SDWebImage
import HCSStarRatingView
import SideMenu

class AllBeautyCenterViewController : UIViewController{
    var shopes = Shops(){
        didSet{
            if (shopes.data?.count) ?? 0 > 0 {
                self.tableView.reloadData()
            }
        }
    }
    var id = Int()
    var url = String(){
        didSet{
            print(url)
            loadShopsData()
        }
    }
    @IBOutlet weak var tableView: UITableView!
    
   override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.changeFont()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.tabBarController?.tabBar.isHidden = true
        setupSideMenu()
        self.title = NSLocalizedString("Beauty Center", comment: "")
        tableView.separatorStyle = .none
        tableView.delegate = self
        tableView.dataSource = self
        self.tableView.reloadData()
        userData.Instance.fetchUser()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
                  let statusBarColor = #colorLiteral(red: 0.8558626771, green: 0.2503757179, blue: 0.4763913751, alpha: 1)
                  statusBarView.backgroundColor = statusBarColor
                  view.addSubview(statusBarView)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.8558626771, green: 0.2503757179, blue: 0.4763913751, alpha: 1)
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0.8558626771, green: 0.2503757179, blue: 0.4763913751, alpha: 1)
        self.tabBarController?.tabBar.isHidden = false
        if L102Language.currentAppleLanguage() == "ar"{
            self.title = "مراكز التجميل"
        }else{
            self.title = NSLocalizedString("Beauty Center", comment: "")
        }
    }
    
    
    
    func goToMainScreen(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "TabBarControllerViewController") as! TabBarControllerViewController
        self.present(vc, animated: true, completion: nil)
    }
    func loadShopsData(){
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        Alamofire.request(url , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "img34"))
                        print(err.message!)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.shopes = try JSONDecoder().decode(Shops.self, from: response.data!)
                        print(self.shopes)
                    }catch{
                        print(error)
                      //  HUD.flash(.label("Error Try Again"), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        _ = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "map" {
            let destination = segue.destination as!MapViewController
            destination.url = url
        }
    }
    
    @IBAction func changeLanguage(_ sender: Any) {
        if L102Language.currentAppleLanguage() == "ar"{
            L102Language.setAppleLAnguageTo(lang: "en")
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
        } else {
            L102Language.setAppleLAnguageTo(lang: "ar")
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
        };
       restartApplication ()
        }
        func restartApplication () {
                print(L102Language.currentAppleLanguage())
                Bundle.setLanguage(L102Language.currentAppleLanguage())
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
                UIApplication.shared.keyWindow?.rootViewController = storyboard.instantiateInitialViewController()
            }
    @IBAction func menue(_ sender: Any) {
        if L102Language.currentAppleLanguage() == "en" {
            present(SideMenuManager.default.menuLeftNavigationController!, animated: true, completion: nil)
        }else{
            present(SideMenuManager.default.menuRightNavigationController!, animated: true, completion: nil)
        }
    }
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
//MARK:- TableView Delegate and DataSource
extension AllBeautyCenterViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return shopes.data?.count ?? 0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        id = shopes.data?[indexPath.row].id ?? 0
        if let ShopID = shopes.data?[indexPath.row].id {
            // MARK:- go to shop details
            call_api(id: ShopID)
            let currentID = ShopID
            userData.Instance.removeShopID()
            userData.Instance.userSelectedShopId =  currentID
            UserDefaults.standard.set(currentID, forKey: "userSelectedShopId")
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AllBeautyCenterCell", for: indexPath)
        let address = cell.viewWithTag(2) as! UILabel
        if let nameValue = shopes.data?[indexPath.row].name {
            let name = cell.viewWithTag(1) as! UILabel
            name.text = "\(nameValue)"
            if L102Language.currentAppleLanguage() == "ar" {
                name.textAlignment = .right
            }
        }
        if L102Language.currentAppleLanguage() == "ar" {
            address.textAlignment = .right
        }
        if let addressValue = shopes.data?[indexPath.row].address {
            address.text = "\(addressValue)"
        }
        if let rateAverageValue = shopes.data?[indexPath.row].review_average {
            let rateAverage = cell.viewWithTag(8) as! UILabel
            let ratting = cell.viewWithTag(10) as! HCSStarRatingView
            
            if let n = NumberFormatter().number(from: rateAverageValue) {
                let f = CGFloat(truncating: n)
                ratting.value = f
                rateAverage.text = "\(f)"
            }
        }
        if let DISTANCEValue = shopes.data?[indexPath.row].distance?.SString {
            let distance = cell.viewWithTag(44) as! UILabel
            if let n = NumberFormatter().number(from: DISTANCEValue) {
                let f = CGFloat(truncating: n)
                distance.text = "\(f)"
                if L102Language.currentAppleLanguage() == "ar" {
                    distance.textAlignment = .right
                }
            }
        }
        if let  imageIndex = (shopes.data?[indexPath.row].images?.first?.large) {
            let image = cell.viewWithTag(5) as! UIImageView
            image.sd_setImage(with: URL(string: imageIndex), completed: nil)
        }
        if let addressValue = shopes.data?[indexPath.row].address {
            let address = cell.viewWithTag(2) as! UILabel
            if L102Language.currentAppleLanguage() == "ar" {
                address.textAlignment = .right
            }
            address.text = "\(addressValue)"
        }
        if let reviewCountValue = shopes.data?[indexPath.row].review_count {
            let address = cell.viewWithTag(9) as! UILabel
            address.text = "\(reviewCountValue)"
            if L102Language.currentAppleLanguage() == "ar" {
                address.textAlignment = .right
            }
        }
        return cell
    }
}
//MARK:- Network
extension AllBeautyCenterViewController{
    func call_api(id : Int){
        HUD.show(.progress, onView: self.view)
        let request = APIs.Instance.showShop(id: id)
        callApi(vc:self, withURL: request, method: .get, headers: APIs.Instance.getHeader(), Parameter: [:]) { (result) in
            HUD.hide()
            print("----done ----")
            switch result {
            case .success(let data):
                do {
                    let shopData = try JSONDecoder().decode(Shop.self, from: data)
                    let navVC = self.storyboard?.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
                    if let nameValue = shopData.data?.name {
                        navVC.shopTitle = nameValue
                    }
                    navVC.shopData = shopData.data!
                    self.navigationController?.pushViewController(navVC, animated: true)
                }catch{
                    HUD.flash(.label("Error Try Again"), delay: 1.0)
                }
            default:
                print("-- error")
            }
        }
    }
}
