//
//  RattingViewViewController.swift
//  mashagel
//
//  Created by MACBOOK on 9/30/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//

import UIKit

class RattingViewViewController: UIViewController {

    @IBOutlet var RattingBackGround: UIView!
   override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.changeFont()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
       let tap = UITapGestureRecognizer(target: self, action: #selector(dismissView))
        tap.numberOfTapsRequired = 1
        self.RattingBackGround.addGestureRecognizer(tap)
        userData.Instance.fetchUser()
    }
    @objc func dismissView() {
       self.resignFirstResponder()
    }
    @IBAction func dismiss(_ sender: Any) {
        self.resignFirstResponder()
    }
}
