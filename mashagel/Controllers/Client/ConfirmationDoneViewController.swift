//
//  ConfirmationDoneViewController.swift
//  mashagel
//
//  Created by iMac on 10/15/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//
protocol refreshDelegate {
    func refresh()
}


import UIKit

class ConfirmationDoneViewController: UIViewController {
    var delegate:refreshDelegate? = nil

   override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.changeFont()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    @IBAction func dismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        delegate?.refresh()
    }
}
