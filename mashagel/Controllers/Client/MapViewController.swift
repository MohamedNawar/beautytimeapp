//
//  MapViewController.swift
//  mashagel
//
//  Created by MACBOOK on 10/1/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation
class MapViewController: UIViewController , CLLocationManagerDelegate {
    @IBOutlet weak var mapView: GMSMapView!
    var img = UIImage()
    var url = String()
    var shop = ShopData()
    let locationManager = CLLocationManager()
    
   override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.changeFont()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        mapView.mapType = .hybrid
        if shop != nil {
            let double_lat = Double(shop.latitude ?? "0") ?? 0.0
            let double_long = Double(shop.longitude ?? "0") ?? 0.0
            let mkr = GMSMarker()
            if (double_lat != 0.0 && double_long != 0.0)
            {
                let postion = CLLocationCoordinate2D(latitude: double_lat, longitude: double_long)
                mkr.position = postion
                mkr.title = shop.name
                mkr.map = mapView
                let img = UIImageView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
                img.layer.cornerRadius = 25
                img.clipsToBounds = true
                let url = URL(string: shop.images?.first?.large ?? "")
                img.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "img42"), options: .progressiveDownload, completed: nil)
                mkr.iconView = img
                mkr.iconView?.clipsToBounds = true
                let camera = GMSCameraPosition(target: postion, zoom: 12, bearing: 0.0, viewingAngle: 0.0)
                mkr.isTappable = true
                self.mapView.camera = camera;
                self.view.reloadInputViews()
            }
        }
        GMSServices.provideAPIKey("AIzaSyAcTqtg8D77LF88xx_JWm_AEsxTel3n94g")
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager.startUpdatingLocation()
        userData.Instance.fetchUser()
    }
    

}
extension GMSMarker {
    func setIconSize(scaledToSize newSize: CGSize) {
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        icon?.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        icon = newImage
    }
}
