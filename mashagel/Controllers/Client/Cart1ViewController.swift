//
//  Cart1ViewController.swift
//  mashagel
//
//  Created by MACBOOK on 9/30/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//'
import SideMenu
import UIKit
import Alamofire
import PKHUD
import FCAlertView
import Font_Awesome_Swift
import SideMenu

class Cart1ViewController: UIViewController{
    var currentService = ServicesData()
    var currentPackage = Package_item()
    var headerHeight = Int()
    var sectionsCount = Int()
    let datePicker = UIDatePicker()
    var reserVation1Id = Int()
    var reservation1 = Reservation(){
        didSet{
            if let dateValue = reservation1.data?.date?.date {
                var stringValue = dateValue
                stringValue = stringValue.components(separatedBy: " ")[0]
                dateTextField.text = stringValue
            }else{
                if let date = userData.Instance.selectedDate {
                    dateTextField.text = date
                }
            }
            if let priceValue = reservation1.data?.total_price{
                totalPriceLabl.text = "$\(priceValue)"
            }
            self.tableView.reloadData()
        }
    }
    var reservationForTime = ReservationForTime()
    var reservation = Reservation(){
        didSet{
            if let dateValue = reservation.data?.date?.date {
                var stringValue = dateValue
                stringValue = stringValue.components(separatedBy: " ")[0]
                print(stringValue)
                dateTextField.text = stringValue
            }else{
                let date = Date()
                dateTextField.text = date.asString()
            }
            if let priceValue = reservation.data?.total_price{
                totalPriceLabl.text = "$\(priceValue)"
            }
            self.words2 = self.reservation1.data?.times?.ArString  as? [String] ?? [String]()
            if self.words2.count <= 0 {
                self.dateTextField1.isEnabled = false
                self.dateTextField1.isUserInteractionEnabled = false
            }else{
                self.dateTextField1.isEnabled = true
                self.dateTextField1.isUserInteractionEnabled = true
            }
            self.tableView.reloadData()
        }
    }
    let timePicker = UIPickerView()
    var words2 = [String](){
        didSet{
            if words2.count > 0 {
                dateTextField1.isEnabled = true
            }else{
                dateTextField1.isEnabled = false
            }
        }
    }
    var selectedItem2 = ""
    var flag = ""
    var reserVationId = Int()
    
    @IBOutlet weak var topDistanceToview: NSLayoutConstraint!
    @IBOutlet weak var heightForDateTextfield: NSLayoutConstraint!
    @IBOutlet weak var headerHeightForView: NSLayoutConstraint!
    @IBOutlet weak var hiddentextField: UITextField!
    @IBOutlet weak var hiddentextField1: UITextField!
    @IBOutlet weak var dateTextField: DesignableTextField!
    @IBOutlet weak var dateTextField1: DesignableTextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var totalPriceLabl: UILabel!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var buttonDistance: NSLayoutConstraint!
    @IBOutlet weak var buttonDistance2: NSLayoutConstraint!
    
   override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.changeFont()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        setupController()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        print(reserVation1Id)
        self.navigationItem.backBarButtonItem?.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationController?.navigationBar.backItem?.backBarButtonItem?.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        dateTextField1.text = ""
        let date = Date().dayAfter
        dateTextField.text = date.asString()
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.8558626771, green: 0.2503757179, blue: 0.4763913751, alpha: 1)
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0.8558626771, green: 0.2503757179, blue: 0.4763913751, alpha: 1)
        userData.Instance.fetchUser()
        headerHeightForView.constant = CGFloat(40)
        heightForDateTextfield.constant = 40
        buttonDistance.constant = 0
        buttonDistance2.constant = 0
        topDistanceToview.constant = 0
    }
    override func viewDidAppear(_ animated: Bool) {
        print(reserVation1Id)
        if userData.Instance.serviceType ?? "" == "inside"{
            print(flag)
            if flag == "" {
                reserVationId = userData.Instance.identifierInside ?? -1
            }
            if dateTextField.text != "" {
                if userData.Instance.serviceType ?? "" == "inside"{
                    if flag == "" {
                        self.reserVation1Id = userData.Instance.identifierInside ?? -1
                    }
                    PreparingSendDateForCurrentReservation(id:reserVationId)
                }else{
                    if flag == "" {
                        self.reserVation1Id = userData.Instance.identifierOutside ?? -1
                    }
                    PreparingSendDateForCurrentReservation1(id:reserVationId)
                }
            }
            if let dateValue = reservation.data?.date?.date {
                var stringValue = dateValue
                stringValue = stringValue.components(separatedBy: " ")[0]
                dateTextField.text = stringValue
            }else{
                let date = Date().dayAfter
                dateTextField.text = date.asString()
                if userData.Instance.serviceType ?? "" == "inside"{
                    userData.Instance.fetchUser()
                    if flag == "" {
                        self.reserVation1Id = userData.Instance.identifierInside ?? -1
                    }
                    PreparingSendDateForCurrentReservation(id:reserVationId)
                }else{
                    userData.Instance.fetchUser()
                    if flag == "" {
                        self.reserVation1Id = userData.Instance.identifierOutside ?? -1
                    }
                    PreparingSendDateForCurrentReservation1(id:reserVationId)
                }
            }
        }else{
            if flag == "" {
                reserVationId = userData.Instance.identifierOutside ?? -1
            }
            if dateTextField.text != "" {
                if userData.Instance.serviceType ?? "" == "inside"{
                    userData.Instance.fetchUser()
                    if flag == "" {
                        self.reserVation1Id = userData.Instance.identifierInside ?? -1
                    }
                    PreparingSendDateForCurrentReservation(id:reserVation1Id)
                }else{
                    if flag == "" {
                        self.reserVation1Id = userData.Instance.identifierOutside ?? -1
                    }
                    PreparingSendDateForCurrentReservation1(id:reserVation1Id)
                }                }
            if let dateValue = reservation.data?.date?.date {
                var stringValue = dateValue
                stringValue = stringValue.components(separatedBy: " ")[0]
                dateTextField.text = stringValue
            }else{
                let date = Date()
                dateTextField.text = date.asString()
                if userData.Instance.serviceType  ?? "" == "inside" || userData.Instance.serviceType == nil{
                    userData.Instance.fetchUser()
                    if flag == "" {
                        self.reserVation1Id = userData.Instance.identifierInside ?? -1
                    }
                    PreparingSendDateForCurrentReservation(id:reserVation1Id)
                }else{
                    if flag == "" {
                        self.reserVation1Id = userData.Instance.identifierOutside ?? -1
                    }
                    print(userData.Instance.serviceType ?? "")
                    PreparingSendDateForCurrentReservation1(id:reserVation1Id)
                }
            }
            if userData.Instance.serviceType ?? "" == "inside"{
                userData.Instance.fetchUser()
                if flag == "" {
                    self.reserVation1Id = userData.Instance.identifierInside ?? -1
                }
                PreparingReservationShopData1(id: self.reserVation1Id)
            }else{
                if flag == "" {
                    self.reserVation1Id = userData.Instance.identifierOutside ?? -1
                }
                PreparingReservationShopData1(id: self.reserVation1Id)
            }
        }
        
    }
    
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        alert.colorScheme = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle:NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
    @IBAction func confirmReservation(_ sender: Any) {
        if reservation.data == nil && reservation1.data == nil  {return}
        if dateTextField1.text == "" {
            HUD.flash(.label(NSLocalizedString("Enter your Time", comment: "حدث خطأ برجاء اعادة المحاولة")), delay: 1.0)
            return
        }
        if userData.Instance.token == "" || userData.Instance.token == nil {
            self.performSegue(withIdentifier: "popOver1111", sender: self)
        }else{
            if flag == "" {
                let navVC = storyboard?.instantiateViewController(withIdentifier: "summeryRquestVC") as! summeryRquestVC
                if reservation.data != nil {
                    navVC.reservationData = reservation.data ?? ReservationData()
                }else{
                    navVC.reservationData = reservation1.data ?? ReservationData()
                }
                navVC.delegate = self
                self.navigationController?.pushViewController(navVC, animated: true)
            }else{
                confirmReservationShopData(userId: reserVation1Id)
            }
        }
    }
}

//MARK:- Confirm Service Protocol
extension Cart1ViewController:ConfirmService {
    func ConfirmServiceDelegate() {
        if userData.Instance.serviceType ?? "" == "inside" {
            let userIdentifier = userData.Instance.identifierInside
            confirmReservationShopData(userId: userIdentifier ?? -1)
        }else{
            let userIdentifier = userData.Instance.identifierOutside
            confirmReservationShopData(userId: userIdentifier ?? -1)
        }
    }
}

//MARK:- Table View Delegate and Data Source
extension Cart1ViewController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if userData.Instance.serviceType ?? "" == "inside" {
            let service = reservation1.data?.service_items?.items?.count ?? 0
            if service > 0 {
                if section == reservation1.data?.service_items?.package_items?.count {
                    return service
                }
                return reservation1.data?.service_items?.package_items?[section].items?.count ?? 0
            }else{
                return reservation1.data?.service_items?.package_items?[section].items?.count ?? 0
            }
        }else {             print(reservation.data?.service_items?.items?.count ?? 0)
            return reservation.data?.service_items?.items?.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if userData.Instance.serviceType ?? "" == "inside" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cart2Cell1", for: indexPath) as!  Cart2ContentCell
            let package = reservation1.data?.service_items?.package_items?.count ?? 0
            if  package > 0 {
                let service = reservation1.data?.service_items?.items?.count ?? 0
                if service > 0 {
                    if indexPath.section == (reservation1.data?.service_items?.package_items?.count ?? 0) {
                        cell.deleteBtn.isEnabled = true
                        cell.deleteBtn.isHidden = false
                        cell.updateViews(service:reservation1.data?.service_items?.items?[indexPath.row] ?? ServicesData())
                        currentService = reservation1.data?.service_items?.items?[indexPath.row] ?? ServicesData()
                        cell.DeleteServiceCallBack = {
                            () -> () in
                            self.deleteServiceInReservationShopData(id:self.reservation1.data?.service_items?.items?[indexPath.row].service?.id ?? -1 , userId : self.reserVation1Id)
                            self.reservation1.data?.service_items?.items?.remove(at: indexPath.row)
                            self.tableView.reloadData()
                        }
                    }else{
                        cell.deleteBtn.isEnabled = false
                        cell.deleteBtn.isHidden = true
                        cell.updateViews(service:reservation1.data?.service_items?.package_items?[indexPath.section].items?[indexPath.row] ?? ServicesData())
                        currentService = reservation1.data?.service_items?.package_items?[indexPath.section].items?[indexPath.row] ?? ServicesData()
                        cell.DeleteServiceCallBack = {
                            () -> () in
                            self.deleteServiceInReservationShopData(id:self.reservation1.data?.service_items?.items?[indexPath.row].service?.id ?? -1 , userId : self.reserVation1Id)
                            self.reservation1.data?.service_items?.package_items?[indexPath.section].items?.remove(at: indexPath.row)
                            self.tableView.reloadData()
                        }
                    }
                }else{
                    cell.deleteBtn.isEnabled = false
                    cell.deleteBtn.isHidden = true; cell.updateViews(service:reservation1.data?.service_items?.package_items?[indexPath.section].items?[indexPath.row] ?? ServicesData())
                    currentService = reservation1.data?.service_items?.package_items?[indexPath.section].items?[indexPath.row] ?? ServicesData()
                    cell.DeleteServiceCallBack = {
                        () -> () in
                        self.deleteServiceInReservationShopData(id:self.reservation1.data?.service_items?.items?[indexPath.row].service?.id ?? -1 , userId : self.reserVation1Id)
                        self.reservation1.data?.service_items?.package_items?[indexPath.section].items?.remove(at: indexPath.row)
                        self.tableView.reloadData()
                    }
                }
            }else{
                cell.deleteBtn.isEnabled = true
                cell.deleteBtn.isHidden = false;
                let service = reservation1.data?.service_items?.items?.count ?? 0
                if service > 0 {
                    if indexPath.section == (reservation1.data?.service_items?.package_items?.count ?? 0) {
                        cell.updateViews(service:reservation1.data?.service_items?.items?[indexPath.row] ?? ServicesData())
                        currentService = reservation1.data?.service_items?.items?[indexPath.row] ?? ServicesData()
                        cell.DeleteServiceCallBack = {
                            () -> () in
                            self.deleteServiceInReservationShopData(id:self.reservation1.data?.service_items?.items?[indexPath.row].service?.id ?? -1 , userId : self.reserVation1Id)
                            self.reservation1.data?.service_items?.items?.remove(at: indexPath.row)
                            self.tableView.reloadData()
                        }
                    }
                }
            }
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cart1Cell1", for: indexPath) as!  Cart1ContentCell
            cell.updateViews(service:reservation.data?.service_items?.items?[indexPath.row] ?? ServicesData())
            currentService = reservation.data?.service_items?.items?[indexPath.row] ?? ServicesData()
            
            cell.DeleteServiceCallBack = {
                () -> () in
                if userData.Instance.serviceType ?? "" == "inside"{
                    if self.flag == "" {
                        self.reserVation1Id = userData.Instance.identifierInside ?? -1
                    }
                    self.deleteServiceInReservationShopData1(id:self.reservation.data?.service_items?.items?[indexPath.row].service?.id ?? -1 , userId : self.reserVationId)              }else{
                    if self.flag == "" {
                        self.reserVation1Id = userData.Instance.identifierOutside ?? -1
                    }; self.deleteServiceInReservationShopData1(id:self.reservation.data?.service_items?.items?[indexPath.row].service?.id ?? -1 , userId : self.reserVationId)              }
                self.reservation.data?.service_items?.items?.remove(at: indexPath.row)
                self.tableView.reloadData()
            }
            return cell
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerCell = tableView.dequeueReusableCell(withIdentifier: "Cart2Cell11") as! Cart2HeaderCell
        headerCell.updateViews(Package: reservation1.data?.service_items?.package_items?[section] ?? Package_item())
        headerCell.DeleteServiceCallBack = {
            () -> () in
            self.deletePackageInReservationShopData(id:self.reservation1.data?.service_items?.package_items?[section].id ?? -1 , userId : self.reserVation1Id)
            self.reservation1.data?.service_items?.package_items?.remove(at: section)
            self.tableView.reloadData()
        }
        return headerCell
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if userData.Instance.serviceType ?? "" == "inside" {
            let package = reservation1.data?.service_items?.package_items?.count ?? 0
            if  package > 0 {
                let service = reservation1.data?.service_items?.items?.count ?? 0
                if service > 0 {
                    if section == (reservation1.data?.service_items?.package_items?.count ?? 0) {
                        return CGFloat(0)
                    }
                    return CGFloat(50)
                }
            }else{
                return CGFloat(0)
            }
            return CGFloat(50)
        }else{
            return CGFloat(0)
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if userData.Instance.serviceType ?? "" == "inside" {
            let service = reservation1.data?.service_items?.items?.count ?? 0
            if service > 0 {
                let packageCount = reservation1.data?.service_items?.package_items?.count
                return (packageCount ?? 0)+1
            }else{
                return reservation1.data?.service_items?.package_items?.count ?? 0
            }
        }else{
            return 1
        }
    }
    
}

//MARK:- Refresh Delegate
extension Cart1ViewController: refreshDelegate{
    func refresh() {
        NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: nil)
        self.tableView.reloadData()
    }
}

//MARK:- register Protocol
extension Cart1ViewController: registerOrLogin{
    func registerOrLoginToReserve(flag: Int) {
        if flag == 0 {
            let navVC = storyboard?.instantiateViewController(withIdentifier: "RegisterationViewController") as! RegisterationViewController
            self.navigationController?.pushViewController(navVC, animated: true)
        }else{
            let navVC = storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            self.navigationController?.pushViewController(navVC, animated: true)
        }
    }
}

//MARK:- Picker View Delegate and Data Source
extension Cart1ViewController:UIPickerViewDataSource , UIPickerViewDelegate{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == timePicker {
            return "\(words2[row])"
        }
        return ""
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return words2.count
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == timePicker {
            selectedItem2 = words2[row]
            dateTextField1.text = selectedItem2
        }
    }
}

//MARK:- Picker Methods
extension Cart1ViewController{
    func showDatePicker11(){
        //Formate Date
        datePicker.datePickerMode = .date
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        let doneButton = UIBarButtonItem(title:NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donedatePicker11));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Dismiss", comment: "إلغاء"), style: .plain, target: self, action: #selector(cancelDatePicker11));
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        dateTextField.inputAccessoryView = toolbar
        dateTextField.inputView = datePicker
    }
    @objc func donedatePicker11(){
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        dateTextField.text = formatter.string(from: datePicker.date)
        if userData.Instance.serviceType ?? "" == "inside"{
            if flag == "" {
                reserVation1Id = userData.Instance.identifierInside ?? -1
            }
            PreparingSendDateForCurrentReservation(id:reserVation1Id)
        }else{
            if flag == "" {
                reserVation1Id = userData.Instance.identifierOutside ?? -1
            }
            PreparingSendDateForCurrentReservation1(id:reserVation1Id)
        }
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker11(){
        self.view.endEditing(true)
    }
    func showDatePicker(){
        //Formate Date
        datePicker.datePickerMode = .date
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Dismiss", comment: "إلغاء"), style: .plain, target: self, action: #selector(cancelDatePicker));
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        dateTextField.inputAccessoryView = toolbar
        dateTextField.inputView = datePicker
    }
    @objc func donedatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        dateTextField.text = formatter.string(from: datePicker.date)
        if userData.Instance.serviceType ?? "" == "inside"{
            if flag == "" {
                reserVation1Id = userData.Instance.identifierInside ?? -1
            }
            PreparingSendDateForCurrentReservation(id:reserVation1Id)
        }else{
            if flag == "" {
                reserVation1Id = userData.Instance.identifierOutside ?? -1
            }
            PreparingSendDateForCurrentReservation1(id:reserVation1Id)
        }
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    func showTimePicker(){
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        //let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donedatePicker1));
        let spaceButton1 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Dismiss", comment: "إلغاء"), style: .plain , target: self, action: #selector(cancelDatePicker1))
        toolbar.setItems([doneButton , spaceButton1 ,cancelButton], animated: false)
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        dateTextField1.inputView = timePicker
        dateTextField1.inputAccessoryView = toolbar
    }
    @objc func donedatePicker1(){
        if dateTextField1.text == "" {
            if words2.count > 0 {
                dateTextField1.text = words2[0]
                selectedItem2 = words2[0]
                self.view.endEditing(true)
            }
        }
        self.view.endEditing(true)
    }
    @objc func cancelDatePicker1(){
        dateTextField1.text = ""
        self.view.endEditing(true)
    }
}

//MARK:- TextField Delegate
extension Cart1ViewController:UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.borderColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        textField.borderWidth = 1
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text == "" {
            textField.borderColor = #colorLiteral(red: 0.6642242074, green: 0.6642400622, blue: 0.6642315388, alpha: 1)
        }
    }
}

//MARK:- prepare for segue
extension Cart1ViewController{
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "popOver1111" {
            let destination = segue.destination as! AlartForLoginViewController
            destination.delegate = self
        }
        if segue.identifier == "popOver" {
            let destination = segue.destination as! ConfirmationDoneViewController
            destination.delegate = self
        }
    }
}

//MARK:- Networking
extension Cart1ViewController{
    func deletePackageInReservationShopData(id:Int , userId : Int){
        var header = APIs.Instance.getHeader()
        header.updateValue(setidentifier(), forKey: "reservation_id")
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.RemovePackage(id: id, identifier: userId) , method: .delete, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "img34"))
                        print(err.errors ?? "")
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.reservation1 = try JSONDecoder().decode(Reservation.self, from: response.data!)
                    }catch{
                        print(error)
                      //  HUD.flash(.label("Error Try Again"), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    func deleteServiceInReservationShopData(id:Int , userId : Int){
        var header = APIs.Instance.getHeader()
        header.updateValue(setidentifier(), forKey: "reservation_id")
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.RemoveService(id: id, identifier: userId) , method: .delete, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.reservation = Reservation()
                        self.tableView.reloadData()
                        print(err.message ?? "")
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.reservation1 = try JSONDecoder().decode(Reservation.self, from: response.data!)
                    }catch{
                        print(error)
                      //  HUD.flash(.label("Error Try Again"), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    func PreparingReservationShopData(id:Int){
        var header = APIs.Instance.getHeader()
        header.updateValue(setidentifier(), forKey: "reservation_id")
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.CurrentReservation(id: id) , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        print(err.errors ?? "")
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.reservation1 = try JSONDecoder().decode(Reservation.self, from: response.data!)
                    }catch{
                        print(error)
                      //  HUD.flash(.label("Error Try Again"), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    func PreparingSendDateForCurrentReservation(id:Int){
        HUD.show(.progress)
        var header = APIs.Instance.getHeader()
        header.updateValue("application/x-www-form-urlencoded", forKey: "Content-Type")
        let identifier = setidentifier()
        header.updateValue(identifier, forKey: "reservation_id")
        let par = ["date": dateTextField.text!] as [String : Any];
        print(par)
        print(APIs.Instance.SendDateForCurrentReservation(id:identifier));
        userData.Instance.fetchUser()
        if flag == "" {
            reserVation1Id = userData.Instance.identifierInside ?? -1;
        }
        Alamofire.request(APIs.Instance.SendDateForCurrentReservation(id:"\(reserVation1Id)"), method: .put , parameters: par, encoding: URLEncoding(), headers: header)
            .responseJSON { (response:DataResponse<Any>) in
                HUD.hide()
                switch(response.result) {
                case .success(let value):
                    print(value)
                    HUD.hide()
                    let temp = response.response?.statusCode ?? 400
                    if temp >= 300 {
                        do {
                            let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                            print(err.errors ?? "")
                            self.tableView.reloadData()
                        }catch{
                            print("errorrrrelse")
                        }
                    }else{
                        do {
                            self.reservation1 = try JSONDecoder().decode(Reservation.self, from: response.data!)
                            self.words2 = self.reservation1.data?.times?.ArString  as? [String] ?? [String]()
                        }catch{
                            print(error)
                          //  HUD.flash(.label("Error Try Again"), delay: 1.0)
                        }
                    }
                case .failure(_):
                    let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                    HUD.flash(.label(lockString), delay: 1.0)
                    break
                }
        }
    }
    func confirmReservationShopData(userId : Int){
        if dateTextField1.text == "" {
            HUD.flash(.label(NSLocalizedString("Enter your Time", comment: "حدث خطأ برجاء اعادة المحاولة")), delay: 1.0)
            return
        }
        var header = APIs.Instance.getHeader()
        header.updateValue(setidentifier(), forKey: "reservation_id")
        HUD.show(.progress, onView: self.view)
        print(APIs.Instance.ConfirmReservation(identifier: userId))
        Alamofire.request(APIs.Instance.ConfirmReservation(identifier: userId) , method: .post, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.reservation = Reservation()
                        self.tableView.reloadData()
                        print(err.message ?? "")
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.reservation1 = try JSONDecoder().decode(Reservation.self, from: response.data!)
                        self.performSegue(withIdentifier: "popOver", sender: self)
                        if self.flag == "" {
                            if userData.Instance.serviceType ?? "" == "inside" {
                                userData.Instance.removeIdentifierInside()
                                userData.Instance.userSelectedShopId = nil
                                userData.Instance.removeidentifierOutside()
                                userData.Instance.removecurrentReserveShopId()
                                self.reservation.data?.service_items?.items?.removeAll()
                                self.reservation.data?.packages?.removeAll()
                                self.tableView.reloadData()
                            }else{
                                userData.Instance.removeIdentifierInside()
                                userData.Instance.removeidentifierOutside()
                                userData.Instance.removecurrentReserveShopId()
                                self.reservation1.data?.service_items?.items?.removeAll()
                                self.reservation1.data?.packages?.removeAll()
                                self.tableView.reloadData()
                            }
                        }else{
                            userData.Instance.removeeEditFlag()
                            userData.Instance.removeEditReservationID()
                        }
                    }catch{
                        print(error)
                      //  HUD.flash(.label("Error Try Again"), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    func deleteServiceInReservationShopData1(id:Int , userId : Int){
        var header = APIs.Instance.getHeader()
        header.updateValue(setidentifier(), forKey: "reservation_id")
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.RemoveService(id: id, identifier: userId) , method: .delete, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.reservation = Reservation()
                        self.tableView.reloadData()
                        print(err.message ?? "")
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.reservation = try JSONDecoder().decode(Reservation.self, from: response.data!)
                    }catch{
                        print(error)
                      //  HUD.flash(.label("Error Try Again"), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    func PreparingReservationShopData1(id:Int){
        var header = APIs.Instance.getHeader()
        header.updateValue(setidentifier(), forKey: "reservation_id")
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.CurrentReservation(id: id) , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        print(err.message ?? "")
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.reservation = try JSONDecoder().decode(Reservation.self, from: response.data!)
                    }catch{
                        print(error)
                      //  HUD.flash(.label("Error Try Again"), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    func PreparingSendDateForCurrentReservation1(id:Int){
        var header = APIs.Instance.getHeader()
        let identifier = setidentifier()
        userData.Instance.fetchUser()
        header.updateValue(identifier, forKey: "reservation_id")
        print(identifier)
        header.updateValue("application/x-www-form-urlencoded", forKey: "Content-Type")
        print(header)
        HUD.show(.progress, onView: self.view)
        let par = ["date": dateTextField.text!] as [String : Any];
        Alamofire.request(APIs.Instance.SendDateForCurrentReservation(id:identifier), method: .put , parameters: par, encoding: URLEncoding(), headers: header)
            .responseJSON { (response:DataResponse<Any>) in
                switch(response.result) {
                case .success(let value):
                    print(value)
                    HUD.hide()
                    let temp = response.response?.statusCode ?? 400
                    if temp >= 300 {
                        do {
                            let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                            print(err.errors ?? "")
                        }catch{
                            print("errorrrrelse")
                        }
                    }else{
                        do {
                            self.reservation = try JSONDecoder().decode(Reservation.self, from: response.data!)
                            self.reservationForTime = try JSONDecoder().decode(ReservationForTime.self, from: response.data!)
                        }catch{
                            print(error)
                          //  HUD.flash(.label("Error Try Again"), delay: 1.0)
                        }
                    }
                case .failure(_):
                    HUD.hide()
                    let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                    HUD.flash(.label(lockString), delay: 1.0)
                    break
                }
        }
    }
    
    func setidentifier() -> String{
        var identifier = String()
        userData.Instance.fetchUser()
        if flag == "" {
            if userData.Instance.serviceType ?? "" == "inside" {
                identifier = String(describing: userData.Instance.identifierInside)
                return identifier
            }else{
                identifier = String(describing: userData.Instance.identifierOutside)
                return identifier
            }
        }else{
            identifier = String(describing: reserVation1Id)
            return identifier
        }
    }
}

//MARK:- Setup Controller
extension Cart1ViewController{
    func setupController(){
        userData.Instance.fetchUser()
        let flagEdit = userData.Instance.editFlag
        if flagEdit != "" && flagEdit != nil {
            flag = flagEdit ?? ""
        }
        let idEdit = userData.Instance.editReservationID ?? -1
        if idEdit != -1  {
            reserVation1Id = idEdit
            reserVationId = idEdit
        }
        setupSideMenu()
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.8558626771, green: 0.2503757179, blue: 0.4763913751, alpha: 1)
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0.8558626771, green: 0.2503757179, blue: 0.4763913751, alpha: 1)
        timePicker.backgroundColor = .white
        timePicker.delegate = self
        timePicker.dataSource = self
        timePicker.showsSelectionIndicator = true
        self.title = NSLocalizedString("Cart", comment: "الحجوزات")
        navigationItem.title = NSLocalizedString("Cart", comment: "الحجوزات")
        userData.Instance.fetchUser()
        tableView.allowsSelection = false
        datePicker.backgroundColor = .white
        tableView.separatorStyle = .none
        tableView.allowsSelection = false
        hiddentextField.delegate = self
        hiddentextField1.delegate = self
        userData.Instance.fetchUser()
        hiddentextField.addPadding(UITextField.PaddingSide.left(20))
        hiddentextField1.addPadding(UITextField.PaddingSide.left(20))
        hiddentextField.setRightViewFAIcon(icon: .FAAngleDown, rightViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
        hiddentextField1.setRightViewFAIcon(icon: .FAAngleDown, rightViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
        showDatePicker()
        showTimePicker()
        tabBarItem.imageInsets.top = -6
    }
}
