//
//  ClientRankingForCentersViewController.swift
//  mashagel
//
//  Created by MACBOOK on 9/29/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD
import FCAlertView
import HCSStarRatingView
class ClientRankingForCentersViewController: UIViewController{
    var myReviews = Reviews(){
        didSet{
            tableView.reloadData()
        }
    }
    
    @IBOutlet weak var tableView: UITableView!
    
   override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.changeFont()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        tableView.separatorStyle = .none
        tableView.delegate = self
        tableView.dataSource = self
        userData.Instance.fetchUser()
        loadMyReviews()
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        alert.colorScheme = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
}

//MARK:- TableView Delegate and DataSource
extension ClientRankingForCentersViewController: UITableViewDelegate , UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myReviews.data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ClientRankingForCentersCell", for: indexPath)
        if let nameValue = myReviews.data?[indexPath.row].name{
            let name = cell.viewWithTag(1) as! UILabel
            name.text = "\(nameValue)"
        }
        if let messageValue = myReviews.data?[indexPath.row].message {
            let message = cell.viewWithTag(2) as! UILabel
            if L102Language.currentAppleLanguage() == "ar" {
                message.textAlignment = .right
            }
            message.text = "\(messageValue)"
        }
        if let messageValue = myReviews.data?[indexPath.row].review {
            let message = cell.viewWithTag(120) as! UILabel
            message.text = messageValue
        }
        if let rateValue = myReviews.data?[indexPath.row].review {
            let ratting = cell.viewWithTag(10) as! HCSStarRatingView
            ratting.value = rateValue.CGFloatValue() ?? 0.0
        }
        if let dateValue = myReviews.data?[indexPath.row].created_at {
            let date = cell.viewWithTag(8) as! UILabel
            var stringValue = dateValue.date
            stringValue = stringValue?.components(separatedBy: ".")[0]
            print(stringValue ?? "")
            date.text = "\(stringValue ?? "")"
        }
        if let imageValue = myReviews.data?[indexPath.row].image?.large{
            let image = cell.viewWithTag(111) as! UIImageView
            image.sd_setImage(with: URL(string: imageValue), completed: nil)
        }
        return cell
    }
}

//MARK:- NetWork
extension ClientRankingForCentersViewController{
    func loadMyReviews(){
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.ListReviews() , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "img34"))
                        print(err.message!)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.myReviews = try JSONDecoder().decode(Reviews.self, from: response.data!)
                    }catch{
                        print(error)
                      //  HUD.flash(.label("Error Try Again"), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                HUD.flash(.label("Error Try Again Later"), delay: 1.0)
                break
            }
        }
    }
}
