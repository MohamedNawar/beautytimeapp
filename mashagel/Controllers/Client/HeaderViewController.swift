//
//  HeaderViewController.swift
//  mashagel
//
//  Created by iMac on 10/31/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//

import UIKit
import Auk
import moa
import PKHUD
import Alamofire
import FCAlertView
class HeaderViewController: UIViewController {
    var shopData = ShopData()
//    var id = Int(){
//        didSet{
//
//            print(id)
//
//        }
//    }
    var images = [String](){
        didSet{
            
        }
    }
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
      self.navigationItem.backBarButtonItem?.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
             self.navigationController?.navigationBar.backItem?.backBarButtonItem?.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
             self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
             self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
    }
   override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.changeFont()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)

      scrollView.auk.settings.contentMode = .scaleToFill
        setScrollView()
    }
    private func setScrollView(){
        
        if let  imageIndex = self.shopData.images   {
            for img in imageIndex{
                self.images.append(img.large ?? "")
            }
        }
            for image in images {
                scrollView.auk.show(url:image )
            }
    }

}
