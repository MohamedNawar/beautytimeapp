//
//  UserSendReviewToApplication.swift
//  mashagel
//
//  Created by iMac on 10/14/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//

import UIKit
import HCSStarRatingView
import Alamofire
import PKHUD
import FCAlertView
import Font_Awesome_Swift
class UserSendReviewToApplication: UIViewController{
    @IBOutlet weak var msgLbl: UILabel!
    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var textViewView: UIView!
    @IBOutlet weak var rattingView: HCSStarRatingView!
    
   override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.changeFont()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        userData.Instance.fetchUser()
        rattingView.value = CGFloat(0)
        messageTextView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.backBarButtonItem?.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationController?.navigationBar.backItem?.backBarButtonItem?.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        if L102Language.currentAppleLanguage() == "ar" {
            msgLbl.textAlignment = .right
            messageTextView.textAlignment = .right
        }
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        _ = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
    @IBAction func sendMessage(_ sender: Any) {
        let  id = userData.Instance.userSelectedShopId!
        PostMessage(id:id)
    }
    @IBAction func dismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
//MARK:- TextView Delegate
extension UserSendReviewToApplication: UITextViewDelegate{
    func textViewDidBeginEditing(_ textView: UITextView) {
        textViewView.borderColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        textViewView.borderWidth = 1
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        textViewView.borderColor = #colorLiteral(red: 0.6642242074, green: 0.6642400622, blue: 0.6642315388, alpha: 1)
        textViewView.borderWidth = 1
    }
}
//MARK:- Network
extension UserSendReviewToApplication{
    func PostMessage(id:Int){
        if    self.messageTextView.text == ""  {
            HUD.flash(.label("Enter your Message"), delay: 1.0)
            return
        }
        let header = APIs.Instance.getHeader()
        let par = ["review":"\(rattingView.value)", "message": messageTextView.text] as [String : Any]
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.sendShopReview(id: id), method: .post, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            print(response)
            switch(response.result) {
            case .success( _):
                HUD.hide()
                let temp = response.response?.statusCode ?? 400
                print(temp)
                if temp >= 300 {
                    print("errorrrr")
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "img34"))
                        print(err.message!)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.dismiss(animated: true, completion: nil)
                        print("successsss")
                    }catch{
                      //  HUD.flash(.label("Error Try Again"), delay: 1.0)
                    }
                    HUD.flash(.success, delay: 1.0)
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
}
