//
//  filterViewController.swift
//  mashagel
//
//  Created by iMac on 10/21/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD
import CoreLocation
import FCAlertView
import DLRadioButton


class filterViewController: UIViewController , CLLocationManagerDelegate{
    @IBOutlet weak var departmentTextField: UITextField!
    @IBOutlet weak var serviceTextField: UITextField!
    @IBOutlet weak var serviceLocationLabel: UILabel!
    @IBOutlet weak var districtionLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var districtTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    var request = "http://service.beautytimes.org/api/home"
    var shopes = Shops()
    var services = Services(){
        didSet{
            servicePicker.reloadAllComponents()
        }
    }
    var selectedDepartment = Int()
    var departments = Department()
    let locationManager = CLLocationManager()
    var coordinateValue = CLLocationCoordinate2D(latitude: 0, longitude: 0)
    let cityPicker = UIPickerView()
    let areaPicker = UIPickerView()
    let servicePicker = UIPickerView()
    let departmentPicker = UIPickerView()
    let datePicker = UIDatePicker()
    var selectedCity = CitiesData()
    var selectedArea = Areas()
    var words = ["Cat", "Chicken", "fish", "Dog", "Mouse", "Guinea Pig", "monkey"]
    var selected = ""
    var words1 = ["Ccat", "Cchicken", "fcish", "Dcog", "Mcouse", "Gucinea Pcig", "cmonkey"]
    var selected1 = ""
    var cityId :Int = 0
    var areaId :Int = 0
    var serviceId :Int = 0
    var selectedCityIndex = 0
    var serType = "inside"
    
    
   override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.changeFont()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        setupView()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
        let statusBarColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
            statusBarView.backgroundColor = statusBarColor
            view.addSubview(statusBarView)
        self.navigationItem.backBarButtonItem?.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationController?.navigationBar.backItem?.backBarButtonItem?.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        
        if L102Language.currentAppleLanguage() == "ar" {
            cityLabel.textAlignment = .right
            districtionLabel.textAlignment = .right
            serviceLocationLabel.textAlignment = .right
            //                serviceType.setTitle("في ضيافتنا", forSegmentAt: 0)
            //                serviceType.setTitle("بالمنزل", forSegmentAt: 1)
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
    }
    
    //radio btn ==> service location
    @IBAction private func logSelectedButton(radioButton : DLRadioButton) {
        let o = radioButton.selected()!.titleLabel!.text!
        print(String(format: "%@ is selected.\n", o));
        if o == "Outside"{
            serType = "outside"
        }else{
            serType = "inside"
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if  let coordinate = locationManager.location?.coordinate {
            coordinateValue = coordinate
            self.locationManager.stopUpdatingLocation()
        }else {
        }
    }
    func loadCityyData() {
        HUD.show(.progress, onView: self.view)
        CityMain.Instance.getCitiesServer(enterDoStuff: { () in
            self.words = CityMain.Instance.getCitiesNameArr()
            self.cityPicker.reloadAllComponents()
            HUD.hide(animated: true)
        })
    }
    
    @IBAction func search(_ sender: Any) {
        //                if self.serviceType.selectedSegmentIndex == 1 {
        //                    self.serType = "outside"
        //                }else {
        //                    self.serType = "inside"
        //                }
        if self.cityTextField.text != "" {
            self.request =  "http://service.beautytimes.org/api/home?area=\(self.areaId)?city=\(self.cityId)?categories=\(self.serviceId)?latitude=\(self.coordinateValue.latitude)?longitude=\(self.coordinateValue.longitude)?type=\(self.serType)?departments=\(selectedDepartment)"
            print(self.request)
        }
        let navVC = self.storyboard?.instantiateViewController(withIdentifier: "AllBeautyCenterViewController") as! AllBeautyCenterViewController
        navVC.url = self.request
        self.navigationController?.pushViewController(navVC, animated: true)
    }
}

//MARK:- TextField Delegate
extension filterViewController: UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.borderColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        textField.borderWidth = 1
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text == "" {
            textField.borderColor = #colorLiteral(red: 0.6642242074, green: 0.6642400622, blue: 0.6642315388, alpha: 1)
        }
    }
}
//MARK:- PickerView Delegate and DataSource
extension filterViewController: UIPickerViewDelegate , UIPickerViewDataSource{
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == cityPicker {
            return words.count
        }
        if pickerView == areaPicker {
            return words1.count
        }
        if pickerView == servicePicker {
            return services.data?.count ?? 0
        }
        if pickerView == departmentPicker {
            return departments.data?.count ?? 0
        }
        return 0
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == cityPicker {
            return "\(words[row])"
        }
        if pickerView == areaPicker {
            return "\(words1[row])"
        }
        if pickerView == servicePicker {
            return "\(services.data?[row].name ?? "")"
        }
        if pickerView == departmentPicker {
            return "\(departments.data?[row].name ?? "")"
        }
        return ""
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == cityPicker {
            selected = words[row]
            selectedCity = CityMain.Instance.cityData.data?[row] ?? CitiesData()
            selectedCityIndex = row
            words1 = selectedCity.getAreasName()
            cityId = selectedCity.id ?? 0
            cityTextField.text = selected
            areaPicker.reloadAllComponents()
        }
        if pickerView == areaPicker {
            selected1 = words1[row]
            selectedArea = selectedCity.areas?[row] ?? Areas()
            areaId = selectedArea.id ?? 0
            districtTextField.text = selected1
        }
        if pickerView == areaPicker {
            serviceId = services.data?[row].id ?? 0
            serviceTextField.text = services.data?[row].name ?? ""
        }
        if pickerView == departmentPicker {
            departmentTextField.text = departments.data?[row].name ?? ""
            selectedDepartment = departments.data?[row].id ?? 0
        }
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
}
//MARK:- PickerView Methods
extension filterViewController{
    func showServicePicker(){
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(doneservicePicker));
        let spaceButton1 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Dismiss", comment: "إلغاء"), style: .plain , target: self, action: #selector(cancel1))
        toolbar.setItems([doneButton , spaceButton1 ,cancelButton], animated: false)
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        serviceTextField.inputView = servicePicker
        serviceTextField.inputAccessoryView = toolbar
    }
    func showDepartmentPicker(){
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(doneDepartmentPicker));
        let spaceButton1 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Dismiss", comment: "إلغاء"), style: .plain , target: self, action: #selector(cancel11))
        toolbar.setItems([doneButton , spaceButton1 ,cancelButton], animated: false)
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        departmentTextField.inputView = departmentPicker
        departmentTextField.inputAccessoryView = toolbar
    }
    func showAreaPicker(){
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donePicker1));
        let spaceButton1 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Dismiss", comment: "إلغاء"), style: .plain , target: self, action: #selector(cancel1))
        toolbar.setItems([doneButton , spaceButton1 ,cancelButton], animated: false)
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        districtTextField.inputView = areaPicker
        districtTextField.inputAccessoryView = toolbar
    }
    
    func showCityPicker(){
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donePicker));
        let spaceButton1 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Dismiss", comment: "إلغاء"), style: .plain , target: self, action: #selector(cancel))
        toolbar.setItems([doneButton , spaceButton1 ,cancelButton], animated: false)
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        cityTextField.inputView = cityPicker
        cityTextField.inputAccessoryView = toolbar
    }
    @objc func cancel1(){
        districtTextField.text = ""
        self.view.endEditing(true)
    }
    @objc func cancel11(){
        districtTextField.text = ""
        self.view.endEditing(true)
    }
    @objc func doneDepartmentPicker(){
        if departmentTextField.text == "" {
            if departments.data?.count ?? 0 > 0 {
                departmentTextField.text = departments.data?[0].name ?? ""
                selectedDepartment = departments.data?[0].id ?? 0
            } else {
                self.dismiss(animated: true, completion: nil)
            }
        }
        self.view.endEditing(true)
    }
    
    @objc func donePicker1(){
        if districtTextField.text == "" {
            if words1.count > 0 {
                districtTextField.text = words1[0]
                selectedArea = selectedCity.areas?[0] ?? Areas()
                areaId = selectedArea.id ?? 0
            } else {
                self.dismiss(animated: true, completion: nil)
            }
        }
        self.view.endEditing(true)
    }
    
    @objc func cancel(){
        cityTextField.text = ""
        self.view.endEditing(true)
    }
    @objc func donePicker(){
        districtTextField.text = ""
        districtTextField.isEnabled = false
        districtTextField.borderColor = #colorLiteral(red: 0.6642242074, green: 0.6642400622, blue: 0.6642315388, alpha: 1)
        if cityTextField.text == "" {
            if words.count > 0 {
                cityTextField.text = words[0]
                selectedCity = CityMain.Instance.cityData.data?[0] ?? CitiesData()
                cityId = selectedCity.id ?? 0
                words1 = selectedCity.getAreasName()
                self.areaPicker.reloadAllComponents()
            } else {
                self.dismiss(animated: true, completion: nil)
            }
        }
        self.view.endEditing(true)
        districtTextField.isEnabled = true
        self.areaPicker.reloadAllComponents()
    }
    @objc func doneservicePicker(){
        serviceTextField.text = ""
        serviceTextField.borderColor = #colorLiteral(red: 0.6642242074, green: 0.6642400622, blue: 0.6642315388, alpha: 1)
        if serviceTextField.text == "" {
            if services.data?.count ?? 0 > 0 {
                serviceTextField.text = services.data?[0].name ?? ""
                serviceId = services.data?[0].id ?? 0
                self.servicePicker.reloadAllComponents()
            } else {
                self.dismiss(animated: true, completion: nil)
            }
        }
        self.view.endEditing(true)
    }
}
//MARK:- ViewSetup
extension filterViewController{
    func setupView(){
        userData.Instance.fetchUser()
        self.title = NSLocalizedString("Search", comment:"ابحث")
        navigationItem.title = NSLocalizedString("Search", comment:"ابحث")
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager.startUpdatingLocation()
        districtTextField.isEnabled = false; self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        let textAttributes = [NSAttributedStringKey.foregroundColor:UIColor.clear]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        userData.Instance.fetchUser()
        if L102Language.currentAppleLanguage() == "ar" {
            cityLabel.textAlignment = .right
            districtionLabel.textAlignment = .right
            serviceLocationLabel.textAlignment = .right
        }
        cityPicker.delegate = self
        cityPicker.dataSource = self
        cityPicker.backgroundColor = .white
        cityPicker.showsSelectionIndicator = true
        departmentPicker.delegate = self
        departmentPicker.dataSource = self
        departmentPicker.backgroundColor = .white
        departmentPicker.showsSelectionIndicator = true
        areaPicker.delegate = self
        areaPicker.dataSource = self
        areaPicker.backgroundColor = .white
        servicePicker.delegate = self
        servicePicker.dataSource = self
        servicePicker.backgroundColor = .white
        servicePicker.showsSelectionIndicator = true
        areaPicker.showsSelectionIndicator = true
        datePicker.backgroundColor = .white
        districtTextField.delegate = self
        cityTextField.delegate = self
        serviceTextField.addPadding(UITextField.PaddingSide.left(20))
        departmentTextField.addPadding(UITextField.PaddingSide.left(20))
        districtTextField.addPadding(UITextField.PaddingSide.left(20))
        cityTextField.addPadding(UITextField.PaddingSide.left(20))
        departmentTextField.setRightViewFAIcon(icon: .FAAngleDown, rightViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
        districtTextField.setRightViewFAIcon(icon: .FAAngleDown, rightViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
        cityTextField.setRightViewFAIcon(icon: .FAAngleDown, rightViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
        serviceTextField.setRightViewFAIcon(icon: .FAAngleDown, rightViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
        getService()
        getDepartment()
        loadCityyData()
        showCityPicker()
        showAreaPicker()
        showServicePicker()
        showDepartmentPicker()
    }
}
//MARK:- Network
extension filterViewController{
    func getDepartment(){
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.getDepartments() , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        print(err.errors!)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.departments = try JSONDecoder().decode(Department.self, from: response.data!)
                        print(value)
                    }catch{
                        let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                        print(error)
                        HUD.flash(.label(lockString), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    func getService(){
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.showservices() , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        print(err.message!)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.services = try JSONDecoder().decode(Services.self, from: response.data!)
                    }catch{
                        let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                        print(error)
                        HUD.flash(.label(lockString), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
}
