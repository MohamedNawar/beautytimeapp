//
//  TableViewCell.swift
//  mashagel
//
//  Created by iMac on 3/31/19.
//  Copyright © 2019 MuhammedAli. All rights reserved.
//

import UIKit

class ShopReservationCell : UITableViewCell {
    var makeCallCallBack: (() -> Void)?
    var getLocationCallBack: (() -> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    @IBAction func makeACall(_ sender: Any) {
        makeCallCallBack!()
    }
    
    
    @IBAction func getLocation(_ sender: Any) {
        getLocationCallBack!()
    }
}
