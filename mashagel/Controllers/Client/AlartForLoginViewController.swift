//
//  AlartForLoginViewController.swift
//  mashagel
//
//  Created by iMac on 10/15/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//

protocol registerOrLogin {
    func registerOrLoginToReserve(flag:Int)
}

import UIKit

class AlartForLoginViewController: UIViewController {
    var delegate:registerOrLogin? = nil
    var backTo = String()
   override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.changeFont()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        userData.Instance.fetchUser()
    }
    
    @IBAction func registerationBtn(_ sender: Any) {
        delegate?.registerOrLoginToReserve(flag: 0)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func loginBtn(_ sender: Any) {
        delegate?.registerOrLoginToReserve(flag: 1)
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func dismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
