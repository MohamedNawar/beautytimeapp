//
//  SginUpAsServiceProviderVC.swift
//  mashagel
//
//  Created by iMac on 3/19/19.
//  Copyright © 2019 MuhammedAli. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD
import FCAlertView

class SginUpAsServiceProviderVC: UIViewController{
    var verificationFlag = 0
    
    @IBOutlet weak var signupBtn: UIButton!
    @IBOutlet weak var verificationImg: UIImageView!
    @IBOutlet weak var view11: UIView!
    @IBOutlet weak var password2Lbl: UILabel!
    @IBOutlet weak var password1Lbl: UILabel!
    @IBOutlet weak var phoneLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var password1TextField: UITextField!
    @IBOutlet weak var password2TextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    
   override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.changeFont()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        password1TextField.delegate = self
        emailTextField.delegate = self
        password2TextField.delegate = self
        nameTextField.delegate = self
        phoneTextField.delegate = self
        nameTextField.addPadding(UITextField.PaddingSide.left(20))
        phoneTextField.addPadding(UITextField.PaddingSide.left(20))
        emailTextField.addPadding(UITextField.PaddingSide.left(20))
        password1TextField.addPadding(UITextField.PaddingSide.left(20))
        password2TextField.addPadding(UITextField.PaddingSide.left(20))
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        _ = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
    @IBAction func LoginBtn(_ sender: Any) {
        PostProfileInfo()
    }
    @IBAction func signIn(_ sender: Any) {
        let navVC = storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(navVC, animated: true)
    }
    @IBAction func verification(_ sender: Any) {
        if verificationFlag == 0 {
            verificationFlag = 1
            signupBtn.isEnabled = true
            verificationImg.isHidden = false
        }else{
            verificationFlag = 0
            signupBtn.isEnabled = false
            verificationImg.isHidden = true
        }
    }
    @IBAction func termsAndConditions(_ sender: Any) {
        let navVC = storyboard?.instantiateViewController(withIdentifier: "AboutUsTermAndConditionsVC") as! AboutUsTermAndConditionsVC
        navVC.flag = 1
        self.navigationController?.pushViewController(navVC, animated: true)
    }
    @IBAction func dismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
//MARK:- Network
extension SginUpAsServiceProviderVC{
    func PostProfileInfo(){
        if self.emailTextField.text == "" || self.nameTextField.text == "" || self.phoneTextField.text == "" ||
            self.password1TextField.text == "" || self.password2TextField.text == "" {
            HUD.flash(.label("Enter your Data"), delay: 1.0)
            if L102Language.currentAppleLanguage() == "ar" {
                HUD.flash(.label("أدخل بياناتك"), delay: 1.0)
            }
            return
        }
        //        if self.password1TextField.text != self.password2TextField.text {
        //            HUD.flash(.label("Your Password Not Identical"), delay: 1.0)
        //
        //            return
        //        }
        let header = APIs.Instance.getHeader()
        let par = ["name": nameTextField.text!, "email": emailTextField.text! , "mobile": "+966\(phoneTextField.text!.replacedArabicDigitsWithEnglish)" , "password": password2TextField.text!.replacedArabicDigitsWithEnglish,"onesignal-player-id" :  UserDefaults.standard.string(forKey: "onesignalid") ?? ""] as [String : Any]
        print(par)
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.registerationAsOwner(), method: .post, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            print(response)
            switch(response.result) {
            case .success( _):
                HUD.hide()
                let temp = response.response?.statusCode ?? 400
                print(temp)
                if temp >= 300 {
                    print("errorrrr")
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "img34"))
                        print(err.message!)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    userData.Instance.saveUser(data: response.data!)
                    userData.Instance.fetchUser()
                    print("successsss")
                    NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: nil)
                    self.dismiss(animated: true, completion: nil)
                    HUD.flash(.success, delay: 1.0)
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }

}
//MARK:- TextField Delegate
extension SginUpAsServiceProviderVC: UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == nameTextField {
            nameLbl.isHidden = false
            textField.borderColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
            textField.borderWidth = 1
        }
        if textField == phoneTextField {
            phoneLbl.isHidden = false
            view11.borderColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        }
        if textField == emailTextField {
            textField.borderColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
            textField.borderWidth = 1
            emailLbl.isHidden = false
        }
        if textField == password1TextField {
            textField.borderColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
            textField.borderWidth = 1
            password1Lbl.isHidden = false
        }
        if textField == password2TextField {
            textField.borderColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
            textField.borderWidth = 1
            password2Lbl.isHidden = false
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text == "" {
            textField.borderColor = #colorLiteral(red: 0.6642242074, green: 0.6642400622, blue: 0.6642315388, alpha: 1)
            if textField == nameTextField {
                nameLbl.isHidden = true
            }
            if textField == phoneTextField {
                phoneLbl.isHidden = true
            }
            if textField == emailTextField {
                emailLbl.isHidden = true
            }
            if textField == password1TextField {
                password1Lbl.isHidden = true
            }
            if textField == password2TextField {
                password2Lbl.isHidden = true
            }
        }
    }
}
