//
//  SignUpOrSignInOrSkipVC.swift
//  mashagel
//
//  Created by iMac on 3/19/19.
//  Copyright © 2019 MuhammedAli. All rights reserved.
//

import UIKit

class SignUpOrSignInOrSkipVC: UIViewController {

   override func viewDidLoad() {
        super.viewDidLoad()
    let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
    let statusBarColor = #colorLiteral(red: 0.8558626771, green: 0.2503757179, blue: 0.4763913751, alpha: 1)
    statusBarView.backgroundColor = statusBarColor
    view.addSubview(statusBarView)
        self.navigationController?.navigationBar.changeFont()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.backBarButtonItem?.tintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        self.navigationController?.navigationBar.backItem?.backBarButtonItem?.tintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
    }
    
    @IBAction func signUp(_ sender: Any) {
        let navVC = storyboard?.instantiateViewController(withIdentifier: "RegisterationViewController") as! RegisterationViewController
        self.navigationController?.pushViewController(navVC, animated: true)
    }
    @IBAction func signIn(_ sender: Any) {
        let navVC = storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.navigationController?.pushViewController(navVC, animated: true)
    }
    @IBAction func skip(_ sender: Any) {
//        NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: nil)
//        self.dismiss(animated: true, completion: nil)
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let vc = storyboard.instantiateViewController(withIdentifier: "TabBarControllerViewController") as! TabBarControllerViewController
//        self.navigationController?.pushViewController(vc, animated: true)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window = UIWindow(frame: UIScreen.main.bounds)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let initialViewController =  storyboard.instantiateViewController(withIdentifier: "TabBarControllerViewController") as! TabBarControllerViewController
        appDelegate.window?.rootViewController = initialViewController
        appDelegate.window?.makeKeyAndVisible()
    }
    @IBAction func signUpAsServiceProvider(_ sender: Any) {
        let navVC = storyboard?.instantiateViewController(withIdentifier: "SginUpAsServiceProviderVC") as! SginUpAsServiceProviderVC
        self.navigationController?.pushViewController(navVC, animated: true)
    }
}
