//
//  RequestSummary2ViewController.swift
//  mashagel
//
//  Created by MACBOOK on 10/1/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD
import FCAlertView
import Font_Awesome_Swift

class RequestSummary2ViewController: UIViewController{
    var reservation = Reservation()
    var reservationData = ReservationData()
    
    @IBOutlet weak var totalPrice: UILabel!
    @IBOutlet weak var dateLable: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
   override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.changeFont()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        tableView.separatorStyle = .none
        userData.Instance.fetchUser()
        if let dateValue = reservationData.date?.date {
            var stringValue = dateValue
            stringValue = stringValue.components(separatedBy: " ")[0]
            if stringValue.isEmpty {
                dateLable.text = "\(stringValue)"
            }
        }
        if let priceValue = reservationData.total_price {
            totalPrice.text = "$\(priceValue)"
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationItem.backBarButtonItem?.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationController?.navigationBar.backItem?.backBarButtonItem?.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.tableView.reloadData()
        if L102Language.currentAppleLanguage() == "ar" {
            totalPrice.textAlignment = .right
        }
    }
    
    @IBAction func canelReservation(_ sender: Any) {
        if reservationData.service_items?.items?.count ?? 0 > 0 {
            let count = reservationData.service_items?.items?.count ?? 0
            for i in 0..<count {
                deleteServiceInReservationShopData(id:self.reservationData.service_items?.items?[i].service?.id ?? -1 , userId : self.reservationData.id ?? -1)
            }
        }
        if reservationData.service_items?.package_items?.count ?? 0 > 0 {
            let count = reservationData.service_items?.package_items?.count ?? 0
            for i in 0..<count {
                deletePackageInReservationShopData(id:self.reservation.data?.service_items?.package_items?[i].id ?? -1 , userId : self.reservationData.id ?? -1)
            }
        }
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        _ = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
}
//MARK:- TableView Delegate and DataSource
extension RequestSummary2ViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if reservationData.service_items?.items?.count ?? 0 > 0 {
            return reservationData.service_items?.items?.count ?? 0
        }
        if reservationData.service_items?.package_items?.count ?? 0 > 0 {
            return reservationData.service_items?.package_items?.count ?? 0
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RequestSummary2Cell", for: indexPath)
        if let dateValue = reservationData.date?.date {
            var stringValue = dateValue
            stringValue = stringValue.components(separatedBy: " ")[0]
            if stringValue.isEmpty {
                dateLable.text = "\(stringValue)"
            }
        }
        if reservationData.service_items?.items?.count ?? 0 > 0 {
            if let titleValue = reservationData.service_items?.items?[indexPath.row].service?.name {
                print(titleValue)
                let title = cell.viewWithTag(1) as! UILabel
                if L102Language.currentAppleLanguage() == "ar" {
                    title.textAlignment = .right
                }
                title.text = titleValue
            }
            if let titleValue = reservationData.service_items?.items?[indexPath.row].assigned_employee_name {
                let title = cell.viewWithTag(2) as! UILabel
                if L102Language.currentAppleLanguage() == "ar" {
                    title.textAlignment = .right
                }
                title.text = titleValue
            }
            if let titleValue = reservationData.service_items?.items?[indexPath.row].amount_of_time?.IInt {
                let title = cell.viewWithTag(3) as! UILabel
                if L102Language.currentAppleLanguage() == "ar" {
                    title.textAlignment = .right
                }
                title.text = "\(titleValue) Minute"
            }
            if var titleValue = reservationData.service_items?.items?[indexPath.row].due_time {
                let toCount = titleValue.count
                titleValue.remove(at: String.Index(encodedOffset: toCount))
                titleValue.remove(at: String.Index(encodedOffset: toCount-1))
                titleValue.remove(at: String.Index(encodedOffset: toCount-2))
                titleValue.characters.removeLast()
                let title = cell.viewWithTag(4) as! UILabel
                if L102Language.currentAppleLanguage() == "ar" {
                    title.textAlignment = .right
                }
                title.text = "\(titleValue)"
            }
        }
        if reservationData.service_items?.package_items?.count ?? 0 > 0 {
            if let titleValue = reservationData.service_items?.package_items?.first?.services?[indexPath.row].name{
                print(titleValue)
                let title = cell.viewWithTag(1) as! UILabel
                if L102Language.currentAppleLanguage() == "ar" {
                    title.textAlignment = .right
                }
                title.text = titleValue
            }
            if let titleValue = reservationData.service_items?.package_items?.first?.items?[indexPath.row].assigned_employee_name {
                let title = cell.viewWithTag(2) as! UILabel
                if L102Language.currentAppleLanguage() == "ar" {
                    title.textAlignment = .right
                }
                title.text = titleValue
            }
            if let titleValue = reservationData.service_items?.package_items?.first?.items?[indexPath.row].amount_of_time?.IInt {
                let title = cell.viewWithTag(3) as! UILabel
                if L102Language.currentAppleLanguage() == "ar" {
                    title.textAlignment = .right
                }
                title.text = "\(titleValue) Minute"
            }
            if var titleValue = reservationData.service_items?.package_items?.first?.items?[indexPath.row].due_time {
                let toCount = titleValue.count
                titleValue.remove(at: String.Index(encodedOffset: toCount))
                titleValue.remove(at: String.Index(encodedOffset: toCount-1))
                titleValue.remove(at: String.Index(encodedOffset: toCount-2))
                titleValue.characters.removeLast()
                let title = cell.viewWithTag(4) as! UILabel
                if L102Language.currentAppleLanguage() == "ar" {
                    title.textAlignment = .right
                }
                title.text = "\(titleValue)"
            }
        }
        return cell
    }
}
//MARK:- NetWork
extension RequestSummary2ViewController{
    func deletePackageInReservationShopData(id:Int , userId : Int){
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.RemovePackage(id: id, identifier: userId) , method: .delete, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "img34"))
                        print(err.message!)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.reservation = try JSONDecoder().decode(Reservation.self, from: response.data!)
                    }catch{
                        print(error)
                      //  HUD.flash(.label("Error Try Again"), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    func deleteServiceInReservationShopData(id:Int , userId : Int){
        let header = APIs.Instance.getHeader()
        let url = (APIs.Instance.RemoveService(id: id, identifier: userId))
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.RemoveService(id: id, identifier: userId) , method: .delete, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                print(url)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "img34"))
                        print(err.message!)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.reservation = try JSONDecoder().decode(Reservation.self, from: response.data!)
                        
                    }catch{
                        print(error)
                      //  HUD.flash(.label("Error Try Again"), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
}
