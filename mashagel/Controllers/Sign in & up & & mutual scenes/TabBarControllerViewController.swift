//
//  TabBarControllerViewController.swift
//  mashagel
//
//  Created by MACBOOK on 10/6/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//

import UIKit

class TabBarControllerViewController: UITabBarController {
    var id = Int()
   override func viewDidLoad() {
        super.viewDidLoad()
    let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
           let statusBarColor = #colorLiteral(red: 0.8558626771, green: 0.2503757179, blue: 0.4763913751, alpha: 1)
           statusBarView.backgroundColor = statusBarColor
           view.addSubview(statusBarView)
        self.navigationController?.navigationBar.changeFont()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        userData.Instance.fetchUser()
        if userData.Instance.token == "" || userData.Instance.token == nil {
            setupTabBar()
            self.selectedIndex = 0
        }else{
            setupTabBarwhenUserLoginAndReserveOutSide()
            self.selectedIndex = 0
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationItem.backBarButtonItem?.isEnabled = false
        self.navigationItem.backBarButtonItem?.tintColor = .clear
        self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        userData.Instance.fetchUser()
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("NotificationIdentifier"), object: nil)
    }
    @objc func methodOfReceivedNotification(notification: Notification) {
        userData.Instance.fetchUser()
        if userData.Instance.token == "" || userData.Instance.token == nil {
            setupTabBar()
            self.selectedIndex = 0
        }else{
            setupTabBarwhenUserLoginAndReserveOutSide()
            self.selectedIndex = 0
        }
    }
    func setupTabBar(){
        let serachVC = storyboard?.instantiateViewController(withIdentifier: "SearchViewController")
        let firstViewController = UINavigationController(rootViewController: serachVC ?? UIViewController())
        firstViewController.tabBarItem.image = UIImage(named: "img10")
        firstViewController.tabBarItem.title = NSLocalizedString("Shop" , comment: "")
        let cartVC = storyboard?.instantiateViewController(withIdentifier: "Cart1ViewController")
        let secondViewController = UINavigationController(rootViewController: cartVC ?? UIViewController())
        secondViewController.tabBarItem.image = UIImage(named: "img08")
        secondViewController.tabBarItem.title = NSLocalizedString("My Cart" , comment: "")
        viewControllers = [firstViewController,secondViewController]
        self.setViewControllers(viewControllers, animated: true)
    }
    func setupTabBarwhenUserLoginAndReserveOutSide(){
        let serachVC = storyboard?.instantiateViewController(withIdentifier: "SearchViewController")
        let firstViewController = UINavigationController(rootViewController: serachVC ?? UIViewController())
        firstViewController.tabBarItem.image = UIImage(named: "img10")
        firstViewController.tabBarItem.title = NSLocalizedString("Shop" , comment: "")
        
        let cartVC = storyboard?.instantiateViewController(withIdentifier: "Cart1ViewController")
        let secondViewController = UINavigationController(rootViewController: cartVC ?? UIViewController())
        secondViewController.tabBarItem.image = UIImage(named: "img08")
        secondViewController.tabBarItem.title = NSLocalizedString("My Cart" , comment: "")
        let profileVC = storyboard?.instantiateViewController(withIdentifier: "PersonalProfileViewController")
        let thirdViewController = UINavigationController(rootViewController: profileVC ?? UIViewController())
        thirdViewController.tabBarItem.image = UIImage(named: "img09")
        thirdViewController.tabBarItem.title = NSLocalizedString("My Profile" , comment: "")
        let notificationVC = storyboard?.instantiateViewController(withIdentifier: "NotificationViewController")
        let fourthViewController = UINavigationController(rootViewController: notificationVC ?? UIViewController())
        fourthViewController.tabBarItem.image = UIImage(named: "img07")
        fourthViewController.tabBarItem.selectedImage = UIImage(named: "img07")
        fourthViewController.tabBarItem.title = NSLocalizedString("My Notifications" , comment: "")
        
        viewControllers = [ firstViewController,secondViewController ,thirdViewController , fourthViewController ]
        self.setViewControllers(viewControllers, animated: false)
    }
}
