//
//  SelectLanguageVC.swift
//  mashagel
//
//  Created by iMac on 3/19/19.
//  Copyright © 2019 MuhammedAli. All rights reserved.
//

import UIKit

class SelectLanguageVC: UIViewController {
    
   override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.changeFont()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
    }
    func changeLangTo(lag:String) {
        L102Language.setAppleLAnguageTo(lang: lag)
        UIView.appearance().semanticContentAttribute = .forceLeftToRight
        let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
        rootviewcontroller.rootViewController = self.storyboard?.instantiateViewController(withIdentifier: "SignUpOrSignInOrSkipNV")
        let mainwindow = (UIApplication.shared.delegate?.window!)!
        mainwindow.backgroundColor = UIColor(hue: 0.6477, saturation: 0.6314, brightness: 0.6077, alpha: 0.8)
        UIView.transition(with: mainwindow, duration: 0, options: .transitionFlipFromLeft, animations: { () -> Void in
        }) { (finished) -> Void in
        }
    }
    
    @IBAction func arabicLang(_ sender: Any) {
        changeLangTo(lag:"ar")
        UserDefaults.standard.set("false", forKey: "firstUse")
    }
    @IBAction func englishLang(_ sender: Any) {
        changeLangTo(lag:"en")
        UserDefaults.standard.set("false", forKey: "firstUse")
    }
}
