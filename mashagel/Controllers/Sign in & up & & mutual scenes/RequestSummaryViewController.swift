//
//  RequestSummaryViewController.swift
//  mashagel
//
//  Created by MACBOOK on 9/26/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD
import FCAlertView
import Font_Awesome_Swift

class RequestSummaryViewController: UIViewController{
    var reservation = Reservation()
    var count = Int()
    var reservationData = ReservationData()
    
    @IBOutlet weak var timeLable: UILabel!
    @IBOutlet weak var totalPrice: UILabel!
    @IBOutlet weak var dateLable: UILabel!
    @IBOutlet weak var editBtnView: UIButton!
    @IBOutlet weak var cancelBtnView: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
   override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.changeFont()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        tableView.separatorStyle = .none
        userData.Instance.fetchUser()
        if let dateValue = reservationData.date?.date {
            var stringValue = dateValue
            stringValue = stringValue.components(separatedBy: " ")[0]
            dateLable.text = stringValue
        }
        timeLable.text = reservationData.due_time

        if let priceValue = reservationData.total_price {
            if L102Language.currentAppleLanguage() == "ar" {
                totalPrice.text = "ريال \(priceValue)"
            }else{
                totalPrice.text = "\(priceValue) SR"
            }
        }
        if reservationData.status == "cancelled" {
            cancelBtnView.isHidden = true
            editBtnView.isHidden = true
        }else{
            cancelBtnView.isHidden = false
            editBtnView.isHidden = false
        }
    if reservationData.can_cancel == false {
        cancelBtnView.isHidden = true
        editBtnView.isHidden = true
    }else{
        cancelBtnView.isHidden = false
        editBtnView.isHidden = false
    }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationItem.backBarButtonItem?.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationController?.navigationBar.backItem?.backBarButtonItem?.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.tableView.reloadData()
        if L102Language.currentAppleLanguage() == "ar" {
            totalPrice.textAlignment = .right
        }
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        _ = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
    func showChangeDialog(){
        let alert = FCAlertView()
        alert.makeAlertTypeCaution()
        alert.colorScheme = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        if L102Language.currentAppleLanguage() == "ar" {
            alert.addButton("موافق") {
                UserDefaults.standard.set("editFlag", forKey: "editFlag")
                UserDefaults.standard.set("\(self.reservationData.id ?? 0)", forKey: "editReservationID")
                self.PreparingReservation()
            }
            alert.showAlert(inView: self, withTitle: "تحذير", withSubtitle: "سيكون هذا الحجز معلقا في حالة عدم أكتمال عملية التعديل", withCustomImage: nil, withDoneButtonTitle: "تخطي" , andButtons: nil)
        }else{
            alert.addButton(NSLocalizedString("Ok", comment: "")) {
                UserDefaults.standard.set("editFlag", forKey: "editFlag")
                UserDefaults.standard.set("\(self.reservationData.id ?? 0)", forKey: "editReservationID")
                self.PreparingReservation()
            }
            alert.showAlert(inView: self, withTitle: NSLocalizedString("WARNING", comment: ""), withSubtitle: NSLocalizedString("This reservtion will be on hold in case of incomplete editing process", comment: ""), withCustomImage: nil, withDoneButtonTitle: NSLocalizedString("Cancel", comment: ""), andButtons: nil)
        }
    }
    @IBAction func map(_ sender: Any) {
        let navVC = self.storyboard?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        navVC.shop = self.reservationData.shop ?? ShopData()
        self.navigationController?.pushViewController(navVC, animated: true)
    }
    @IBAction func editReservation(_ sender: Any) {
        showChangeDialog()
    }
    @IBAction func canelReservation(_ sender: Any) {
        if reservationData.service_items?.items?.count ?? 0 > 0 {
            let count = reservationData.service_items?.items?.count ?? 0
            for i in 0..<count {
                deleteServiceInReservationShopData(id:self.reservationData.service_items?.items?[i].service?.id ?? -1 , userId : self.reservationData.id ?? -1)
            }
        }
        if reservationData.packages?.count ?? 0 > 0 {
            let count = reservationData.packages?.count ?? 0
            for i in 0..<count {
                deletePackageInReservationShopData(id:self.reservationData.packages?[i].id ?? -1 , userId : self.reservationData.id ?? -1)
            }
        }
    }
}
//MARK:- TableView Delegate and DataSource
extension RequestSummaryViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        count = reservationData.packages?.count ?? 0
        if reservationData.service_items?.items?.count ?? 0 > 0 {
            count+=1
        }
        print(count)
        return count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if reservationData.service_items?.items?.count ?? 0 > 0 {
            if section == count-1 {
                return reservationData.service_items?.items?.count ?? 0
            }
        }else{
            print(reservationData.packages?[section].services?.count ?? 0)
            return reservationData.packages?[section].services?.count ?? 0
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RequestSummaryCell", for: indexPath)
        if reservationData.service_items?.items?.count ?? 0 > 0 {
            if indexPath.section == count-1 {
                let title = cell.viewWithTag(11) as! UILabel
                let title1 = cell.viewWithTag(3) as! UILabel
                let title2 = cell.viewWithTag(2) as! UILabel
                if let titleValue = reservationData.service_items?.items?[indexPath.row].service?.name {
                    print(titleValue)
                    title.text = titleValue
                }
                if let titleValue = reservationData.service_items?.items?[indexPath.row].quantity {
                    if L102Language.currentAppleLanguage() == "ar" {
                        title2.text = "عدد الاشخاص: " + "\(titleValue)"
                        title2.textAlignment = .right
                    }else{
                        title2.text = NSLocalizedString("Number Of Persons: ", comment: "") + "\(titleValue)"
                        title2.textAlignment = .right
                    }
                }
                if let titleValue = reservationData.service_items?.items?[indexPath.row].amount_of_time?.SString {
                    title1.text = "\(titleValue)" + NSLocalizedString(" Minutes", comment: "")
                    if L102Language.currentAppleLanguage() == "ar" {
                        title1.text = "\(titleValue)" + NSLocalizedString(" دقيقة", comment: "")
                        title1.textAlignment = .right
                    }
                }
                if L102Language.currentAppleLanguage() == "ar" {
                    title1.textAlignment = .right
                    title.textAlignment = .right
                    title2.textAlignment = .left
                }
            }
        }else{
            if let titleValue = reservationData.packages?[indexPath.section].services?[indexPath.row].name {
                print(titleValue)
                let title = cell.viewWithTag(11) as! UILabel
                if L102Language.currentAppleLanguage() == "ar" {
                    title.textAlignment = .right
                }
                title.text = titleValue
            }
            let title = cell.viewWithTag(2) as! UILabel
            if L102Language.currentAppleLanguage() == "ar" {
                title.textAlignment = .right
            }
            if let titleValue = reservationData.packages?[indexPath.section].services?[indexPath.row].quantity {
                title.text = NSLocalizedString("Number Of Persons: ", comment: "") + "\(titleValue)"
                if L102Language.currentAppleLanguage() == "ar" {
                    title.text = NSLocalizedString("عدد الأشخاص: ", comment: "") + "\(titleValue)"
                    title.textAlignment = .right
                }
            }else{
                title.text = NSLocalizedString("Number Of Persons: 1", comment: "")
                if L102Language.currentAppleLanguage() == "ar" {
                    title.textAlignment = .right
                    title.text = NSLocalizedString("عدد الأشخاص: ١ ", comment: "")
                    title.textAlignment = .right
                }
            }
            if let titleValue = reservationData.packages?[indexPath.section].services?[indexPath.row].amount_of_time?.SString {
                let title = cell.viewWithTag(3) as! UILabel
                title.text = "\(titleValue)" + NSLocalizedString(" Minutes", comment: "")
                if L102Language.currentAppleLanguage() == "ar" {
                    title.text = "\(titleValue)" + NSLocalizedString(" دقيقة", comment: "")
                    title.textAlignment = .right
                }
            }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerCell = tableView.dequeueReusableCell(withIdentifier: "RequestSummaryHeaderCell")
        if let titleValue = reservationData.packages?[section].name {
            print(titleValue)
            let title = headerCell?.viewWithTag(11) as! UILabel
            if L102Language.currentAppleLanguage() == "ar" {
                title.textAlignment = .right
            }
            title.text = titleValue
        }
        return headerCell
        //
        //        if reservationData.service_items?.items?.count ?? 0 > 0 {
        //            if section == count-1 {
        //                return UIView()
        //            }
        //        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if reservationData.service_items?.items?.count ?? 0 > 0 {
            if section == count-1 {
                return 0.0
            }else{
                return 40
            }
        }else{
            return 40
        }
    }
}
//MARK:- NetWork
extension RequestSummaryViewController{
    func PreparingReservation(){
        var header = APIs.Instance.getHeader()
        header.updateValue("application/x-www-form-urlencoded", forKey: "Content-Type")
        header.updateValue("\(reservationData.id ?? 0)", forKey: "reservation_id")
        print(header)
        let par = ["reservation_id": "\(reservationData.id ?? 0)"] as [String : Any];
        print(par)
        print(APIs.Instance.SendDateForCurrentReservationForUpdate(id:"\(reservationData.id ?? 0)"));
        userData.Instance.fetchUser()
        Alamofire.request(APIs.Instance.SendDateForCurrentReservationForUpdate(id:"\(reservationData.id ?? 0)"), method: .patch , parameters: par, encoding: URLEncoding(), headers: header)
            .responseJSON { (response:DataResponse<Any>) in
                
                switch(response.result) {
                case .success(let value):
                    print(value)
                    HUD.hide()
                    print(value)
                    let temp = response.response?.statusCode ?? 400
                    if temp >= 300 {
                        do {
                            let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                            print(err.message!)
                            let navVC = self.storyboard?.instantiateViewController(withIdentifier: "Cart1ViewController") as! Cart1ViewController
                            navVC.reserVationId = self.reservationData.id ?? 0
                            navVC.flag = "tt"
                            navVC.reserVation1Id = self.reservationData.id ?? 0
                            self.navigationController?.pushViewController(navVC, animated: true)
                            self.tableView.reloadData()
                        }catch{
                            let navVC = self.storyboard?.instantiateViewController(withIdentifier: "Cart1ViewController") as! Cart1ViewController
                            navVC.reserVationId = self.reservationData.id ?? 0
                            navVC.flag = "tt"
                            navVC.reserVation1Id = self.reservationData.id ?? 0
                            self.navigationController?.pushViewController(navVC, animated: true)
                            print("errorrrrelse")
                        }
                    }else{
                            let navVC = self.storyboard?.instantiateViewController(withIdentifier: "Cart1ViewController") as! Cart1ViewController
                            navVC.reserVationId = self.reservationData.id ?? 0
                            navVC.flag = "tt"
                            navVC.reserVation1Id = self.reservationData.id ?? 0
                            self.navigationController?.pushViewController(navVC, animated: true)
                    }
                case .failure(_):
                    let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                    HUD.flash(.label(lockString), delay: 1.0)
                    break
                }
        }
    }
    func deletePackageInReservationShopData(id:Int , userId : Int){
        let header = APIs.Instance.getHeader()
        let url = (APIs.Instance.RemovePackage(id: id, identifier: userId))
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.RemovePackage(id: id, identifier: userId) , method: .delete, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                print(url)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "img34"))
                        print(err.message!)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.reservation = try JSONDecoder().decode(Reservation.self, from: response.data!)
                    }catch{
                        print(error)
                      //  HUD.flash(.label("Error Try Again"), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    func deleteServiceInReservationShopData(id:Int , userId : Int){
        let header = APIs.Instance.getHeader()
        let url = (APIs.Instance.RemoveService(id: id, identifier: userId))
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.RemoveService(id: id, identifier: userId) , method: .delete, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                print(url)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "img34"))
                        print(err.message!)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.reservation = try JSONDecoder().decode(Reservation.self, from: response.data!)
                    }catch{
                        print(error)
                      //  HUD.flash(.label("Error Try Again"), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
}
extension UINavigationBar {
    func changeFont() {
        self.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont(name:"Cairo", size: 18)!]
    }
}
