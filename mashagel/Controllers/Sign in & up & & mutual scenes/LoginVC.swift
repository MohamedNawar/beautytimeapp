//
//  LoginVC.swift
//  mashagel
//
//  Created by ElNoorOnline on 12/31/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//

import UIKit
import PKHUD
import FCAlertView
import GoogleSignIn
import FBSDKCoreKit
import FBSDKLoginKit
import TwitterKit
import Alamofire
import Firebase
import FirebaseAuth

// Match the ObjC symbol name inside Main.storyboard.
@objc(LoginVC)
class LoginVC: UIViewController ,GIDSignInUIDelegate {
    @IBOutlet weak var password1Lbl: UILabel!
    @IBOutlet weak var phoneLbl: UILabel!
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var email_txtField: UITextField!
    @IBOutlet weak var code_txtField: UITextField!
    @IBOutlet weak var verificationCodeTxt: UIView!
    @IBOutlet weak var useAnotherPhone: UIButton!
    @IBOutlet weak var phoneView: UIView!
    
    var currentUser =  CurrentUser()
    var idToken:String = ""
    var social_media_login:String = ""
    var firebaseLoginState = "1"
    var verificationID = ""
   override func viewDidLoad() {
        super.viewDidLoad()
    let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
    let statusBarColor = #colorLiteral(red: 0.8558626771, green: 0.2503757179, blue: 0.4763913751, alpha: 1)
    statusBarView.backgroundColor = statusBarColor
    view.addSubview(statusBarView)
        self.navigationController?.navigationBar.changeFont()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        email_txtField.delegate = self
        code_txtField.delegate = self
        email_txtField.addPadding(UITextField.PaddingSide.left(20))
        code_txtField.addPadding(UITextField.PaddingSide.left(20))
        /*  google login */
        //adding the delegates
        GIDSignIn.sharedInstance().uiDelegate = self
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.receiveToggleAuthUINotification(_:)),
                                               name: NSNotification.Name(rawValue: "ToggleAuthUINotification"),
                                               object: nil)
    }
    deinit {
        NotificationCenter.default.removeObserver(self,
                                                  name: NSNotification.Name(rawValue: "ToggleAuthUINotification"),
                                                  object: nil)
    }
    @objc func receiveToggleAuthUINotification(_ notification: NSNotification) {
        if notification.name.rawValue == "ToggleAuthUINotification" {
            if notification.userInfo != nil {
                guard let userInfo = notification.userInfo as? [String:String] else { return }
                //        self.statusText.text = userInfo["statusText"]!
                idToken = userInfo["statusText"]!
                print("-----\(idToken)")
                if idToken != "" {
                    call_api(googleLogin: true)
                }
                //
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
     configureView()
    }
    
    func configureView() {
       firebaseLoginState = "1"
       verificationID = ""
        code_txtField.isUserInteractionEnabled = false
       verificationCodeTxt.isHidden = true
       useAnotherPhone.isHidden = true
       phoneView.isHidden = false
    }
    
    /* sign_up */
    @IBAction func sign_up(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "RegisterationViewController") as! RegisterationViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    /* login ==> phone & passsword*/
    @IBAction func login_btn(_ sender: Any) {
        if firebaseLoginState == "1" {
               loginFirebase()
        }else{
          verifyCode()
        }
    }
    
    @IBAction func useAnotherPhone(_ sender: Any) {
       configureView()
        email_txtField.text = ""
    }
    
    func verifyCode() {
            
            if code_txtField.text == "" {
                
                HUD.flash(.labeledError(title: "", subtitle: NSLocalizedString("Please enter the Code", comment: "")), delay: 1.0)
                return
            }
                HUD.show(.progress)
                print(UserDefaults.standard.string(forKey: "authVID"))
                let defaults = UserDefaults.standard
                print(defaults.string(forKey: "authVID")!)
                let credential: PhoneAuthCredential = PhoneAuthProvider.provider().credential(withVerificationID: defaults.string(forKey: "authVID")!, verificationCode: self.code_txtField.text!.replacedArabicDigitsWithEnglish)
                print(credential)
                
                Auth.auth().signIn(with: credential) { (authResult, error) in
                    if error != nil {
                        print("error: \(String(describing: error?.localizedDescription))")
                        
                        HUD.hide()
                        self.makeDoneAlert(title: "", SubTitle: NSLocalizedString("Invalid code Please try again or check your phone number", comment: ""), Image: #imageLiteral(resourceName: "img34"))
                    } else {
                        authResult?.user.getIDToken(completion: { (token, error) in
                            if error != nil {
                                print("error: \(String(describing: error?.localizedDescription))")
                                
                                self.makeDoneAlert(title: "", SubTitle: NSLocalizedString("Invalid code Please try again or check your phone number", comment: ""), Image: #imageLiteral(resourceName: "2-5"))
                                HUD.hide()
                            } else{
                                self.login(id:token ?? "")
                            }
                        })
                        
                    }
            }
        }
    
    func loginFirebase() {
        if email_txtField.text == "" || email_txtField.text == nil {
            HUD.flash(.labeledError(title: "", subtitle: NSLocalizedString("Please enter your phone number", comment: "")), delay: 1.0)
            return
        }
        HUD.show(.progress)
        PhoneAuthProvider.provider().verifyPhoneNumber("+966\(self.email_txtField.text!.replacedArabicDigitsWithEnglish)", uiDelegate: nil) { (verificationID, error) in
            if error != nil {
                print("eror: \(String(describing: error?.localizedDescription))")
                HUD.hide()
            } else {
                print(verificationID!)
                let defaults = UserDefaults.standard
                defaults.set(verificationID, forKey: "authVID")
                self.verificationID = verificationID!
                self.verificationCodeTxt.isHidden = false
                self.useAnotherPhone.isHidden = false
                self.phoneView.isHidden = true
                self.firebaseLoginState = "2"
                self.code_txtField.isUserInteractionEnabled = true
                HUD.hide()
            }
        }
    }
        func login(id:String){
            
            let par = ["id_token":"\(id)",
                "onesignal-player-id" :  UserDefaults.standard.string(forKey: "onesignalid") ?? "",
                "mobile":"+966\(email_txtField.text ?? "")"] as [String : Any]
            let headers = ["Content-Type": "application/x-www-form-urlencoded",
                           "Accept":"application/json",
                           "Accept-Language":L102Language.currentAppleLanguage()]
            print(par)
            HUD.show(.progress)
            Alamofire.request(APIs.Instance.firebase_login(), method: .post, parameters: par, encoding: URLEncoding.default, headers: headers).responseJSON { (response:DataResponse) in
                HUD.hide()
                switch(response.result) {
                case .success(let value):
                    print(value)
                    let temp = response.response?.statusCode ?? 400
                    if temp >= 300 {
                        do {
                            let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data ?? Data())
                            self.makeDoneAlert(title: "", SubTitle: NSLocalizedString(err.parseError() ?? "", comment: ""), Image: #imageLiteral(resourceName: "img21-1"))
                            

                            
                        }catch{
                            
                            HUD.flash(.labeledError(title: "", subtitle: "حدث خطا بالرجاء التأكد من صحة البيانات المدخلة"), delay: 1.0)
                        }
                    }else{
                        if response.data != nil {
                            do{
                                self.configureView()
                               self.currentUser = try JSONDecoder().decode(CurrentUser.self, from: response.data!)
                                userData.Instance.saveUser(data: response.data!)
                                UserDefaults.standard.set(self.currentUser.token ?? "", forKey: "token")
                                userData.Instance.saveUser(data: response.data!)
                                userData.Instance.fetchUser()
                                print("successsss")
                                if self.currentUser.data?.type == "2" {
                                   self.goToAdmin()
                                }else{
                                   self.goToMainScreen()
                                }
                            }catch{
                                print(error)
                                
                                
                                
                            }
                            
                        }else {
                            HUD.flash(.labeledError(title: "", subtitle: "حدث خطا بالرجاء التأكد من صحة البيانات المدخلة"), delay: 1.0)
                        }
                    }
                    
                case .failure(_):
                    HUD.flash(.labeledError(title: "", subtitle:"حدث خطأ برجاء إعادة المحاولة لاحقاً"), delay: 1.0)
                    
                    break
                    
                }
            }
        }
    @IBAction func serviceProvider(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    /* signin later */
    @IBAction func signIn_later(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: nil)
        self.dismiss(animated: true, completion: nil)
        goToMainScreen()
    }
    /* login ==> faceook*/
    @IBAction func faceBookLogin_btn(_ sender: Any) {
        let fbLoginManger: FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManger.logIn(withReadPermissions: ["email"], from: self) { (result, error) in
            if (error == nil){
                let fBLoginresult :FBSDKLoginManagerLoginResult = result!
                print("accessToken:  \(String(describing: result?.token!))")
                if result?.token != nil {
                    self.socialMediaService(flag: true, token: "\(String(describing: result?.token!))")
                    if fBLoginresult.grantedPermissions != nil {
                        if(fBLoginresult.grantedPermissions.contains("email")){
                            self.getFBUserData()
                        }
                    }
                }
            }else{
                print(error!)
            }
        }
    }
    /* login ==> faceook*/
    @IBAction func TwitterLogin_btn(_ sender: Any) {
        TWTRTwitter.sharedInstance().logIn { (session, error) in
            if(session != nil){
                print("done ")
                print(session?.authToken ?? "")
                self.socialMediaService(flag: false, token: session?.authToken ?? "")
            }else{
                print("no")
            }
        }
        
    }
    /* login ==> Google */
    @IBAction func googleLogin_btn(_ sender: Any) {
        GIDSignIn.sharedInstance().signIn()
    }
    
    // MARK: - faceBook login
    func loginBtnDidLogout(){}
    func getFBUserData(){
        if (FBSDKAccessToken.current() != nil) {
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"id, name, first_name, last_name, picture.type(large), email"])?.start(completionHandler: { (connection, result, error) in
                if (error == nil){
                    let faceDict = result as! [String:AnyObject]
                    print(faceDict)
                }
            })
        }
    }
    func goToMainScreen(){
        let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        rootviewcontroller.rootViewController = storyboard.instantiateViewController(withIdentifier: "TabBarControllerViewController")
    }
    func goToAdmin()  {
        let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        rootviewcontroller.rootViewController = storyboard.instantiateViewController(withIdentifier: "AllBeautyCenterForAdminViewController")
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        _ = alert.bounds
        alert.colorScheme = UIColor.init(patternImage: UIImage());
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
}

//MARK:- TextField Delegate
extension LoginVC: UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == email_txtField {
            emailView.borderColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
            phoneLbl.isHidden = false
        }
        if textField == code_txtField {
            textField.borderColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
            textField.borderWidth = 1
            password1Lbl.isHidden = false
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text == "" {
            if textField == email_txtField {
                phoneLbl.isHidden = true
            }
            if textField == code_txtField {
                password1Lbl.isHidden = true
            }
        }
    }
}
//MARK:- Network
extension LoginVC{
    func call_api(googleLogin: Bool){
        HUD.show(.progress, onView: self.view)
        var param:[String:Any] = [:]
        var request = ""
        if googleLogin  {
            social_media_login = "google"
            param =  [ "access_token" : idToken,
                       "driver" : social_media_login,
                       "onesignal-player-id" :  UserDefaults.standard.string(forKey: "onesignalid") ?? ""]
            request = APIs.Instance.googleLogin()
        }else {
            param = [ "email" : "+966\(email_txtField.text!.replacedArabicDigitsWithEnglish)",
                           "password" : code_txtField.text!.replacedArabicDigitsWithEnglish,
                           "onesignal-player-id": UserDefaults.standard.string(forKey: "onesignalid") ?? ""
                       ]
            //  "onesignal-player-id":UserDefaults.standard.string(forKey: "onesignalid")
            request = APIs.Instance.login()
        }
        print(param)
        Alamofire.request(request , method: .post, parameters: param, encoding: URLEncoding.default, headers: APIs.Instance.getHeader()).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        print(err.message!)
                        self.makeDoneAlert(title: NSLocalizedString("Error", comment: ""), SubTitle: err.message ?? "", Image:  UIImage())
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.currentUser = try JSONDecoder().decode(CurrentUser.self, from: response.data!)
                        userData.Instance.saveUser(data: response.data!)
                        UserDefaults.standard.set(self.currentUser.token ?? "", forKey: "token")
                        userData.Instance.saveUser(data: response.data!)
                        userData.Instance.fetchUser()
                        print("successsss")
                        if self.currentUser.data?.type == "2" {
                            self.goToAdmin()
                        }else{
                            self.goToMainScreen()
                        }
                    }catch{
                        HUD.flash(.label(NSLocalizedString("Error Try Again", comment: "حدث خطأ برجاء اعادة المحاولة")), delay: 1.0)
                        print(error)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
    func socialMediaService(flag: Bool , token:String){
        HUD.show(.progress, onView: self.view)
        var param:[String:Any] = [:]
        if flag  {
            param =  [ "access_token" : token ,
                       "driver" : "facebook",
                       "onesignal-player-id" :  UserDefaults.standard.string(forKey: "onesignalid") ?? "15161cac-5ed4-46c9-9c95-81dae96401fc"]
        }else {
            param =  [ "access_token" : token,
                       "driver" : "twitter",
                       "secret" : "Q5bDqFtQRFtJIkkLdTG1GMA9AHdhGPuiCKYLfTG8BVUTmLRWKJ",
                       "onesignal-player-id" :  UserDefaults.standard.string(forKey: "onesignalid") ?? "15161cac-5ed4-46c9-9c95-81dae96401fc"]
            //  "onesignal-player-id":UserDefaults.standard.string(forKey: "onesignalid")
        }
        let  request = APIs.Instance.googleLogin()
        print(param)
        callApi(vc:self, withURL: request, method: .post, headers: APIs.Instance.getHeader(), Parameter: param) { (result) in
            HUD.hide()
            print("----done ----")
            switch result {
            case .success(let data):
                // let user_data :userData   = try JSONDecoder().decode(userData.self, from: data)
                userData.Instance.saveUser(data: data)
                userData.Instance.fetchUser()
                print("successsss")
                NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: nil)
                self.dismiss(animated: true, completion: nil)
                if userData.Instance.data?.type == "2" {
                    self.goToAdmin()
                }else{
                    self.goToMainScreen()
                }
                HUD.flash(.success, delay: 1.0)
            default:
                print("-- error")
            }
        }
    }
}
