//
//  RegisterationViewController.swift
//  mashagel
//
//  Created by MACBOOK on 9/26/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD
import FCAlertView

class RegisterationViewController: UIViewController , UITextFieldDelegate {
    var backTo = String()
    let cityPicker = UIPickerView()
    let areaPicker = UIPickerView()
    var selectedCity = CitiesData()
    var selectedArea = Areas()
    var words = ["", "", "", "", "", " ", ""]
    var selected = ""
    var words1 = ["", "", "", "", "", " ", ""]
    var selected1 = ""
    var cityId :Int = 0
    var areaId :Int = 0
    var selectedCityIndex = 0
    var verificationFlag = 0
    
    @IBOutlet weak var view11: UIView!
    @IBOutlet weak var signupBtn: UIButton!
    @IBOutlet weak var verificationImg: UIImageView!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var cityLbl: UILabel!
    @IBOutlet weak var areaLbl: UILabel!
    @IBOutlet weak var districtTextField: UITextField!
    @IBOutlet weak var password2Lbl: UILabel!
    @IBOutlet weak var password1Lbl: UILabel!
    @IBOutlet weak var phoneLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var password1TextField: UITextField!
    @IBOutlet weak var password2TextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    
   override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.changeFont()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        cityPicker.delegate = self
        areaPicker.delegate = self
        cityPicker.dataSource = self
        cityPicker.backgroundColor = .white
        cityPicker.showsSelectionIndicator = true
        areaPicker.delegate = self
        areaPicker.dataSource = self
        areaPicker.backgroundColor = .white
        areaPicker.showsSelectionIndicator = true
        districtTextField.setRightViewFAIcon(icon: .FAAngleDown, rightViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
        cityTextField.setRightViewFAIcon(icon: .FAAngleDown, rightViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
        password1TextField.delegate = self
        emailTextField.delegate = self
        password2TextField.delegate = self
        nameTextField.delegate = self
        phoneTextField.delegate = self
        districtTextField.delegate = self
        cityTextField.delegate = self
        nameTextField.addPadding(UITextField.PaddingSide.left(20))
        phoneTextField.addPadding(UITextField.PaddingSide.left(20))
        emailTextField.addPadding(UITextField.PaddingSide.left(20))
        password1TextField.addPadding(UITextField.PaddingSide.left(20))
        password2TextField.addPadding(UITextField.PaddingSide.left(20))
        districtTextField.addPadding(UITextField.PaddingSide.left(20))
        cityTextField.addPadding(UITextField.PaddingSide.left(20))
        loadCityyData()
        showCityPicker()
        showAreaPicker()
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == nameTextField {
            textField.borderColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
            textField.borderWidth = 1
            nameLbl.isHidden = false
        }
        if textField == phoneTextField {
            view11.borderColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
            phoneLbl.isHidden = false
        }
        if textField == emailTextField {
            textField.borderColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
            textField.borderWidth = 1
            emailLbl.isHidden = false
        }
        if textField == password1TextField {
            textField.borderColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
            textField.borderWidth = 1
            password1Lbl.isHidden = false
        }
        if textField == password2TextField {
            textField.borderColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
            textField.borderWidth = 1
            password2Lbl.isHidden = false
        }
        if textField == districtTextField {
            textField.borderColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
            textField.borderWidth = 1
            areaLbl.isHidden = false
        }
        if textField == cityTextField {
            textField.borderColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
            textField.borderWidth = 1
            cityLbl.isHidden = false
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text == "" {
            if textField == nameTextField {
                textField.borderColor = #colorLiteral(red: 0.6642242074, green: 0.6642400622, blue: 0.6642315388, alpha: 1)
                nameLbl.isHidden = true
            }
            if textField == phoneTextField {
                phoneLbl.isHidden = true
            }
            if textField == emailTextField {
                textField.borderColor = #colorLiteral(red: 0.6642242074, green: 0.6642400622, blue: 0.6642315388, alpha: 1)
                emailLbl.isHidden = true
            }
            if textField == password1TextField {
                textField.borderColor = #colorLiteral(red: 0.6642242074, green: 0.6642400622, blue: 0.6642315388, alpha: 1)
                password1Lbl.isHidden = true
            }
            if textField == password2TextField {
                textField.borderColor = #colorLiteral(red: 0.6642242074, green: 0.6642400622, blue: 0.6642315388, alpha: 1)
                password2Lbl.isHidden = true
            }
            if textField == districtTextField {
                textField.borderColor = #colorLiteral(red: 0.6642242074, green: 0.6642400622, blue: 0.6642315388, alpha: 1)
                areaLbl.isHidden = true
            }
            if textField == cityTextField {
                textField.borderColor = #colorLiteral(red: 0.6642242074, green: 0.6642400622, blue: 0.6642315388, alpha: 1)
                cityLbl.isHidden = true
            }
        }
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        _ = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
    
    @IBAction func LoginBtn(_ sender: Any) {
        PostProfileInfo()
    }
    @IBAction func signIn(_ sender: Any) {
        let navVC = storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.navigationController?.pushViewController(navVC, animated: true)
    }
    @IBAction func verification(_ sender: Any) {
        if verificationFlag == 0 {
            verificationFlag = 1
            signupBtn.isEnabled = true
            verificationImg.isHidden = false
        }else{
            verificationFlag = 0
            signupBtn.isEnabled = false
            verificationImg.isHidden = true
        }
    }
    @IBAction func termsAndConditions(_ sender: Any) {
        let navVC = storyboard?.instantiateViewController(withIdentifier: "AboutUsTermAndConditionsVC") as! AboutUsTermAndConditionsVC
        navVC.flag = 1
        self.navigationController?.pushViewController(navVC, animated: true)
    }
}
//MARK:- Network
extension RegisterationViewController{
    func loadCityyData() {
        HUD.show(.progress, onView: self.view)
        CityMain.Instance.getCitiesServer(enterDoStuff: { () in
            self.words = CityMain.Instance.getCitiesNameArr()
            self.cityPicker.reloadAllComponents()
            HUD.hide(animated: true)
        })
    }
    func PostProfileInfo(){
        if self.emailTextField.text == "" || self.nameTextField.text == "" || self.phoneTextField.text == "" || self.districtTextField.text == "" ||
            self.password1TextField.text == "" || self.password2TextField.text == "" ||
            self.districtTextField.text == "" {
            HUD.flash(.label("Enter your Data"), delay: 1.0)
            if L102Language.currentAppleLanguage() == "ar" {
                HUD.flash(.label("أدخل بياناتك"), delay: 1.0)
            }
            return
        }
        let header = APIs.Instance.getHeader()
        let par = ["name": nameTextField.text!, "email": emailTextField.text! , "mobile": "+966\(phoneTextField.text!.replacedArabicDigitsWithEnglish)", "area_id": areaId  , "password": password2TextField.text!.replacedArabicDigitsWithEnglish,"onesignal-player-id" :  UserDefaults.standard.string(forKey: "onesignalid") ?? ""] as [String : Any]
        print(par)
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.registeration(), method: .post, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            print(response)
            switch(response.result) {
            case .success( _):
                HUD.hide()
                let temp = response.response?.statusCode ?? 400
                print(temp)
                if temp >= 300 {
                    print("errorrrr")
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "img34"))
                        print(err.message!)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        userData.Instance.saveUser(data: response.data!)
                        userData.Instance.fetchUser()
                        print("successsss")
                        NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: nil)
                        self.dismiss(animated: true, completion: nil)
                        HUD.flash(.success, delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
}
//MARK:- PickerView Delegate and DataSource
extension RegisterationViewController: UIPickerViewDelegate, UIPickerViewDataSource{
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == cityPicker {
            return words.count
        }
        if pickerView == areaPicker {
            return words1.count
        }
        return 0
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == cityPicker {
            return "\(words[row])"
        }
        if pickerView == areaPicker {
            return "\(words1[row])"
        }
        return ""
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == cityPicker {
            selected = words[row]
            selectedCity = CityMain.Instance.cityData.data?[row] ?? CitiesData()
            selectedCityIndex = row
            words1 = selectedCity.getAreasName()
            cityId = selectedCity.id ?? 0
            cityTextField.text = selected
            areaPicker.reloadAllComponents()
        }
        if pickerView == areaPicker {
            selected1 = words1[row]
            selectedArea = selectedCity.areas?[row] ?? Areas()
            areaId = selectedArea.id ?? 0
            districtTextField.text = selected1
        }
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
}
//MARK:- PickerView Methods
extension RegisterationViewController{
    func showAreaPicker(){
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donePicker1));
        let spaceButton1 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Dismiss", comment: "إلغاء"), style: .plain , target: self, action: #selector(cancel1))
        toolbar.setItems([doneButton , spaceButton1 ,cancelButton], animated: false)
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        districtTextField.inputView = areaPicker
        districtTextField.inputAccessoryView = toolbar
    }
    func showCityPicker(){
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: "تم"), style: .plain, target: self, action: #selector(donePicker));
        let spaceButton1 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Dismiss", comment: "إلغاء"), style: .plain , target: self, action: #selector(cancel))
        toolbar.setItems([doneButton , spaceButton1 ,cancelButton], animated: false)
        toolbar.barTintColor = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        cityTextField.inputView = cityPicker
        cityTextField.inputAccessoryView = toolbar
    }
    @objc func cancel1(){
        districtTextField.text = ""
        self.view.endEditing(true)
    }
    @objc func donePicker1(){
        if districtTextField.text == "" {
            if words1.count > 0 {
                districtTextField.text = words1[0]
                selectedArea = selectedCity.areas?[0] ?? Areas()
                areaId = selectedArea.id ?? 0
            } else {
                self.dismiss(animated: true, completion: nil)
            }
        }
        self.view.endEditing(true)
    }
    @objc func cancel(){
        cityTextField.text = ""
        self.view.endEditing(true)
    }
    @objc func donePicker(){
        districtTextField.text = ""
        districtTextField.isEnabled = false
        districtTextField.borderColor = #colorLiteral(red: 0.6642242074, green: 0.6642400622, blue: 0.6642315388, alpha: 1)
        if cityTextField.text == "" {
            if words.count > 0 {
                cityTextField.text = words[0]
                selectedCity = CityMain.Instance.cityData.data?[0] ?? CitiesData()
                cityId = selectedCity.id ?? 0
                words1 = selectedCity.getAreasName()
                self.areaPicker.reloadAllComponents()
            } else {
                self.dismiss(animated: true, completion: nil)
            }
        }
        self.view.endEditing(true)
        districtTextField.isEnabled = true
        self.areaPicker.reloadAllComponents()
    }
}
