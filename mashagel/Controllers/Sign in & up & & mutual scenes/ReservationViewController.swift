//
//  ReservationViewController.swift
//  mashagel
//
//  Created by MACBOOK on 9/26/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD
import FCAlertView
import SDWebImage
import HCSStarRatingView
class ReservationViewController: UIViewController{
    var myReservation = MyReservation(){
        didSet{
            tableView.reloadData()
        }
    }
        
    @IBOutlet weak var tableView: UITableView!
    
   override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.changeFont()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        tableView.separatorStyle = .none
        userData.Instance.fetchUser()
        loadMyReservationData()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationItem.backBarButtonItem?.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
               self.navigationController?.navigationBar.backItem?.backBarButtonItem?.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
               self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
               self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        if L102Language.currentAppleLanguage() == "ar" {
            self.title = "الحجوزات"
        }else{
            self.title = NSLocalizedString("Reservations", comment:"الحجوزات")
        }
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        _ = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
    @IBAction func dismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
//MARK:- TableView Delegate and DataSoruce
extension ReservationViewController{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myReservation.data?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReservationCell", for: indexPath)
        if let titleValue = myReservation.data?[indexPath.row].shop?.name {
            let title = cell.viewWithTag(1) as! UILabel
            title.text = titleValue
        }
        if let titleValue1 = myReservation.data?[indexPath.row].total_price {
            let title1 = cell.viewWithTag(2) as! UILabel
            title1.text = "$\(titleValue1)"
        }
        return cell
    }
}
//MARK:- Network
extension ReservationViewController{
    func loadMyReservationData(){
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.showMyReservations() , method: .get, parameters: nil , encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "Error", SubTitle: err.parseError(), Image: #imageLiteral(resourceName: "img34"))
                        print(err.message!)
                    }catch{
                        print("errorrrrelse")
                    }
                }else{
                    do {
                        self.myReservation = try JSONDecoder().decode(MyReservation.self, from: response.data!)
                        print(self.myReservation)
                    }catch{
                      //  HUD.flash(.label("Error Try Again"), delay: 1.0)
                        print(error)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }

}
