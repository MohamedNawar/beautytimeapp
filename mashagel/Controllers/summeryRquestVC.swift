//
//  summeryRquestVC.swift
//  mashagel
//
//  Created by iMac on 9/16/19.
//  Copyright © 2019 MuhammedAli. All rights reserved.
//



import UIKit
import Alamofire
import PKHUD
import FCAlertView
import Font_Awesome_Swift


protocol ConfirmService {
    func ConfirmServiceDelegate()
}

class summeryRquestVC: UIViewController{
    var count = Int()
    var reservationData = ReservationData(){
        didSet{
        }
    }
    var checkFlag = false
    var delegate: ConfirmService?
    
    @IBOutlet weak var confirmImage: UIImageView!
    @IBOutlet weak var timeLable: UILabel!
    @IBOutlet weak var totalPrice: UILabel!
    @IBOutlet weak var dateLable: UILabel!
    @IBOutlet weak var checkImage: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        print(reservationData.id ?? 0)
        super.viewDidLoad()
        if L102Language.currentAppleLanguage() == "ar" {
            confirmImage.transform = CGAffineTransform (scaleX: -1,y: 1);
        }
        tableView.separatorStyle = .none
        userData.Instance.fetchUser()
        self.tableView.reloadData()
        if let dateValue = reservationData.date?.date {
            var stringValue = dateValue
            stringValue = stringValue.components(separatedBy: " ")[0]
            dateLable.text = stringValue
        }
        let arr = reservationData.times?.ArString ?? [String]()
        timeLable.text = "\(arr.first ?? "")"
        if let priceValue = reservationData.total_price {
            if L102Language.currentAppleLanguage() == "ar" {
                totalPrice.text = "ريال \(priceValue)"
            }else{
                totalPrice.text = "\(priceValue) SR"
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationItem.backBarButtonItem?.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationController?.navigationBar.backItem?.backBarButtonItem?.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.tableView.reloadData()
        if L102Language.currentAppleLanguage() == "ar" {
            totalPrice.textAlignment = .right
        }
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        _ = alert.bounds
        alert.colorScheme = #colorLiteral(red: 0.8539453149, green: 0.2522385418, blue: 0.4786654115, alpha: 1)
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: NSLocalizedString("Done", comment: "تم"), andButtons: nil)
    }
    @IBAction func reserve(_ sender: Any) {
        self.delegate?.ConfirmServiceDelegate()
    }
    @IBAction func check(_ sender: Any) {
        if checkFlag == false {
            checkImage.image = #imageLiteral(resourceName: "verification-mark")
            checkFlag = true
        }else{
            checkImage.image =  #imageLiteral(resourceName: "img41-2")
            checkFlag = false
        }
    }
    @IBAction func termsAndConditions(_ sender: Any) {
        let navVC = storyboard?.instantiateViewController(withIdentifier: "AboutUsTermAndConditionsVC") as! AboutUsTermAndConditionsVC
        navVC.flag = 1
        self.navigationController?.pushViewController(navVC, animated: true)
    }
}
//MARK:- TableView Delegate and DataSource
extension summeryRquestVC: UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        count = reservationData.packages?.count ?? 0
        if reservationData.service_items?.items?.count ?? 0 > 0 {
            count+=1
        }
        print(count)
        return count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if reservationData.service_items?.items?.count ?? 0 > 0 {
            if section == count-1 {
                return reservationData.service_items?.items?.count ?? 0
            }
        }else{
            print(reservationData.packages?[section].services?.count ?? 0)
            return reservationData.packages?[section].services?.count ?? 0
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RequestSummaryCell", for: indexPath)
        if reservationData.service_items?.items?.count ?? 0 > 0 {
            if indexPath.section == count-1 {
                let title = cell.viewWithTag(11) as! UILabel
                let title1 = cell.viewWithTag(3) as! UILabel
                let title2 = cell.viewWithTag(2) as! UILabel
                if let titleValue = reservationData.service_items?.items?[indexPath.row].service?.name {
                    print(titleValue)
                    title.text = titleValue
                }
                if let titleValue = reservationData.service_items?.items?[indexPath.row].quantity {
                    if L102Language.currentAppleLanguage() == "ar" {
                        title2.text = "عدد الاشخاص: " + "\(titleValue)"
                        title2.textAlignment = .right
                    }else{
                        title2.text = NSLocalizedString("Number Of Persons: ", comment: "") + "\(titleValue)"
                        title2.textAlignment = .right
                    }
                }
                if let titleValue = reservationData.service_items?.items?[indexPath.row].amount_of_time?.SString {
                    title1.text = "\(titleValue)" + NSLocalizedString(" Minutes", comment: "")
                    if L102Language.currentAppleLanguage() == "ar" {
                        title1.text = "\(titleValue)" + NSLocalizedString(" دقيقة", comment: "")
                        title1.textAlignment = .right
                    }
                }
                if L102Language.currentAppleLanguage() == "ar" {
                    title1.textAlignment = .right
                    title.textAlignment = .right
                    title2.textAlignment = .right
                    confirmImage.transform = CGAffineTransform (scaleX: -1,y: 1);
                }
            }
        }else{
            if let titleValue = reservationData.packages?[indexPath.section].services?[indexPath.row].name {
                print(titleValue)
                let title = cell.viewWithTag(11) as! UILabel
                if L102Language.currentAppleLanguage() == "ar" {
                    title.textAlignment = .right
                }
                title.text = titleValue
            }
            let title = cell.viewWithTag(2) as! UILabel
            if L102Language.currentAppleLanguage() == "ar" {
                title.textAlignment = .right
            }
            
            if let titleValue = reservationData.packages?[indexPath.section].services?[indexPath.row].quantity {
                title.text = NSLocalizedString("Number Of Persons: ", comment: "") + "\(titleValue)"
                if L102Language.currentAppleLanguage() == "ar" {
                    title.text = NSLocalizedString("عدد الأشخاص: ", comment: "") + "\(titleValue)"
                    title.textAlignment = .right
                }
            }else{
                title.text = NSLocalizedString("Number Of Persons: 1", comment: "")
                if L102Language.currentAppleLanguage() == "ar" {
                    title.textAlignment = .right
                    title.text = NSLocalizedString("عدد الأشخاص: ١ ", comment: "")
                    title.textAlignment = .right
                }
            }
            if let titleValue = reservationData.packages?[indexPath.section].services?[indexPath.row].amount_of_time?.IInt {
                let title = cell.viewWithTag(3) as! UILabel
                title.text = "\(titleValue)" + NSLocalizedString(" Minutes", comment: "")
                if L102Language.currentAppleLanguage() == "ar" {
                    title.text = "\(titleValue)" + NSLocalizedString(" دقيقة", comment: "")
                    title.textAlignment = .right
                }
            }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerCell = tableView.dequeueReusableCell(withIdentifier: "RequestSummaryHeaderCell")
        if let titleValue = reservationData.packages?[section].name {
            print(titleValue)
            let title = headerCell?.viewWithTag(11) as! UILabel
            if L102Language.currentAppleLanguage() == "ar" {
                title.textAlignment = .right
            }
            title.text = titleValue
        }
        return headerCell
        if reservationData.service_items?.items?.count ?? 0 > 0 {
            if section == count-1 {
                return UIView()
            }
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if reservationData.service_items?.items?.count ?? 0 > 0 {
            if section == count-1 {
                return 0.0
            }else{
                return 40
            }
        }else{
            return 40
        }
    }
}
