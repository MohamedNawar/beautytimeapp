//
//  OfferCollapseCell.swift
//  mashagel
//
//  Created by ElNoorOnline on 1/6/19.
//  Copyright © 2019 MuhammedAli. All rights reserved.
//

import UIKit
import SDWebImage
protocol TableViewCellDelegate : class {
    func didAddService(_ sender: OfferCollapseCell)
}

class OfferCollapseCell: UITableViewCell {
    
    @IBOutlet weak var oldPriceLbl: UILabel!
    @IBOutlet weak var discountPercentage: UILabel!
    @IBOutlet weak var service_image: UIImageView!
    @IBOutlet weak var service_name: UILabel!
    @IBOutlet weak var serviceTime_lbl: UILabel!
    @IBOutlet weak var servicePrice_lbl: UILabel!
    @IBOutlet weak var numOfPerson_lbl: UILabel!
    @IBOutlet weak var increase_btn: UIButton!
    @IBOutlet weak var addService_btn: btn!
    
    weak var delegate: TableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    func configure_cell(data : ServicesData) {
        if data != nil {
            if data.percentage ?? "0" != "0" {
                var oldPriceString:String = ""
                discountPercentage.text = "\(data.percentage ?? "")% \(NSLocalizedString("Discount", comment: ""))"
                    if L102Language.currentAppleLanguage() == "ar" {
                        oldPriceString = "\(data.price ?? "0") ريال"
                    serviceTime_lbl.text = "\(data.amount_of_time?.SString ?? "0") دقيقة"
                    servicePrice_lbl.text = "\(getPercentag(percentage: (data.percentage ?? "0.0").replacingOccurrences(of: ",", with: "").toDouble() ?? 0.0, newPrice: (data.price ?? "0.0").replacingOccurrences(of: ",", with: "").toDouble() ?? 0.0)) ريال"
                    }else{
                        serviceTime_lbl.text = "\(data.amount_of_time?.SString ?? "0") minute"
                        servicePrice_lbl.text = "\(getPercentag(percentage: (data.percentage ?? "0.0").replacingOccurrences(of: ",", with: "").toDouble() ?? 0.0, newPrice: (data.price ?? "0.0").replacingOccurrences(of: ",", with: "").toDouble() ?? 0.0)) SR"

                        oldPriceString = "\(data.price ?? "0") SR"
                    }

                    let textRange = NSMakeRange(0, oldPriceString.count)
                    let attributedText = NSMutableAttributedString(string: oldPriceString)
                    attributedText.addAttribute(NSAttributedString.Key.strikethroughStyle,
                                                value: NSUnderlineStyle.styleSingle.rawValue,
                                                range: textRange)
                    self.oldPriceLbl.attributedText = attributedText
                                         
                
            }else{
                discountPercentage.isHidden = true
                oldPriceLbl.isHidden = true
                if L102Language.currentAppleLanguage() == "ar" {
                                    servicePrice_lbl.text = "\(data.price ?? "0") ريال"
                                }else{
                                    servicePrice_lbl.text = "\(data.price ?? "0") SR"
                                }
            }
            service_name.text = data.name
           
            service_image.sd_setImage(with: URL(string: "http://service.beautytimes.org/media/2/conversions/medium.png"), completed: nil)
        }
    }
    func getPercentag(percentage:Double , newPrice:Double) -> String {
        let percent = newPrice * percentage / 100
        return (newPrice - percent).rounded(toPlaces: 1).clean
    }
    
    @IBAction func increase_btn(_ sender: Any) {
        let current_count = Int(numOfPerson_lbl.text!)
        if  current_count != 1{
            numOfPerson_lbl.text = "\(current_count! - 1 )"
        }
    }
    @IBAction func decrease_btn(_ sender: Any) {
        let current_count = Int(numOfPerson_lbl.text!)
        numOfPerson_lbl.text = "\(current_count! + 1 )"
    }
    @IBAction func addService_btn(_ sender: UIButton) {
        delegate?.didAddService(self)
    }
}

extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}

extension Double {
    var clean: String {
        return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self)
    }
}
