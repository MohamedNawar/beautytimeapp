//
//  Cart1ContentCell.swift
//  mashagel
//
//  Created by iMac on 10/23/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD
import FCAlertView
import Font_Awesome_Swift
class Cart1ContentCell: UITableViewCell {
    var words = [EmployeeData]()
    var DeleteServiceCallBack: (() -> Void)?
    var reservation = Reservation()
    
    @IBOutlet weak var serviceName: UILabel!
    @IBOutlet weak var servicePrice: UILabel!
    @IBOutlet weak var serviceDescription: UILabel!
    @IBOutlet weak var serviceDescription2: UILabel!
    @IBOutlet weak var deleteBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        if L102Language.currentAppleLanguage() == "ar"{
            serviceName.textAlignment = .right
            serviceDescription.textAlignment = .right
            serviceDescription2.textAlignment = .right
        }
    }
    func updateViews(service: ServicesData) {
        serviceName.text = service.service?.name ?? ""
        servicePrice.text = "$\(service.service?.price ?? "")"
        serviceDescription2.text = service.service?.description ?? ""
        serviceDescription.text = "\(service.amount_of_time?.IInt ?? 0) minute"
        words = service.employees ?? [EmployeeData]()
    }
    @IBAction func deleteService(_ sender: Any) {
        DeleteServiceCallBack!()
    }
}
