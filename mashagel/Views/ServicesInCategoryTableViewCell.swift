//
//  ServicesInCategoryTableViewCell.swift
//  mashagel
//
//  Created by MACBOOK on 10/13/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//

import UIKit

class ServicesInCategoryTableViewCell: UITableViewCell {
    var AddServiceCallBack: ((Int) -> Void)?
    var ShowServiceImages: (()->Void)?
    var quentity = 1{
        didSet{
            numberOfOrder.text = "\(quentity)"
        }
    }
    
    @IBOutlet weak var numberOfOrder: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    @IBAction func addTpReservation(_ sender: Any) {
        AddServiceCallBack!(quentity)
    }
    @IBAction func addOne(_ sender: Any) {
        quentity = quentity + 1
    }
    @IBAction func removeOne(_ sender: Any) {
        if quentity == 1 {
            return
        }else{
            quentity = quentity - 1
        }
    }
    @IBAction func showImages(_ sender: Any) {
        ShowServiceImages!()
    }
}
