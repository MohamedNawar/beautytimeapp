//
//  PackageReservationCell.swift
//  mashagel
//
//  Created by MACBOOK on 10/8/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//

import UIKit

class PackageReservationCell: UITableViewCell {
    var PackageReservationCallBack: (() -> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    @IBAction func reserve(_ sender: Any) {
        PackageReservationCallBack!()
    }
}
