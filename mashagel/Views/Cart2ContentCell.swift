//
//  Cart2ContentCell.swift
//  mashagel
//
//  Created by MACBOOK on 10/10/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD
import FCAlertView
import Font_Awesome_Swift
class Cart2ContentCell: UITableViewCell {
    var DeleteServiceCallBack: (() -> Void)?
    var reservation = Reservation()
    
    @IBOutlet weak var oldPriceLbl: UILabel!
    @IBOutlet weak var percentageLbl: UILabel!
    @IBOutlet weak var serviceName: UILabel!
    @IBOutlet weak var servicePrice: UILabel!
    @IBOutlet weak var serviceDescription: UILabel!
    @IBOutlet weak var serviceDescription2: UILabel!
    @IBOutlet weak var deleteBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        if L102Language.currentAppleLanguage() == "ar"{
            serviceName.textAlignment = .right
            serviceDescription.textAlignment = .right
            serviceDescription2.textAlignment = .right
        }
    }
    func updateViews(service: ServicesData) {
        if service.service?.percentage ?? "0" != "0" {
                      var oldPriceString:String = ""
                      percentageLbl.text = "\(service.service?.percentage ?? "")% \(NSLocalizedString("Discount", comment: ""))"
                          if L102Language.currentAppleLanguage() == "ar" {
                              oldPriceString = "\(service.service?.price ?? "0") ريال"
                          servicePrice.text = "\(getPercentag(percentage: (service.service?.percentage ?? "0.0").replacingOccurrences(of: ",", with: "").toDouble() ?? 0.0, newPrice: (service.service?.price ?? "0.0").replacingOccurrences(of: ",", with: "").toDouble() ?? 0.0)) ريال"
                          }else{
                              servicePrice.text = "\(getPercentag(percentage: (service.service?.percentage ?? "0.0").replacingOccurrences(of: ",", with: "").toDouble() ?? 0.0, newPrice: (service.service?.price ?? "0.0").replacingOccurrences(of: ",", with: "").toDouble() ?? 0.0)) SR"

                              oldPriceString = "\(service.service?.price ?? "0") SR"
                          }

                          let textRange = NSMakeRange(0, oldPriceString.count)
                          let attributedText = NSMutableAttributedString(string: oldPriceString)
                          attributedText.addAttribute(NSAttributedString.Key.strikethroughStyle,
                                                      value: NSUnderlineStyle.styleSingle.rawValue,
                                                      range: textRange)
                          self.oldPriceLbl.attributedText = attributedText
                                               
                      
                  }else{
                    percentageLbl.isHidden = true
                      oldPriceLbl.isHidden = true
                      if L102Language.currentAppleLanguage() == "ar" {
                                          servicePrice.text = "\(service.price ?? "0") ريال"
                                      }else{
                                          servicePrice.text = "\(service.price ?? "0") SR"
                                      }
                  }
        serviceName.text = service.service?.name ?? ""
        if L102Language.currentAppleLanguage() == "ar" {
            serviceDescription.text = "\(service.amount_of_time?.SString ?? "0") دقيقة "
            servicePrice.text = "ريال \(service.service?.price ?? "")"
            serviceDescription2.text = "عدد الاشخاص :\(service.quantity ?? "") "
        }else{
            serviceDescription.text = "\(service.amount_of_time?.SString ?? "0") minute "
            servicePrice.text = "SR \(service.service?.price ?? "")"
            serviceDescription2.text = "Number of persons: \(service.quantity ?? "")"
        }
    }
    func getPercentag(percentage:Double , newPrice:Double) -> String {
           let percent = newPrice * percentage / 100
           return (newPrice - percent).rounded(toPlaces: 1).clean
       }
    @IBAction func deleteService(_ sender: Any) {
        DeleteServiceCallBack?()
    }
}
