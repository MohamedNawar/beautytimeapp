//
//  WeekDaysEmployeeTableViewCell.swift
//  mashagel
//
//  Created by MACBOOK on 10/8/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//

import UIKit

class WeekDaysEmployeeTableViewCell: UITableViewCell {
    var editWeekDayCallBack: (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBAction func edit(_ sender: Any) {
        editWeekDayCallBack!()
    }
}
