//
//  WeekDaysTableViewCell.swift
//  mashagel
//
//  Created by MACBOOK on 10/7/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//

import UIKit

class WeekDaysTableViewCell: UITableViewCell {
    var editWeekDayCallBack: (() -> Void)?
    var addToHolidaysCallBack: (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBAction func addToHoliday(_ sender: Any) {
        addToHolidaysCallBack!()
    }
    @IBAction func edit(_ sender: Any) {
        editWeekDayCallBack!()
    }
}
