//
//  OfficialHolidaysTableViewCell.swift
//  mashagel
//
//  Created by MACBOOK on 10/7/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//

import UIKit

class OfficialHolidaysTableViewCell: UITableViewCell {
    var OfficialHolidaysCallBack: (() -> Void)?
    var RemoveHolidaysCallBack: (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    @IBAction func edit(_ sender: Any) {
        OfficialHolidaysCallBack!()
    }
    @IBAction func remove(_ sender: Any) {
        RemoveHolidaysCallBack!()
    }
}
