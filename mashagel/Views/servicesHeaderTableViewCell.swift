//
//  servicesHeaderTableViewCell.swift
//  mashagel
//
//  Created by MACBOOK on 10/12/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//

import UIKit

class servicesHeaderTableViewCell: UITableViewCell {
    var openAndCloseServiceCallBack: (() -> Void)?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBAction func openAndClose(_ sender: Any) {
        openAndCloseServiceCallBack!()
    }
}
